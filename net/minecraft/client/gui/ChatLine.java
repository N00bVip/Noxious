package net.minecraft.client.gui;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MovingObjectPosition;

public class ChatLine
{
    /** GUI Update Counter value this Line was created at */
    private final int updateCounterCreated;
    private final IChatComponent lineString;

    private static FloatBuffer field_74543_a = KeyBinding.h(16);
    private static final MovingObjectPosition field_74541_b = new MovingObjectPosition(0.20000000298023224D, 1.0D, -0.699999988079071D).a();
    private static final MovingObjectPosition field_74542_c = new MovingObjectPosition(-0.20000000298023224D, 1.0D, 0.699999988079071D).a();

    /**
     * int value to refer to existing Chat Lines, can be 0 which means unreferrable
     */
    private final int chatLineID;
    private static final String __OBFID = "CL_00000627";

    public static void a()
    {
      GlStateManager.disableLighting();
      GlStateManager.disableBooleanStateAt(0);
      GlStateManager.disableBooleanStateAt(1);
      GlStateManager.disableColorMaterial();
    }

    private static FloatBuffer a(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4)
    {
      return a((float)paramDouble1, (float)paramDouble2, (float)paramDouble3, (float)paramDouble4);
    }

    private static FloatBuffer a(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
      field_74543_a.clear();
      field_74543_a.put(paramFloat1).put(paramFloat2).put(paramFloat3).put(paramFloat4);
      field_74543_a.flip();
      return field_74543_a;
    }

    public static void b()
    {
      GlStateManager.enableLighting();
      GlStateManager.enableBooleanStateAt(0);
      GlStateManager.enableBooleanStateAt(1);
      GlStateManager.enableColorMaterial();
      GlStateManager.colorMaterial(1032, 5634);
      float f1 = 0.4F;
      float f2 = 0.6F;
      float f3 = 0.0F;

      GL11.glLight(16384, 4611, a(field_74541_b.field_72313_a, field_74541_b.field_178784_g, field_74541_b.field_72307_f, 0.0D));
      GL11.glLight(16384, 4609, a(f2, f2, f2, 1.0F));
      GL11.glLight(16384, 4608, a(0.0F, 0.0F, 0.0F, 1.0F));
      GL11.glLight(16384, 4610, a(f3, f3, f3, 1.0F));

      GL11.glLight(16385, 4611, a(field_74542_c.field_72313_a, field_74542_c.field_178784_g, field_74542_c.field_72307_f, 0.0D));
      GL11.glLight(16385, 4609, a(f2, f2, f2, 1.0F));
      GL11.glLight(16385, 4608, a(0.0F, 0.0F, 0.0F, 1.0F));
      GL11.glLight(16385, 4610, a(f3, f3, f3, 1.0F));

      GlStateManager.shadeModel(7424);
      GL11.glLightModel(2899, a(f1, f1, f1, 1.0F));
    }

    public ChatLine(int p_i45000_1_, IChatComponent p_i45000_2_, int p_i45000_3_)
    {
        this.lineString = p_i45000_2_;
        this.updateCounterCreated = p_i45000_1_;
        this.chatLineID = p_i45000_3_;
    }

    public IChatComponent getChatComponent()
    {
        return this.lineString;
    }

    public int getUpdatedCounter()
    {
        return this.updateCounterCreated;
    }

    public int getChatLineID()
    {
        return this.chatLineID;
    }
}
