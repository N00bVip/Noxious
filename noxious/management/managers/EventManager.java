package noxious.management.managers;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

import noxious.event.Event;
import noxious.event.Listener;
import noxious.management.ListManager;

public class EventManager extends ListManager {
   public void call(Event event) {
      if(this.getContents() != null) {
         try {
            Iterator var3 = this.getContents().iterator();

            while(var3.hasNext()) {
               Listener listener = (Listener)var3.next();
               listener.onEvent(event);
            }
         } catch (ConcurrentModificationException var4) {
            ;
         }
      }

   }

   public void addListener(Listener listener) {
      if(!this.getContents().contains(listener)) {
         this.getContents().add(listener);
      }

   }

   public void removeListener(Listener listener) {
      if(this.getContents().contains(listener)) {
         this.getContents().remove(listener);
      }

   }

   public void setup() {
      this.contents = new ArrayList();
   }
}
