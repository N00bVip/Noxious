package noxious.management.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Set;

import org.reflections.Reflections;

import noxious.management.ListManager;
import noxious.mod.Mod;
import noxious.mod.mods.Blink;
import noxious.mod.mods.Outline;
import noxious.mod.mods.TestCivBreak;
import noxious.mod.mods.XivKillAura;
import noxious.util.HttpPost;
import noxious.util.Logger;


public final class ModManager extends ListManager {

private String type;

public final Mod getModByName(String name) {
      if(this.contents == null) {
         return null;
      } else {
         try {
            Iterator var3 = this.contents.iterator();

            while(var3.hasNext()) {
               Mod mod = (Mod)var3.next();
               if(mod.getName().equalsIgnoreCase(name)) {
                  return mod;
               }
            }
         } catch (ConcurrentModificationException var4) {
            ;
         }

         return null;
      }
   }

   public void setup() {
      Logger.logChat("Starting to load up the modules.");
      this.contents = new ArrayList();
      Reflections reflect = new Reflections(new Object[]{Mod.class});
      Set mods = reflect.getSubTypesOf(Mod.class);
      Iterator var4 = mods.iterator();
      type = HttpPost.getUserType();
      Mod.type = type;

      while(var4.hasNext()) {
         Class clazz = (Class)var4.next();
         if(type.equals("0") && (clazz == XivKillAura.class || clazz == Blink.class || clazz == TestCivBreak.class) || !type.equals("4") && clazz == Outline.class) {
        	 continue;
         }
         try {
            Mod var7 = (Mod)clazz.newInstance();
            this.getContents().add(var7);
            Logger.logChat("Loaded mod \"" + var7.getName() + "\"");
         } catch (InstantiationException var6) {
            var6.printStackTrace();
            Logger.logChat("Failed to load mod \"" + clazz.getSimpleName() + "\" (InstantiationException)");
         } catch (IllegalAccessException var71) {
            var71.printStackTrace();
            Logger.logChat("Failed to load mod \"" + clazz.getSimpleName() + "\" (IllegalAccessException)");
         }
      }

      Collections.sort(this.contents, new Comparator() {
         public int compare(Mod mod1, Mod mod2) {
            return mod1.getName().compareTo(mod2.getName());
         }

         public int compare(Object var1, Object var2) {
            return this.compare((Mod)var1, (Mod)var2);
         }
      });
      Logger.logChat("Successfully loaded " + this.getContents().size() + " mods.");
   }

   public static enum Category {
      EXPLOITS("EXPLOITS", 0),
      MOVEMENT("MOVEMENT", 1),
      PLAYER("PLAYER", 2),
      WORLD("WORLD", 3),
      RENDER("RENDER", 4),
      COMBAT("COMBAT", 5),
      HUD("HUD", 6),
      INVISIBLE("INVISIBLE", 7);

      private static final ModManager.Category[] ENUM$VALUES;

      static {
         ENUM$VALUES = new ModManager.Category[]{EXPLOITS, MOVEMENT, PLAYER, WORLD, RENDER, COMBAT, INVISIBLE};
      }

      private Category(String var11, int var21) {
      }
   }
}
