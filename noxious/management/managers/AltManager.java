package noxious.management.managers;

import java.util.ArrayList;

import noxious.management.ListManager;
import noxious.util.Alt;

public final class AltManager extends ListManager {
   private Alt lastAlt;

   public Alt getLastAlt() {
      return this.lastAlt;
   }

   public void setLastAlt(Alt alt) {
      this.lastAlt = alt;
   }

   public void setup() {
      this.contents = new ArrayList();
   }
}
