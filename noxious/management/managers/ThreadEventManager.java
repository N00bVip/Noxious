package noxious.management.managers;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

import noxious.management.ListManager;
import noxious.threads.event.ThreadEvent;
import noxious.threads.event.ThreadEventListener;

public class ThreadEventManager extends ListManager {
   public void call(ThreadEvent event) {
      if(this.getContents() != null) {
         try {
            Iterator var3 = this.getContents().iterator();
            while(var3.hasNext()) {
               ThreadEventListener listener = (ThreadEventListener)var3.next();
               listener.onEvent(event);
            }
         } catch (ConcurrentModificationException var4) {
            ;
         }
      }

   }

   public void addListener(ThreadEventListener listener) {
      if(!this.getContents().contains(listener)) {
         this.getContents().add(listener);
      }

   }

   public void removeListener(ThreadEventListener listener) {
      if(this.getContents().contains(listener)) {
         this.getContents().remove(listener);
      }

   }

   public void setup() {
      this.contents = new ArrayList();
   }
}
