package noxious.management.managers;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.util.StringUtils;
import noxious.management.ListManager;

public class AdminManager extends ListManager {
   public void addAdmin(String admin) {
      if(!this.contents.contains(admin)) {
         this.contents.add(admin);
      }

   }

   public List getAdmins() {
      return this.contents;
   }

   public boolean isAdmin(String admin) {
      return this.contents.contains(StringUtils.stripControlCodes(admin));
   }

   public void removeAdmin(String admin) {
      if(!this.contents.contains(admin)) {
         this.contents.remove(this.contents.indexOf(admin));
      }

   }

   public void setup() {
      this.contents = new ArrayList();
   }
}
