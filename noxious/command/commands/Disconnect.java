package noxious.command.commands;

import net.minecraft.client.Minecraft;
import noxious.command.Command;
import noxious.ui.screens.GuiNoxiousMainMenu;

public class Disconnect extends Command {
   public Disconnect() {
      super("disconnect", "<none>", new String[]{"discon"});
   }

   public void run(String message) {
      Minecraft.getMinecraft().displayGuiScreen(new GuiNoxiousMainMenu());
   }
}
