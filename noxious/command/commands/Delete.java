package noxious.command.commands;

import noxious.Noxious;
import noxious.command.Command;
import noxious.util.Logger;

public final class Delete extends Command {
   public Delete() {
      super("del", "<name>", new String[]{"fdel", "delete"});
   }

   public void run(String message) {
      String name = message.split(" ")[1];
      Noxious.getFriendManager().removeFriend(name);
      Logger.logChat("Friend \"" + name + "\" removed.");
      if(Noxious.getFileManager().getFileByName("friends") != null) {
         Noxious.getFileManager().getFileByName("friends").saveFile();
      }

   }
}
