package noxious.command.commands;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;

import net.minecraft.client.Minecraft;
import net.minecraft.util.MathHelper;
import noxious.command.Command;
import noxious.util.Logger;

public final class CopyCoords extends Command {
   public CopyCoords() {
      super("copycoords", "none", new String[]{"cc"});
   }

   public void run(String message) {
      int x = MathHelper.floor_double(Minecraft.getMinecraft().thePlayer.posX);
      int y = MathHelper.floor_double(Minecraft.getMinecraft().thePlayer.posY);
      int z = MathHelper.floor_double(Minecraft.getMinecraft().thePlayer.posZ);
      StringSelection ss = new StringSelection(x + " " + y + " " + z);
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.setContents(ss, (ClipboardOwner)null);
      Logger.logChat("Current coordinates copied to clipboard.");
   }
}
