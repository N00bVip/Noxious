package noxious.command.commands;

import noxious.Noxious;
import noxious.command.Command;
import noxious.util.Logger;

public class Ghost extends Command {

   public static boolean shouldGhost = false;


   public Ghost() {
      super("screensharemode", "<boolean>", new String[]{"ghostmode", "ssm"});
   }

   public void run(String message) {
      boolean b = Boolean.parseBoolean(message.split(" ")[1]);
      shouldGhost = b;
      if(b) {
         Logger.logChat("Screen Share Mode activated! \nYour command prefix has been changed to \'---\'!");
      } else if(!b) {
         Logger.logChat("Screen Share Mode deactivated! \nYour command prefix has been changed to \'.\'!");
      }

      Noxious.getFileManager().getFileByName("ghostmodefile").saveFile();
   }
}
