package noxious.command.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C14PacketTabComplete;
import net.minecraft.network.play.server.S3APacketTabComplete;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.Listener;
import noxious.event.events.EventPacketReceive;
import noxious.util.Logger;

public class PluginDiscover extends Command {

   public Listener listener = new Listener() {
      public void onEvent(Event event) {
         PluginDiscover com = (PluginDiscover)Noxious.getCommandManager().getCommandByName("plugindiscover");
         if(event instanceof EventPacketReceive) {
        	 EventPacketReceive rec = (EventPacketReceive)event;
            com.plugins = new ArrayList();
            if(rec.getPacket() instanceof S3APacketTabComplete) {
               S3APacketTabComplete packet = (S3APacketTabComplete)rec.getPacket();
               rec.setCancelled(true);
               String[] var8;
               int var7 = (var8 = packet.func_149630_c()).length;

               for(int plugin = 0; plugin < var7; ++plugin) {
                  String pluginBuilder = var8[plugin];
                  String[] split = pluginBuilder.split(":");
                  if(split.length > 1 && !com.plugins.contains(split[0].substring(1))) {
                     com.plugins.add(split[0].substring(1));
                  }
               }

               StringBuilder var10 = new StringBuilder();
               var10.append("Found plugins: ");
               Iterator var12 = com.plugins.iterator();

               while(var12.hasNext()) {
                  String var11 = (String)var12.next();
                  var10.append(var11);
                  var10.append(", ");
               }

               Logger.logChat(var10.substring(0, var10.length() - 2));
               Noxious.getEventManager().removeListener(PluginDiscover.this.listener);
            }
         }

      }
   };
   public List<String> plugins = new ArrayList();


   public PluginDiscover() {
      super("plugindiscover", "name", new String[]{"plugins", "pgd"});
   }

   public void run(String message) {
      Logger.logChat("Discovering plugins, please wait...");
      Minecraft.getMinecraft().getNetHandler().addToSendQueue(new C14PacketTabComplete("/"));
      Noxious.getEventManager().addListener(this.listener);
   }
}
