package noxious.command.commands;

import net.minecraft.client.Minecraft;
import noxious.command.Command;
import noxious.util.Logger;
import noxious.util.StringOBF;

public class Obf extends Command {

   private final Minecraft mc = Minecraft.getMinecraft();


   public Obf() throws Exception {
      super(StringOBF.decodeBase641("b2Jm"), "<string>");
   }

   public void run(String message) {
	   try {
		Logger.logChat(StringOBF.decodeBase641("T0JG") + " " + StringOBF.encodeBase64(message.split(" ")[1]));
	} catch (Exception e1) {
		e1.printStackTrace();
	}
	   try {
		   Logger.logChat(StringOBF.decodeBase641("T0JG") + " " + StringOBF.encrypt("Test", message.split(" ")[1]));
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
	   try {
		   Logger.logChat(StringOBF.decodeBase641("VU5PQkY=") + "  " + StringOBF.decrypt("Test", StringOBF.encrypt("Test", message.split(" ")[1])));
	   } catch (Exception e) {
		   e.printStackTrace();
	   }
   }
}
