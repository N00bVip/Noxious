package noxious.command.commands;

import net.minecraft.client.Minecraft;
import noxious.command.Command;
import noxious.util.Logger;

public class VClip extends Command {

   private final Minecraft mc = Minecraft.getMinecraft();


   public VClip() {
      super("vclip", "<blocks>", new String[]{"vc"});
   }

   public void run(String message) {
      double blocks = Double.parseDouble(message.split(" ")[1]);
      this.mc.thePlayer.setPosition(this.mc.thePlayer.posX, this.mc.thePlayer.posY + blocks, this.mc.thePlayer.posZ);
      Logger.logChat("Teleported \"" + blocks + "\" blocks.");
   }
}
