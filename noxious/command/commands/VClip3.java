package noxious.command.commands;

import net.minecraft.client.Minecraft;
import noxious.command.Command;
import noxious.util.Logger;

public class VClip3 extends Command {

   private final Minecraft mc = Minecraft.getMinecraft();


   public VClip3() {
      super("vclip3", "<blocks>", new String[]{"vc3"});
   }

   public void run(String message) {
      double blocks = Double.parseDouble(message.split(" ")[1]);
      this.mc.thePlayer.setPosition(this.mc.thePlayer.posX, this.mc.thePlayer.posY, this.mc.thePlayer.posZ + blocks);
      Logger.logChat("Teleported \"" + blocks + "\" blocks.");
   }
}
