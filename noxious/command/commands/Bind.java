package noxious.command.commands;

import org.lwjgl.input.Keyboard;

import noxious.Noxious;
import noxious.command.Command;
import noxious.mod.Mod;
import noxious.util.Logger;

public final class Bind extends Command {
   public Bind() {
      super("bind", "<mod name> <key>", new String[0]);
   }

   public void run(String message) {
      Mod mod = Noxious.getModManager().getModByName(message.split(" ")[1]);
      if(mod == null) {
         Logger.logChat("Mod \"" + message.split(" ")[1] + "\" was not found!");
      } else {
         mod.setKeybind(Keyboard.getKeyIndex(message.split(" ")[2].toUpperCase()));
         Logger.logChat("Mod \"" + mod.getName() + "\" is now bound to: " + Keyboard.getKeyName(mod.getKeybind()));
      }

   }
}
