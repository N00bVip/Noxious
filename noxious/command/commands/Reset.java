package noxious.command.commands;

import noxious.Noxious;
import noxious.command.Command;
import noxious.util.Logger;

public class Reset extends Command {
   public Reset() {
      super("reload", "<none>", new String[]{"rl"});
   }

   public void run(String message) {
      Noxious.getCommandManager().setup();
      Noxious.getModManager().setup();
      Noxious.getValueManager().setup();
      Noxious.getEventManager().setup();
      Noxious.getFriendManager().setup();
      Noxious.getFileManager().setup();
      Logger.logChat("Noxious mod manager and command have been reset (dev help).");
   }
}
