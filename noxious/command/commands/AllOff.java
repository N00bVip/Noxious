package noxious.command.commands;

import java.util.Iterator;

import noxious.Noxious;
import noxious.command.Command;
import noxious.mod.Mod;


public class AllOff extends Command {

   public AllOff() {
      super("alloff", "none", new String[]{"aoff", "ao"});
   }

   public void run(String message) {
      Iterator var3 = Noxious.getModManager().getContents().iterator();

      while(var3.hasNext()) {
         Mod mod = (Mod)var3.next();
         if(mod.isEnabled()) {
            mod.toggle();
         }
      }

   }
}
