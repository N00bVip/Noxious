package noxious.command.commands;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C01PacketChatMessage;
import noxious.command.Command;

public final class Say extends Command {
   public Say() {
      super("say", "<message>", new String[0]);
   }

   public void run(String message) {
      Minecraft.getMinecraft().getNetHandler().addToSendQueue(new C01PacketChatMessage(message.substring(".say ".length())));
   }
}
