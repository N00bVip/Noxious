package noxious.command.commands;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import noxious.command.Command;

public final class Damage extends Command {
   public Damage() {
      super("damage", "<hearts>", new String[]{"dmg", "hurt"});
   }

   public void run(String message) {
      Minecraft mc = Minecraft.getMinecraft();
      double damageHearts = Double.parseDouble(message.split(" ")[1]);
      double x = mc.thePlayer.posX;
      double y = mc.thePlayer.posY;
      double z = mc.thePlayer.posZ;
      C03PacketPlayer.C04PacketPlayerPosition setOffGround = new C03PacketPlayer.C04PacketPlayerPosition(x, y + 0.3D, z, false);
      C03PacketPlayer.C04PacketPlayerPosition setDamage = new C03PacketPlayer.C04PacketPlayerPosition(x, y - 3.1D - damageHearts, z, false);
      C03PacketPlayer.C04PacketPlayerPosition setOnGround = new C03PacketPlayer.C04PacketPlayerPosition(x, y, z, true);
      mc.getNetHandler().addToSendQueue(setOffGround);
      mc.getNetHandler().addToSendQueue(setDamage);
      mc.getNetHandler().addToSendQueue(setOnGround);
   }
}
