package noxious.command.commands;

import net.minecraft.client.Minecraft;
import noxious.command.Command;
import noxious.util.Logger;

public class VClip2 extends Command {

   private final Minecraft mc = Minecraft.getMinecraft();


   public VClip2() {
      super("vclip2", "<blocks>", new String[]{"vc2"});
   }

   public void run(String message) {
      double blocks = Double.parseDouble(message.split(" ")[1]);
      this.mc.thePlayer.setPosition(this.mc.thePlayer.posX + blocks, this.mc.thePlayer.posY, this.mc.thePlayer.posZ);
      Logger.logChat("Teleported \"" + blocks + "\" blocks.");
   }
}
