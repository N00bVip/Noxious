package noxious.command.commands;

import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C10PacketCreativeInventoryAction;
import net.minecraft.util.StatCollector;
import noxious.command.Command;
import noxious.util.Logger;

public class Enchant extends Command {
   public Enchant() {
      super("enchant", "<enchant> <level>", new String[]{"enc"});
   }

   public void run(String message) {
      if(Minecraft.getMinecraft().playerController.isNotCreative()) {
         Logger.logChat("You must be in creative mode to edit an item\'s enchants!");
      } else {
         ItemStack itemStack = Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem();
         if(itemStack == null) {
            Logger.logChat("You must be holding an item to edit it!");
         } else {
            String name = message.split(" ")[1];
            int level = Integer.parseInt(message.split(" ")[2]);
            if(level > 127) {
               level = 127;
            } else if(level < -127) {
               level = -127;
            }

            boolean found = false;
            Enchantment[] var9 = Enchantment.enchantmentsList;
            int var8 = Enchantment.enchantmentsList.length;

            for(int var7 = 0; var7 < var8; ++var7) {
               Enchantment enchantment = var9[var7];
               if(enchantment != null) {
                  String encname = StatCollector.translateToLocal(enchantment.getName());
                  if(name.equalsIgnoreCase(encname.replaceAll(" ", "")) || name.equalsIgnoreCase("*")) {
                     itemStack.addEnchantment(enchantment, level);
                     Minecraft.getMinecraft().getNetHandler().addToSendQueue(new C10PacketCreativeInventoryAction(Minecraft.getMinecraft().thePlayer.inventory.currentItem, itemStack));
                     Logger.logChat("Added enchantment \"" + enchantment.getTranslatedName(level) + "\" to your current item.");
                     found = true;
                  }
               }
            }

            if(!found) {
               Logger.logChat("Invalid enchant.");
            }
         }
      }

   }
}
