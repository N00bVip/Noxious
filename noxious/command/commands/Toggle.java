package noxious.command.commands;

import noxious.Noxious;
import noxious.command.Command;
import noxious.mod.Mod;
import noxious.util.Logger;

public final class Toggle extends Command {
   public Toggle() {
      super("toggle", "<mod name>", new String[]{"t"});
   }

   public void run(String message) {
      String[] arguments = message.split(" ");
      Mod mod = Noxious.getModManager().getModByName(arguments[1]);
      if(mod == null) {
         Logger.logChat("Mod \"" + arguments[1] + "\" was not found!");
      } else {
         mod.toggle();
         Logger.logChat("Mod \"" + mod.getName() + "\" was toggled " + (mod.isEnabled()?"§2on":"§4off") + "§f.");
      }

   }
}
