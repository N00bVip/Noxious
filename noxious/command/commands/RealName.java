package noxious.command.commands;

import java.util.Iterator;

import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.util.StringUtils;
import noxious.Noxious;
import noxious.command.Command;
import noxious.util.Logger;

public final class RealName extends Command {
   public RealName() {
      super("realname", "<alias>", new String[]{"rn"});
   }

   public void run(String message) {
      boolean found = false;
      Iterator var4 = Minecraft.getMinecraft().getNetHandler().playerInfoMap.values().iterator();

      while(var4.hasNext()) {
         Object o = var4.next();
         NetworkPlayerInfo info = (NetworkPlayerInfo)o;
         String name = StringUtils.stripControlCodes(info.func_178845_a().getName());
         if(Noxious.getFriendManager().isFriend(name)) {
            String protect = (String)Noxious.getFriendManager().getContents().get(name);
            if(message.substring((message.split(" ")[0] + " ").length()).equalsIgnoreCase(protect)) {
               Logger.logChat("§fFriend \"§3" + protect + "§r\" real name is: " + name);
               found = true;
            }
         }
      }

      if(!found) {
         Logger.logChat("Friend \"" + message.split(" ")[1] + "\" was not found!");
      }

   }
}
