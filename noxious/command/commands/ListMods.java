package noxious.command.commands;

import java.util.Iterator;

import noxious.Noxious;
import noxious.command.Command;
import noxious.mod.Mod;
import noxious.util.Logger;


public final class ListMods extends Command {
   public ListMods() {
      super("listmods", "none", new String[]{"mods", "lm"});
   }

   public void run(String message) {
      StringBuilder list = new StringBuilder("Mods (" + Noxious.getModManager().getContents().size() + "): ");
      Iterator var4 = Noxious.getModManager().getContents().iterator();

      while(var4.hasNext()) {
         Mod mod = (Mod)var4.next();
         list.append(mod.isEnabled()?"§a":"§c").append(mod.getName()).append("§f, ");
      }

      Logger.logChat(list.toString().substring(0, list.toString().length() - 2));
      Logger.logChat("§aNOTE:§f This list is color coded based on if the mod is toggled on or off. Green = on, red = off.");
   }
}
