package noxious.command.commands;

import noxious.Noxious;
import noxious.command.Command;
import noxious.util.Logger;

public class Add extends Command {
   public Add() {
      super("friendadd", "<name alias>", new String[]{"fadd", "add"});
   }

   public void run(String message) {
      String name = message.split(" ")[1];
      String alias = message.substring((message.split(" ")[0] + " " + name + " ").length());
      Noxious.getFriendManager().addFriend(name, alias);
      Logger.logChat("Friend " + name + " added with the alias of " + alias + ".");
      if(Noxious.getFileManager().getFileByName("friends") != null) {
         Noxious.getFileManager().getFileByName("friends").saveFile();
      }

   }
}
