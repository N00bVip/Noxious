package noxious.command.commands;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.Listener;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.util.Logger;

public final class Teleport extends Command {
   public Listener listener = new Listener() {
      public void onEvent(Event event) {
         if(event instanceof EventPreSendMotionUpdates) {
            EventPreSendMotionUpdates pre = (EventPreSendMotionUpdates)event;
            pre.setCancelled(true);
            if(Teleport.this.mc.thePlayer.ridingEntity == null) {
               Noxious.getEventManager().removeListener(this);
            }
         }

      }
   };
   private final Minecraft mc = Minecraft.getMinecraft();

   public Teleport() {
      super("teleport", "<x> <y> <z>", new String[]{"tp"});
   }

   public void run(String message) {
      String[] arguments = message.split(" ");
      double x = Double.parseDouble(arguments[1]);
      double y = Double.parseDouble(arguments[2]);
      double z = Double.parseDouble(arguments[3]);
      if(this.mc.thePlayer.ridingEntity != null) {
         Noxious.getEventManager().addListener(this.listener);
      }

      this.mc.thePlayer.setPosition(x, y, z);
      this.mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y, y + 1.0D, this.mc.thePlayer.onGround));
      Logger.logChat("You have been teleported to " + x + ", " + y + ", " + z);
      if(this.mc.thePlayer.ridingEntity != null) {
         for(int index = 0; index < 20; ++index) {
            this.mc.playerController.attackEntity(this.mc.thePlayer, this.mc.thePlayer.ridingEntity);
         }
      }

   }
}
