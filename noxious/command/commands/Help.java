package noxious.command.commands;

import java.util.Arrays;
import java.util.Iterator;

import noxious.Noxious;
import noxious.command.Command;
import noxious.util.Logger;


public final class Help extends Command {
   public Help() {
      super("help", "none", new String[]{"cmds"});
   }

   public void run(String message) {
      String[] arguments = message.split(" ");
      if(arguments.length == 1) {
         StringBuilder command2 = new StringBuilder("Commands: ");
         Iterator var5 = Noxious.getCommandManager().getContents().iterator();

         while(var5.hasNext()) {
            Command command1 = (Command)var5.next();
            command2.append(command1.getCommand() + ", ");
         }

         Logger.logChat(command2.substring(0, command2.length() - 2).toString());
         Logger.logChat("§aNOTE:§f Most commands have shorter versions, type .help <command> to see it\'s aliases.");
      } else {
         Command command21 = Noxious.getCommandManager().getCommandByName(arguments[1]);
         if(command21 == null) {
            Logger.logChat("Command \"." + arguments[1] + "\" was not found!");
         } else {
            Logger.logChat("Command: " + command21.getCommand());
            if(command21.getAliases().length != 0) {
               Logger.logChat("Aliases: " + Arrays.toString(command21.getAliases()));
            }

            if(!command21.getArguments().equals("none")) {
               Logger.logChat("Arguments: " + command21.getArguments());
            }
         }
      }

   }
}
