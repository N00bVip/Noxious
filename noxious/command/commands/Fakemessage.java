package noxious.command.commands;

import noxious.command.Command;
import noxious.util.Logger;

public class Fakemessage extends Command {
   public Fakemessage() {
      super("fakemsg", "<msg>", new String[]{"fmsg"});
   }

   public void run(String message) {
      String msg = message.split(" ")[1];
      Logger.logChatWithoutnoxious(msg);
   }
}
