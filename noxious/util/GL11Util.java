package noxious.util;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

public class GL11Util {

	public static void setColor(Color c)
	  {
	    GL11.glColor4f(c.getRed() / 255.0F, c.getGreen() / 255.0F, c.getBlue() / 255.0F, 1.0F);
	  }

	  public static void setColor(int rgba)
	  {
	    int r = rgba & 0xFF;int g = rgba >> 8 & 0xFF;int b = rgba >> 16 & 0xFF;int a = rgba >> 24 & 0xFF;
	    GL11.glColor4b((byte)r, (byte)g, (byte)b, (byte)a);
	  }

	  public static void a(float xPos, float yPos, int innerColor, int outerColor, int slices, float radius)
	  {
	    float incr = (float)(6.283185307179586D / slices);
	    GL11.glBegin(6);
	    setColor(innerColor);
	    GL11.glVertex2f(xPos, yPos);
	    setColor(outerColor);
	    for (int i = 0; i < slices; i++)
	    {
	      float angle = incr * i;
	      float x = (float)Math.cos(angle) * radius;
	      float y = (float)Math.sin(angle) * radius;
	      GL11.glVertex2f(x, y);
	    }
	    GL11.glVertex2f(radius, 0.0F);

	    GL11.glEnd();
	  }

}
