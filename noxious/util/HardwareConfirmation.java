package noxious.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;
import java.util.UUID;


public class HardwareConfirmation {

	private static String uuid;
	private static Boolean confirmation = false;
	private static Boolean banned = false;

	@SuppressWarnings("resource")
	public static Boolean onConfirmation(String username) throws IOException {
		URL urlAllownedUUID = new URL("https://18ce3ed09af482c1d20ed78c9b412c17a7436125.googledrive.com/host/0B5s5Ftz_m8nWMlJZbzN2OURjT00/Users/" + username + "/HWID.txt");
		Scanner allownedUUID = new Scanner(urlAllownedUUID.openStream());
		if (urlAllownedUUID != null && allownedUUID.nextLine() != null) {
			while (allownedUUID.hasNext()) {
				String sn1 = allownedUUID.nextLine().toString();
				if (confirmation = Boolean.valueOf(sn1.equals(uuid))) {
					break;
				}
			}
			return confirmation;
		} else {
			return false;
		}
	}

	@SuppressWarnings("resource")
	public static boolean isBanned() throws IOException {
		URL urlBannedUUID = new URL("https://18ce3ed09af482c1d20ed78c9b412c17a7436125.googledrive.com/host/0B5s5Ftz_m8nWMlJZbzN2OURjT00/RocketBannedUUID.txt");
		Scanner bannedUUID = new Scanner(urlBannedUUID.openStream());
		banned = false;
		if (urlBannedUUID != null && bannedUUID.nextLine() != null) {
			while (bannedUUID.hasNext()) {
				String sn1 = bannedUUID.nextLine().toString();
				if (banned = Boolean.valueOf(sn1.equals(uuid))) {
			    	  break;
			      }

			}
			Logger.logConsole("Banned Status: " + banned);
			return banned;
		} else {
			Logger.logConsole("Banned Status: false");
			return false;
		}
	}

	public static final String onGenerateUUID() {
		  String result = "";
		    try {
		      File file = File.createTempFile("realhowto",".vbs");
		      file.deleteOnExit();
		      FileWriter fw = new java.io.FileWriter(file);

		      String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
		                  +"Set colDrives = objFSO.Drives\n"
		                  +"Set objDrive = colDrives.item(\"" + "C:" + "\")\n"
		                  +"Wscript.Echo objDrive.SerialNumber";
		      fw.write(vbs);
		      fw.close();
		      Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
		      BufferedReader input =
		        new BufferedReader
		          (new InputStreamReader(p.getInputStream()));
		      String line;
		      while ((line = input.readLine()) != null) {
		         result += line;
		      }
		      input.close();
		    }
		    catch(Exception e){
		        e.printStackTrace();
		    }
		    UUID u2 = UUID.nameUUIDFromBytes(result.getBytes());
		    uuid = u2.toString();
		    Logger.logConsole("Successfully generated UUID:" + uuid);
		    return uuid;
	}

	public static String getUUID() {
		return uuid;
	}
}
