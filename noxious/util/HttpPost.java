package noxious.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import noxious.threads.NoxiousThread;

public class HttpPost {

	public static String executePost(String urltstring, Map<String, String>param) {
		String result = "";
        try {
            URL url = new URL(urltstring);

            HttpURLConnection connection = null;

            try {

            	StringBuilder paramBuilder = new StringBuilder();
        		boolean notfirst = false;
        		for(Map.Entry<String, String> entry : param.entrySet()){
        			if(!notfirst){
        				notfirst = true;
        			}else{
        				paramBuilder.append("&");
        			}
        			paramBuilder.append(entry.getKey() + "=" + entry.getValue());
        		}
        		String convertedparam = paramBuilder.toString();

                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8));
                writer.write(convertedparam);
                writer.flush();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    try (InputStreamReader isr = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
                    	BufferedReader reader = new BufferedReader(isr)) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                        	result += line;
                        }
                    }
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

	public static String executePost(String urltstring) {
		String result = "";
        try {
            URL url = new URL(urltstring);

            HttpURLConnection connection = null;

            try {

                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8));
                writer.flush();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    try (InputStreamReader isr = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
                    	BufferedReader reader = new BufferedReader(isr)) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                        	result += line;
                        }
                    }
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

	public static String getUserType() {
		String result = "";
        try {
            URL url = new URL("http://noxious.ml/getusertype.php");

            HttpURLConnection connection = null;

            Map<String, String>param = new HashMap<String, String>();
    		param.put("username", NoxiousThread.getUsername());

            try {

            	StringBuilder paramBuilder = new StringBuilder();
        		boolean notfirst = false;
        		for(Map.Entry<String, String> entry : param.entrySet()){
        			if(!notfirst){
        				notfirst = true;
        			}else{
        				paramBuilder.append("&");
        			}
        			paramBuilder.append(entry.getKey() + "=" + entry.getValue());
        		}
        		String convertedparam = paramBuilder.toString();

                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8));
                writer.write(convertedparam);
                writer.flush();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    try (InputStreamReader isr = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
                    	BufferedReader reader = new BufferedReader(isr)) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                        	result += line;
                        }
                    }
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

	public static String getUserType(String username) {
		String result = "";
        try {
            URL url = new URL("http://noxious.ml/getusertype.php");

            HttpURLConnection connection = null;

            Map<String, String>param = new HashMap<String, String>();
    		param.put("username", username);

            try {

            	StringBuilder paramBuilder = new StringBuilder();
        		boolean notfirst = false;
        		for(Map.Entry<String, String> entry : param.entrySet()){
        			if(!notfirst){
        				notfirst = true;
        			}else{
        				paramBuilder.append("&");
        			}
        			paramBuilder.append(entry.getKey() + "=" + entry.getValue());
        		}
        		String convertedparam = paramBuilder.toString();

                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), StandardCharsets.UTF_8));
                writer.write(convertedparam);
                writer.flush();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    try (InputStreamReader isr = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
                    	BufferedReader reader = new BufferedReader(isr)) {
                        String line;
                        while ((line = reader.readLine()) != null) {
                        	result += line;
                        }
                    }
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
