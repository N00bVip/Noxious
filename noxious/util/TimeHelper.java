package noxious.util;

public final class TimeHelper
{
    private long lastMS;

    public long getCurrentMS()
    {
        return System.nanoTime() / 1000000L;
    }

    public long getLastMS()
    {
        return this.lastMS;
    }

    public boolean hasReached(float f)
    {
        return (float)(this.getCurrentMS() - this.lastMS) >= f;
    }

    public void reset()
    {
        this.lastMS = this.getCurrentMS();
    }

    public void setLastMS(long currentMS)
    {
        this.lastMS = currentMS;
    }
}
