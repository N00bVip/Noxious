package noxious.util;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;
import noxious.Noxious;

public final class Logger
{
    private static Minecraft mc = Minecraft.getMinecraft();

    public static void logChatWithoutnoxious(String message)
    {
        if (mc.thePlayer == null)
        {
            logConsole(message);
        }
        else if (Noxious.getModManager().getModByName("ghosthud").isEnabled())
        {
            logConsole("");
        }
        else
        {
            String name = mc.session.getUsername();
            mc.thePlayer.addChatMessage(new ChatComponentText(name + message));
        }
    }

    public static void logChat(String message)
    {
        if (mc.thePlayer == null)
        {
            logConsole(message);
        }
        else
        {
            mc.thePlayer.addChatMessage(new ChatComponentText("§7[§aNoxious§7]§r " + message));
        }
    }

    public static void logConsole(String message)
    {
        System.out.println("[Noxious]" + message);
    }

    public static void logIRC(String message) {
        if (mc.thePlayer == null)
        {
            logConsole(message);
        }
        else
        {
        	logConsole(message);
            mc.thePlayer.addChatMessage(new ChatComponentText("§b[IRC]§f " + message));
        }
    }
}
