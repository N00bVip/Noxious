package noxious.util;

import java.util.Iterator;

public class FlexibleArray<T> implements Iterable<T>
{
    private T[] elements;

    public FlexibleArray(T[] array)
    {
        this.elements = array;
    }

    public FlexibleArray()
    {
        this.elements = (T[]) new Object[0];
    }

    public void add(T t)
    {
        if (t != null)
        {
            Object[] array = new Object[this.size() + 1];

            for (int i = 0; i < array.length; ++i)
            {
                if (i < this.size())
                {
                    array[i] = this.get(i);
                }
                else
                {
                    array[i] = t;
                }
            }

            this.set((T[]) array);
        }
    }

    public void remove(T t)
    {
        if (this.contains(t))
        {
            Object[] array = new Object[this.size() - 1];
            boolean b = true;

            for (int i = 0; i < this.size(); ++i)
            {
                if (b && this.get(i).equals(t))
                {
                    b = false;
                }
                else
                {
                    array[b ? i : i - 1] = this.get(i);
                }
            }

            this.set((T[]) array);
        }
    }

    public boolean contains(T t)
    {
        Object[] var5;
        int var4 = (var5 = this.array()).length;

        for (int var3 = 0; var3 < var4; ++var3)
        {
            Object entry = var5[var3];

            if (entry.equals(t))
            {
                return true;
            }
        }

        return false;
    }

    private void set(T[] array)
    {
        this.elements = array;
    }

    public void clear()
    {
        this.elements = (T[]) new Object[0];
    }

    public T get(int index)
    {
        return this.array()[index];
    }

    public int size()
    {
        return this.array().length;
    }

    public T[] array()
    {
        return this.elements;
    }

    public boolean isEmpty()
    {
        return this.size() == 0;
    }

    public Iterator<T> iterator()
    {
        return new Iterator()
        {
            private int index = 0;
            public boolean hasNext()
            {
                return this.index < FlexibleArray.this.size() && FlexibleArray.this.get(this.index) != null;
            }
            public T next()
            {
                return FlexibleArray.this.get(this.index++);
            }
            public void remove()
            {
                FlexibleArray.this.remove(FlexibleArray.this.get(this.index));
            }
        };
    }
}
