package noxious.util;

import net.minecraft.util.MathHelper;

public final class RotationUtils
{
    private static float silentYaw = 0.0F;
    private static float realYaw = 0.0F;

    public static float getRealYaw()
    {
        return realYaw;
    }

    public static void setRealYaw(float yaw)
    {
        realYaw = yaw;
    }

    public static float getSilentYaw()
    {
        return silentYaw;
    }

    public static void setSilentYaw(float yaw)
    {
        silentYaw = yaw;
    }

    public static Object[] updateRotation(float current, float target, int maxIncrease)
    {
        float angle = MathHelper.wrapAngleTo180_float(target - current);
        boolean aiming = true;

        if (angle > (float)maxIncrease)
        {
            angle = (float)maxIncrease;
            aiming = false;
        }

        if (angle < (float)(-maxIncrease))
        {
            angle = (float)(-maxIncrease);
            aiming = false;
        }

        return new Object[] {Float.valueOf(current + angle), Boolean.valueOf(aiming)};
    }
}
