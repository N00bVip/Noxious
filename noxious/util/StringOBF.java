package noxious.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.internet.MimeUtility;

public class StringOBF {

	public static String encrypt(String key, String text) throws Exception {

		SecretKeySpec sksSpec =
				new SecretKeySpec(key.getBytes(), "Blowfish");

		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, sksSpec);

		return encodeBase64(cipher.doFinal(text.getBytes()));
	}

	public static String encodeBase64(byte[] data) throws Exception {

	    ByteArrayOutputStream forEncode = new ByteArrayOutputStream();

	    OutputStream toBase64 = MimeUtility.encode(forEncode, "base64");
	    toBase64.write(data);
	    toBase64.close();

	    return forEncode.toString("iso-8859-1");
	}

	public static String encodeBase64(String string) throws Exception {

	    ByteArrayOutputStream forEncode = new ByteArrayOutputStream();

	    OutputStream toBase64 = MimeUtility.encode(forEncode, "base64");
	    toBase64.write(string.getBytes());
	    toBase64.close();

	    return forEncode.toString("iso-8859-1");
	}


	public static String decrypt(String key, String codeBase64)
		    throws Exception {

		    SecretKeySpec sksSpec =
		        new SecretKeySpec(key.getBytes(), "Blowfish");

		    Cipher cipher = Cipher.getInstance("Blowfish");
		    cipher.init(Cipher.DECRYPT_MODE, sksSpec);

		    return new String(cipher.doFinal(decodeBase64(codeBase64)));
		}

	public static byte[] decodeBase64(String base64) throws Exception {

		InputStream fromBase64 = MimeUtility.decode(
				new ByteArrayInputStream(base64.getBytes()), "base64");

		byte[] buf = new byte[1024];
		ByteArrayOutputStream toByteArray = new ByteArrayOutputStream();

		for (int len = -1;(len = fromBase64.read(buf)) != -1;)
			toByteArray.write(buf, 0, len);

		return toByteArray.toByteArray();
	}

	public static String decodeBase641(String base64) throws Exception {

		InputStream fromBase64 = MimeUtility.decode(
				new ByteArrayInputStream(base64.getBytes()), "base64");

		byte[] buf = new byte[1024];
		ByteArrayOutputStream toByteArray = new ByteArrayOutputStream();

		for (int len = -1;(len = fromBase64.read(buf)) != -1;)
			toByteArray.write(buf, 0, len);

		return toByteArray.toString();
	}

}
