package noxious.util;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S08PacketPlayerPosLook;
import net.minecraft.network.play.server.S3FPacketCustomPayload;
import noxious.Noxious;
import noxious.event.Event;
import noxious.event.Listener;
import noxious.event.events.EventPacketReceive;
import noxious.event.events.EventPacketSent;

public class ListenerUtil implements Listener {

   private Minecraft mc = Minecraft.getMinecraft();
   public static PositionData serverPos = new PositionData();
   public static String serverBrand = "Server";


   public ListenerUtil() {
      Noxious.getEventManager().addListener(this);
      if(this.mc.thePlayer != null) {
         serverPos.posX = this.mc.thePlayer.posX;
         serverPos.posY = this.mc.thePlayer.posY;
         serverPos.posZ = this.mc.thePlayer.posZ;
         serverPos.rotationYaw = this.mc.thePlayer.rotationYaw;
         serverPos.rotationPitch = this.mc.thePlayer.rotationPitch;
      }

   }

   public void onEvent(Event event) {
      if(event instanceof EventPacketReceive) {
    	  EventPacketReceive sent = (EventPacketReceive)event;
         if(sent.getPacket() instanceof S08PacketPlayerPosLook) {
            S08PacketPlayerPosLook prepacket = (S08PacketPlayerPosLook)sent.getPacket();
            serverPos.posX = prepacket.func_148932_c();
            serverPos.posY = prepacket.func_148928_d();
            serverPos.posZ = prepacket.func_148933_e();
            serverPos.rotationYaw = prepacket.func_148931_f();
            serverPos.rotationPitch = prepacket.func_148930_g();
         }

         if(sent.getPacket() instanceof S3FPacketCustomPayload) {
            S3FPacketCustomPayload prepacket1 = (S3FPacketCustomPayload)sent.getPacket();
            if(prepacket1.getChannelName().equals("MC|Brand")) {
               try {
                  serverBrand = prepacket1.getBufferData().readStringFromBuffer(prepacket1.getBufferData().readableBytes());
               } catch (Exception var5) {
                  var5.printStackTrace();
               }
            }
         }
      } else if(event instanceof EventPacketSent) {
         EventPacketSent sent1 = (EventPacketSent)event;
         C03PacketPlayer prepacket2;
         if(sent1.getPacket() instanceof C03PacketPlayer) {
            prepacket2 = (C03PacketPlayer)sent1.getPacket();
            C03PacketPlayer.C06PacketPlayerPosLook packet = new C03PacketPlayer.C06PacketPlayerPosLook(prepacket2.field_149480_h?prepacket2.x:serverPos.posX, prepacket2.field_149480_h?prepacket2.y:serverPos.posY, prepacket2.field_149480_h?prepacket2.z:serverPos.posZ, prepacket2.rotating?prepacket2.getYaw():serverPos.rotationYaw, prepacket2.rotating?prepacket2.getPitch():serverPos.rotationPitch, prepacket2.field_149474_g);
            packet.rotating = prepacket2.rotating;
            packet.field_149480_h = prepacket2.field_149480_h;
            sent1.setPacket(packet);
         }

         if(sent1.getPacket() instanceof C03PacketPlayer) {
            prepacket2 = (C03PacketPlayer)sent1.getPacket();
            if(prepacket2.rotating) {
               serverPos.rotationYaw = prepacket2.getYaw();
               serverPos.rotationPitch = prepacket2.getPitch();
            }

            if(prepacket2.field_149480_h) {
               serverPos.posX = prepacket2.getPositionX();
               serverPos.posY = prepacket2.getPositionY();
               serverPos.posZ = prepacket2.getPositionZ();
            }
         }
      }

   }
}
