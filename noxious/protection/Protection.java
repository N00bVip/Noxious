package noxious.protection;

import java.io.IOException;

import net.minecraft.client.Minecraft;
import noxious.ui.screens.GuiNoxiousMainMenu;

public class Protection
{
    public static void setup() throws IOException {
        Minecraft.getMinecraft().displayGuiScreen(new GuiNoxiousMainMenu());
    }
}
