package noxious.mod.mods;

import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.item.ItemSoup;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.util.BlockPos;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class AutoSoup extends Mod {

   private final Value<Long> delay = new Value("autosoup_delay", Long.valueOf(500L));
   private final Value<Float> health = new Value("autosoup_health", Float.valueOf(13.0F));
   private final TimeHelper time = new TimeHelper();


   public AutoSoup() {
      super("AutoSoup", -393339, ModManager.Category.PLAYER);
      Noxious.getCommandManager().getContents().add(new Command("autosoupdelay", "<milliseconds>", new String[]{"soupdelay", "asd"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               AutoSoup.this.delay.setValue((Long)AutoSoup.this.delay.getDefaultValue());
            } else {
               AutoSoup.this.delay.setValue(Long.valueOf(Long.parseLong(message.split(" ")[1])));
            }

            if(((Long)AutoSoup.this.delay.getValue()).longValue() > 1000L) {
               AutoSoup.this.delay.setValue(Long.valueOf(1000L));
            } else if(((Long)AutoSoup.this.delay.getValue()).longValue() < 1L) {
               AutoSoup.this.delay.setValue(Long.valueOf(1L));
            }

            Logger.logChat("Auto Soup Delay set to: " + AutoSoup.this.delay.getValue());
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("autosouphealth", "<health>", new String[]{"souphealth", "ash"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               AutoSoup.this.health.setValue((Float)AutoSoup.this.health.getDefaultValue());
            } else {
               AutoSoup.this.health.setValue(Float.valueOf(Float.parseFloat(message.split(" ")[1])));
            }

            if(((Float)AutoSoup.this.health.getValue()).floatValue() < 1.0F) {
               AutoSoup.this.health.setValue(Float.valueOf(1.0F));
            }

            Logger.logChat("Auto Soup Health set to: " + AutoSoup.this.health.getValue());
         }
      });
   }

   private boolean isHotbarFull() {
      for(int index = 36; index < 45; ++index) {
         ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
         if(stack == null) {
            return false;
         }
      }

      return true;
   }

   private boolean doesHotbarHaveSoups() {
      for(int index = 36; index < 45; ++index) {
         ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
         if(stack != null && this.isStackSoup(stack)) {
            return true;
         }
      }

      return false;
   }

   private void eatAndDropSoup() {
      for(int index = 36; index < 45; ++index) {
         ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
         if(stack != null && this.isStackSoup(stack)) {
            this.stackBowls();
            int oldslot = mc.thePlayer.inventory.currentItem;
            mc.getNetHandler().addToSendQueue(new C09PacketHeldItemChange(index - 36));
            mc.playerController.updateController();
            mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(new BlockPos(-1, -1, -1), -1, stack, 0.0F, 0.0F, 0.0F));
            mc.getNetHandler().addToSendQueue(new C09PacketHeldItemChange(oldslot));
            break;
         }
      }

   }

   private void getSoupFromInventory() {
      if(!(mc.currentScreen instanceof GuiChest)) {
         this.stackBowls();

         for(int index = 9; index < 36; ++index) {
            ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
            if(stack != null && this.isStackSoup(stack)) {
               mc.playerController.windowClick(0, index, 0, 1, mc.thePlayer);
               break;
            }
         }

      }
   }

   private boolean isStackSoup(ItemStack stack) {
      return stack == null?false:stack.getItem() instanceof ItemSoup;
   }

   public void onEvent(Event event) {
      if(event instanceof EventPostSendMotionUpdates) {
         if(this.updateCounter() == 0) {
            return;
         }

         if(mc.thePlayer.getHealth() <= ((Float)this.health.getValue()).floatValue() && this.time.hasReached(((Long)this.delay.getValue()).longValue())) {
            if(this.doesHotbarHaveSoups()) {
               this.eatAndDropSoup();
            } else {
               this.getSoupFromInventory();
            }

            this.time.reset();
         }
      }

   }

   private void stackBowls() {}

   private int updateCounter() {
      int counter = 0;

      for(int index = 9; index < 45; ++index) {
         ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
         if(stack != null && this.isStackSoup(stack)) {
            counter += stack.stackSize;
         }
      }

      this.setTag("Auto Soup \u00a77" + counter);
      return counter;
   }
}
