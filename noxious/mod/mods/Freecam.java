package noxious.mod.mods;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.network.play.client.C03PacketPlayer;
import noxious.event.Event;
import noxious.event.events.EventEntityUpdates;
import noxious.event.events.EventInsideBlock;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPlayerMovement;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventPushOutOfBlocks;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;

public class Freecam extends Mod {

   private double x;
   private double y;
   private double minY;
   private double z;
   private float yaw;
   private float pitch;


   public Freecam() {
      super("Freecam", -5192482, ModManager.Category.WORLD);
      this.setEnabled(false);
   }

   public void onDisabled() {
      super.onDisabled();
      if(mc.thePlayer != null) {
         mc.thePlayer.noClip = false;
         mc.theWorld.removeEntityFromWorld(-1);
         mc.thePlayer.setPositionAndRotation(this.x, this.y, this.z, this.yaw, this.pitch);
      }

   }

   public void onEnabled() {
      super.onEnabled();
      if(mc.thePlayer != null) {
         this.x = mc.thePlayer.posX;
         this.y = mc.thePlayer.posY;
         this.minY = mc.thePlayer.boundingBox.minY;
         this.z = mc.thePlayer.posZ;
         this.yaw = mc.thePlayer.rotationYaw;
         this.pitch = mc.thePlayer.rotationPitch;
         EntityOtherPlayerMP ent = new EntityOtherPlayerMP(mc.theWorld, mc.thePlayer.getGameProfile());
         ent.inventory = mc.thePlayer.inventory;
         ent.inventoryContainer = mc.thePlayer.inventoryContainer;
         ent.setPositionAndRotation(this.x, this.minY, this.z, this.yaw, this.pitch);
         ent.rotationYawHead = mc.thePlayer.rotationYawHead;
         mc.theWorld.addEntityToWorld(-1, ent);
         mc.thePlayer.noClip = true;
      }

   }

   public void onEvent(Event event) {
      if(event instanceof EventPlayerMovement) {
         EventPlayerMovement sent = (EventPlayerMovement)event;
         double player = 4.0D;
         if(Keyboard.isKeyDown(29)) {
            player /= 2.0D;
         }

         sent.setY(0.0D);
         mc.thePlayer.motionY = 0.0D;
         if(!mc.inGameHasFocus) {
            return;
         }

         if(mc.thePlayer.movementInput.moveForward != 0.0F || mc.thePlayer.movementInput.moveStrafe != 0.0F) {
            sent.setX(sent.getX() * player);
            sent.setZ(sent.getZ() * player);
         }

         if(Keyboard.isKeyDown(mc.gameSettings.keyBindJump.getKeyCode())) {
            sent.setY(player / 4.0D);
         } else if(Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode())) {
            sent.setY(-(player / 4.0D));
         }
      } else if(event instanceof EventInsideBlock) {
         ((EventInsideBlock)event).setCancelled(true);
      } else if(event instanceof EventPushOutOfBlocks) {
         ((EventPushOutOfBlocks)event).setCancelled(true);
      } else if(event instanceof EventPreSendMotionUpdates) {
         mc.thePlayer.renderArmPitch += 400.0F;
      } else if(event instanceof EventEntityUpdates && mc.thePlayer != null) {
         mc.thePlayer.noClip = true;
      } else if(event instanceof EventPacketSent) {
         EventPacketSent sent1 = (EventPacketSent)event;
         if(sent1.getPacket() instanceof C03PacketPlayer) {
            C03PacketPlayer player1 = (C03PacketPlayer)sent1.getPacket();
            player1.x = this.x;
            player1.y = this.y;
            player1.z = this.z;
            player1.yaw = this.yaw;
            player1.pitch = this.pitch;
         }
      }

   }
}
