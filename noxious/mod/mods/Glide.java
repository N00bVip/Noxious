package noxious.mod.mods;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPlayerMovement;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class Glide extends Mod {

   private Value<Float> motion = new Value("glide_motion", Float.valueOf(1.0F));
   private final TimeHelper time = new TimeHelper();
   private boolean shouldCancelPacket = false;
   private int ticks = 0;


   public Glide() {
      super("Glide", -9868951, ModManager.Category.MOVEMENT);
      Noxious.getCommandManager().getContents().add(new Command("glidemotion", "<factor>", new String[]{"glidem", "gm"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               Glide.this.motion.setValue((Float)Glide.this.motion.getDefaultValue());
            } else {
               Glide.this.motion.setValue(Float.valueOf(Float.parseFloat(message.split(" ")[1])));
            }

            if (!type.equals("0")) {
	            if(((Float)Glide.this.motion.getValue()).floatValue() > 5.0F) {
	               Glide.this.motion.setValue(Float.valueOf(5.0F));
	            } else if(((Float)Glide.this.motion.getValue()).floatValue() < 0.0F) {
	               Glide.this.motion.setValue(Float.valueOf(1.0E-12F));
	            }

	            Logger.logChat("Glide Motion Speed has been multiplied by: " + Glide.this.motion.getValue());
	         } else {
	        	 if(((Float)Glide.this.motion.getValue()).floatValue() > 5.0F) {
		               Glide.this.motion.setValue(Float.valueOf(5.0F));
		            } else if(((Float)Glide.this.motion.getValue()).floatValue() < 1.0F) {
		               Glide.this.motion.setValue(Float.valueOf(1.0F));
		            }

		            Logger.logChat("Glide Motion Speed has been multiplied by: " + Glide.this.motion.getValue());
	         }
         }
      });
   }

   public void onDisabled() {
      super.onDisabled();
      if(mc.thePlayer != null) {
         mc.timer.timerSpeed = 1.0F;
      }

   }

   public void onEnabled() {
      super.onEnabled();
      if (type.equals("0")) {
	      if(((Float)Glide.this.motion.getValue()).floatValue() > 5.0F) {
	          Glide.this.motion.setValue(Float.valueOf(5.0F));
	       } else if(((Float)Glide.this.motion.getValue()).floatValue() < 1.0F) {
	          Glide.this.motion.setValue(Float.valueOf(1.0F));
	       }
      }
      if(mc.thePlayer != null) {
         this.time.reset();
         double x = mc.thePlayer.posX;
         double y = mc.thePlayer.posY;
         double z = mc.thePlayer.posZ;

         for(int i = 0; i < 4; ++i) {
            Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y + 1.01D, z, false));
            Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y, z, false));
         }

         Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y + 0.8D, z, false));
      }

   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
         if(mc.thePlayer.onGround) {
            this.setColor(-9868951);
         } else {
            this.setColor(-12004916);
         }

         if(!mc.thePlayer.capabilities.isFlying && mc.thePlayer.fallDistance > 0.0F && !mc.thePlayer.isSneaking() && this.time.hasReached(500L)) {
            mc.thePlayer.motionY = -0.03127D;
         }

         if(mc.gameSettings.keyBindSneak.pressed) {
            mc.thePlayer.motionY = -0.5D;
         }
      } else if(event instanceof EventPlayerMovement && this.time.hasReached(500L)) {
         if(!mc.gameSettings.keyBindSneak.pressed) {
            ((EventPlayerMovement)event).setY(((EventPlayerMovement)event).getY() * (double)((Float)this.motion.getValue()).floatValue());
         }
      } else if(event instanceof EventPacketSent) {
         EventPacketSent var2 = (EventPacketSent)event;
      }

   }
}
