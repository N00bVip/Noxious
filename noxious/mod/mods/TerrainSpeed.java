package noxious.mod.mods;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLadder;
import net.minecraft.block.BlockVine;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPlayerMovement;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class TerrainSpeed extends Mod
{
    private final Value<Boolean> fastice = new Value("speed_fastice", Boolean.valueOf(true));
    private final Value<Boolean> fastladder = new Value("speed_fastladder", Boolean.valueOf(true));

    public TerrainSpeed()
    {
        super("TerrainSpeed");
        this.setEnabled(true);
        this.setCategory(Category.MOVEMENT);
        Noxious.getCommandManager().getContents().add(new Command("terrainspeed", "<fastice/fastladder>", new String[0])
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("fastice"))
                {
                    TerrainSpeed.this.fastice.setValue(Boolean.valueOf(!((Boolean)TerrainSpeed.this.fastice.getValue()).booleanValue()));
                    Logger.logChat("Terrain Speed will " + (((Boolean)TerrainSpeed.this.fastice.getValue()).booleanValue() ? "now" : "no longer") + " speed up on ice.");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("fastladder"))
                {
                    TerrainSpeed.this.fastladder.setValue(Boolean.valueOf(!((Boolean)TerrainSpeed.this.fastladder.getValue()).booleanValue()));
                    Logger.logChat("Terrain Speed will " + (((Boolean)TerrainSpeed.this.fastladder.getValue()).booleanValue() ? "now" : "no longer") + " go fast on ladders.");
                }
                else
                {
                    Logger.logChat("Option not valid! Available options: fastice, fastladder.");
                }
            }
        });
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventPlayerMovement)
        {
            EventPlayerMovement movement = (EventPlayerMovement)event;

            if (((Boolean)this.fastladder.getValue()).booleanValue())
            {
                TimeHelper time = new TimeHelper();
                boolean movementInput = mc.gameSettings.keyBindForward.pressed || mc.gameSettings.keyBindBack.pressed || mc.gameSettings.keyBindLeft.pressed || mc.gameSettings.keyBindLeft.pressed;
                int posX = MathHelper.floor_double(mc.thePlayer.posX);
                int minY = MathHelper.floor_double(mc.thePlayer.boundingBox.minY);
                int maxY = MathHelper.floor_double(mc.thePlayer.boundingBox.minY + 1.0D);
                int posZ = MathHelper.floor_double(mc.thePlayer.posZ);

                if (movementInput && mc.thePlayer.isCollidedHorizontally && !mc.thePlayer.isInWater())
                {
                    if (!mc.thePlayer.isOnLadder())
                    {
                        Block thePlayer = BlockHelper.getBlockAtPos(new BlockPos((double)posX, (double)minY, (double)posZ));

                        if (!(thePlayer instanceof BlockLadder) && !(thePlayer instanceof BlockVine))
                        {
                            thePlayer = BlockHelper.getBlockAtPos(new BlockPos((double)posX, (double)maxY, (double)posZ));

                            if (thePlayer instanceof BlockLadder || thePlayer instanceof BlockVine)
                            {
                                mc.thePlayer.motionY = 0.5D;
                            }
                        }
                    }
                    else if (mc.thePlayer.isOnLadder())
                    {
                        EntityPlayerSP thePlayer1 = mc.thePlayer;
                        movement.setY(movement.getY() * 2.25D);
                        mc.thePlayer.motionY += 0.1D;

                        if (mc.thePlayer.onGround && this.getAboveLadders() > 0 && !mc.thePlayer.isSneaking() && time.hasReached(1000L))
                        {
                            mc.thePlayer.setLocationAndAngles(mc.thePlayer.posX, (double)(this.getAboveLadders() > 9 ? 9 : this.getAboveLadders()) + mc.thePlayer.posY, mc.thePlayer.posZ, mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch);
                            time.reset();
                        }
                    }
                }
            }

            if (((Boolean)this.fastice.getValue()).booleanValue() && BlockHelper.isOnIce())
            {
                Blocks.ice.slipperiness = 0.6F;
                Blocks.packed_ice.slipperiness = 0.6F;
                movement.setX(movement.getX() * 2.5D);
                movement.setZ(movement.getZ() * 2.5D);
            }
            else
            {
                Blocks.ice.slipperiness = 0.98F;
                Blocks.packed_ice.slipperiness = 0.98F;
            }
        }
    }

    private int getAboveLadders()
    {
        int ladders = 0;

        for (int dist = 1; dist < 256; ++dist)
        {
            BlockPos bpos = new BlockPos(mc.thePlayer.posX, mc.thePlayer.posY + (double)dist, mc.thePlayer.posZ);
            Block block = mc.theWorld.getBlockState(bpos).getBlock();

            if (!(block instanceof BlockLadder) && !(block instanceof BlockVine))
            {
                break;
            }

            ++ladders;
        }

        return ladders;
    }
}
