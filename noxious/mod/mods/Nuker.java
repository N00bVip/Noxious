package noxious.mod.mods;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventBlockBreaking;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.util.RenderHelper;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class Nuker extends Mod {

   private BlockPos blockBreaking;
   private TimeHelper time = new TimeHelper();
   private Block selectedBlock;
   private Block globalBlock;
   private final Value<Boolean> selective = new Value("nuker_selective", Boolean.valueOf(true));


   public Nuker() {
      super("Nuker", -7077677, ModManager.Category.WORLD);
      Noxious.getCommandManager().getContents().add(new Command("nukerselective", "none", new String[]{"nukersel", "ns"}) {
         public void run(String message) {
            Nuker.this.selective.setValue(Boolean.valueOf(!((Boolean)Nuker.this.selective.getValue()).booleanValue()));
            Logger.logChat("Nuker will " + (((Boolean)Nuker.this.selective.getValue()).booleanValue()?"now":"no longer") + " only attack the last selected block.");
         }
      });
   }

   private double getDistance(double x, double y, double z) {
      float xDiff = (float)(mc.thePlayer.posX - x);
      float yDiff = (float)(mc.thePlayer.posY - y);
      float zDiff = (float)(mc.thePlayer.posZ - z);
      return (double)MathHelper.sqrt_float(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);
   }

   public void onEvent(Event event) {
      if(event instanceof EventBlockBreaking) {
         EventBlockBreaking x = (EventBlockBreaking)event;
         if(x.getState() == EventBlockBreaking.EnumBlock.CLICK && x.getPos() != null) {
            this.selectedBlock = BlockHelper.getBlockAtPos(x.getPos());
         }
      } else if(event instanceof EventPreSendMotionUpdates) {
    	  EventPreSendMotionUpdates var15 = (EventPreSendMotionUpdates)event;

         for(int y = 6; y >= -1; --y) {
            for(int y1 = -6; y1 <= 6; ++y1) {
               for(int z = -6; z <= 6; ++z) {
                  if(y1 != 0 || z != 0) {
                     BlockPos z1 = new BlockPos(mc.thePlayer.posX + (double)y1, mc.thePlayer.posY + (double)y, mc.thePlayer.posZ + (double)z);
                     if(z1 != null && this.getFacingDirection(z1) != null && this.blockChecks(BlockHelper.getBlockAtPos(z1)) && mc.thePlayer.getDistance(mc.thePlayer.posX + (double)y1, mc.thePlayer.posY + (double)y, mc.thePlayer.posZ + (double)z) < (double)mc.playerController.getBlockReachDistance() - 0.5D) {
                        float[] rotations = BlockHelper.getBlockRotations((double)z1.getX(), (double)z1.getY(), (double)z1.getZ());
                        var15.setYaw(rotations[0]);
                        var15.setPitch(rotations[1]);
                        BlockHelper.bestTool(z1.getX(), z1.getY(), z1.getZ());
                        this.blockBreaking = z1;
                        return;
                     }
                  }
               }
            }
         }

         this.blockBreaking = null;
      } else if(event instanceof EventPostSendMotionUpdates) {
         if(this.blockBreaking != null) {
            mc.getNetHandler().addToSendQueue(new C0APacketAnimation());
            if(mc.playerController.blockHitDelay > 0) {
               mc.playerController.blockHitDelay = 0;
            }

            EnumFacing var16 = this.getFacingDirection(this.blockBreaking);
            if(var16 != null) {
               mc.playerController.func_180512_c(this.blockBreaking, var16);
            }
         }
      } else if(event instanceof EventRender3D && this.blockBreaking != null) {
         GL11.glDisable(2896);
         GL11.glDisable(3553);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         GL11.glDisable(2929);
         GL11.glEnable(2848);
         GL11.glDepthMask(false);
         GL11.glLineWidth(0.75F);
         GL11.glColor4f(0.2F, 1.2F, 1.2F, 1.0F);
         double var10000 = (double)this.blockBreaking.getX();
         mc.getRenderManager();
         double var17 = var10000 - RenderManager.renderPosX;
         var10000 = (double)this.blockBreaking.getY();
         mc.getRenderManager();
         double var18 = var10000 - RenderManager.renderPosY;
         var10000 = (double)this.blockBreaking.getZ();
         mc.getRenderManager();
         double var19 = var10000 - RenderManager.renderPosZ;
         double xo = 1.0D;
         double yo = 1.0D;
         double zo = 1.0D;
         AxisAlignedBB mask = new AxisAlignedBB(var17, var18, var19, var17 + xo - xo * (double)mc.playerController.curBlockDamageMP, var18 + yo - yo * (double)mc.playerController.curBlockDamageMP, var19 + zo - zo * (double)mc.playerController.curBlockDamageMP);
         RenderHelper.drawOutlinedBoundingBox(mask);
         GL11.glColor4f(0.2F, 1.2F, 1.2F, 0.11F);
         RenderHelper.drawFilledBox(mask);
         GL11.glDepthMask(true);
         GL11.glDisable(2848);
         GL11.glEnable(2929);
         GL11.glDisable(3042);
         GL11.glEnable(2896);
         GL11.glEnable(3553);
      }

   }

   private EnumFacing getFacingDirection(BlockPos pos) {
      EnumFacing direction = null;
      if(!mc.theWorld.getBlockState(pos.add(0, 1, 0)).getBlock().isSolidFullCube()) {
         direction = EnumFacing.UP;
      } else if(!mc.theWorld.getBlockState(pos.add(0, -1, 0)).getBlock().isSolidFullCube()) {
         direction = EnumFacing.DOWN;
      } else if(!mc.theWorld.getBlockState(pos.add(1, 0, 0)).getBlock().isSolidFullCube()) {
         direction = EnumFacing.EAST;
      } else if(!mc.theWorld.getBlockState(pos.add(-1, 0, 0)).getBlock().isSolidFullCube()) {
         direction = EnumFacing.WEST;
      } else if(!mc.theWorld.getBlockState(pos.add(0, 0, 1)).getBlock().isSolidFullCube()) {
         direction = EnumFacing.SOUTH;
      } else if(!mc.theWorld.getBlockState(pos.add(0, 0, 1)).getBlock().isSolidFullCube()) {
         direction = EnumFacing.NORTH;
      }

      MovingObjectPosition rayResult = mc.theWorld.rayTraceBlocks(new Vec3(mc.thePlayer.posX, mc.thePlayer.posY + (double)mc.thePlayer.getEyeHeight(), mc.thePlayer.posZ), new Vec3((double)pos.getX() + 0.5D, (double)pos.getY(), (double)pos.getZ() + 0.5D));
      return rayResult != null && rayResult.func_178782_a() == pos?rayResult.field_178784_b:direction;
   }

   private boolean blockChecks(Block block) {
      if(((Boolean)this.selective.getValue()).booleanValue()) {
         if(block != this.selectedBlock) {
            return false;
         }
      } else if(block instanceof BlockAir) {
         return false;
      }

      if(block.getBlockHardness(mc.theWorld, BlockPos.ORIGIN) > -1.0F || mc.playerController.isInCreativeMode()) {
         return true;
      } else {
         return false;
      }
   }

   private void renderESP(double x, double y, double z) {
      double xPos = x - RenderManager.renderPosX;
      double yPos = y - RenderManager.renderPosY;
      double zPos = z - RenderManager.renderPosZ;
      boolean color = true;
      float red = 1.0F;
      float green = 1.0F;
      float blue = 0.5019608F;
      GL11.glColor4f(1.0F, 1.0F, 0.5019608F, 0.2F);
      RenderHelper.drawFilledBox(new AxisAlignedBB(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
      GL11.glColor4f(1.0F, 1.0F, 0.5019608F, 1.0F);
      RenderHelper.drawOutlinedBoundingBox(new AxisAlignedBB(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
      GL11.glColor4f(1.0F, 1.0F, 0.5019608F, 0.5F);
      RenderHelper.drawLines(new AxisAlignedBB(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
   }
}
