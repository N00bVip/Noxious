package noxious.mod.mods;

import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.potion.Potion;
import noxious.event.Event;
import noxious.event.events.EventBlockBreaking;
import noxious.event.events.EventPacketSent;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.TimeHelper;

public class Criticals extends Mod
{
    private double fallDist;
    private boolean viable;
    private TimeHelper time = new TimeHelper();
    private static boolean attacking;

    public Criticals()
    {
    	super("Criticals", -351136, ModManager.Category.COMBAT);
    }

    public boolean isSafe()
    {
        return mc.thePlayer.isInWater() || mc.thePlayer.isInsideOfMaterial(Material.lava) || mc.thePlayer.isOnLadder() || mc.thePlayer.getActivePotionEffects().contains(Potion.blindness) || mc.thePlayer.ridingEntity != null;
    }

    public void onDisabled()
    {
        super.onDisabled();
        this.fallDist = 0.0D;
    }

    public void onEnabled()
    {
        super.onEnabled();

        if (mc.thePlayer != null)
        {
            this.setTag("Criticals");
        }
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventPacketSent)
        {
            EventPacketSent sent = (EventPacketSent)event;

            if (sent.getPacket() instanceof C03PacketPlayer)
            {
                C03PacketPlayer player = (C03PacketPlayer)sent.getPacket();

                if (!this.isSafe())
                {
                    this.fallDist += (double)mc.thePlayer.fallDistance;
                }

                if (this.fallDist < 4.0D && !this.isSafe())
                {
                    if (this.fallDist > 0.0D)
                    {
                        player.field_149474_g = false;
                    }
                    else
                    {
                    }
                }
                else
                {
                    player.field_149474_g = true;
                    this.fallDist = 0.0D;
                    mc.thePlayer.fallDistance = 0.0F;
                }
            }
        }
        else if (event instanceof EventBlockBreaking)
        {
            this.fallDist = 4.0D;
        }
    }

    public double getFallDistance()
    {
        return this.fallDist;
    }

    public void setFallDistance(double fallDist)
    {
        this.fallDist = fallDist;
    }
}
