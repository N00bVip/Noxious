package noxious.mod.mods;

import java.util.Iterator;

import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class AutoPot extends Mod {

   private final Value<Long> delay = new Value("autopot_delay", Long.valueOf(500L));
   private final Value<Float> health = new Value("autopot_health", Float.valueOf(18.0F));
   private boolean potting;
   private final TimeHelper time = new TimeHelper();


   public AutoPot() {
      super("AutoPot", -65536, ModManager.Category.COMBAT);
      this.setTag("Auto Pot");
      Noxious.getCommandManager().getContents().add(new Command("autopotdelay", "<milliseconds>", new String[]{"potdelay", "apd"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               AutoPot.this.delay.setValue((Long)AutoPot.this.delay.getDefaultValue());
            } else {
               AutoPot.this.delay.setValue(Long.valueOf(Long.parseLong(message.split(" ")[1])));
            }

            if(((Long)AutoPot.this.delay.getValue()).longValue() > 1000L) {
               AutoPot.this.delay.setValue(Long.valueOf(1000L));
            } else if(((Long)AutoPot.this.delay.getValue()).longValue() < 1L) {
               AutoPot.this.delay.setValue(Long.valueOf(1L));
            }

            Logger.logChat("Auto Pot Delay set to: " + AutoPot.this.delay.getValue());
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("autopothealth", "<health>", new String[]{"pothealth", "aph"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               AutoPot.this.health.setValue((Float)AutoPot.this.health.getDefaultValue());
            } else {
               AutoPot.this.health.setValue(Float.valueOf(Float.parseFloat(message.split(" ")[1])));
            }

            if((double)((Float)AutoPot.this.health.getValue()).floatValue() < 1.0D) {
               AutoPot.this.health.setValue(Float.valueOf(1.0F));
            }

            Logger.logChat("Auto Pot Health set to: " + AutoPot.this.health.getValue());
         }
      });
   }

   private boolean doesHotbarHavePots() {
      for(int index = 36; index < 45; ++index) {
         ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
         if(stack != null && this.isStackSplashHealthPot(stack)) {
            return true;
         }
      }

      return false;
   }

   private void getPotsFromInventory() {
      if(!(mc.currentScreen instanceof GuiChest)) {
         for(int index = 9; index < 36; ++index) {
            ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
            if(stack != null && this.isStackSplashHealthPot(stack)) {
               mc.playerController.windowClick(0, index, 0, 1, mc.thePlayer);
               break;
            }
         }

      }
   }

   public boolean isPotting() {
      return this.potting;
   }

   private boolean isStackSplashHealthPot(ItemStack stack) {
      if(stack == null) {
         return false;
      } else {
         if(stack.getItem() instanceof ItemPotion) {
            ItemPotion potion = (ItemPotion)stack.getItem();
            if(ItemPotion.isSplash(stack.getItemDamage())) {
               Iterator var4 = potion.getEffects(stack).iterator();

               while(var4.hasNext()) {
                  Object o = var4.next();
                  PotionEffect effect = (PotionEffect)o;
                  if(effect.getPotionID() == Potion.heal.id) {
                     return true;
                  }
               }
            }
         }

         return false;
      }
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
         if(this.updateCounter() == 0) {
            return;
         }

         EventPreSendMotionUpdates pre = (EventPreSendMotionUpdates)event;
         if(mc.thePlayer.getHealth() <= ((Float)this.health.getValue()).floatValue() && this.time.hasReached(((Long)this.delay.getValue()).longValue()) && this.doesHotbarHavePots()) {
            this.potting = true;
         }
      } else if(event instanceof EventPostSendMotionUpdates) {
         if(this.updateCounter() == 0) {
            return;
         }

         if(mc.thePlayer.getHealth() <= ((Float)this.health.getValue()).floatValue() && this.time.hasReached(((Long)this.delay.getValue()).longValue())) {
            if(this.doesHotbarHavePots()) {
               if(!BlockHelper.isOnLiquid()) {
                  this.splashPot();
               }

               this.potting = false;
            } else {
               this.getPotsFromInventory();
            }

            this.time.reset();
         }
      }

   }

   private void splashPot() {
      for(int index = 36; index < 45; ++index) {
         ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
         if(stack != null && this.isStackSplashHealthPot(stack)) {
            int oldslot = mc.thePlayer.inventory.currentItem;
            this.potting = true;
            mc.getNetHandler().addToSendQueue(new C09PacketHeldItemChange(index - 36));
            mc.playerController.updateController();
            mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(new BlockPos(-1, -1, -1), -1, stack, 0.0F, 0.0F, 0.0F));
            mc.getNetHandler().addToSendQueue(new C09PacketHeldItemChange(oldslot));
            this.potting = false;
            mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C05PacketPlayerLook(mc.thePlayer.rotationYaw, mc.thePlayer.rotationPitch, mc.thePlayer.onGround));
            break;
         }
      }

   }

   private int updateCounter() {
      int counter = 0;

      for(int index = 9; index < 45; ++index) {
         ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(index).getStack();
         if(stack != null && this.isStackSplashHealthPot(stack)) {
            counter += stack.stackSize;
         }
      }

      this.setTag("Auto Pot\u00a7f \u00a77" + counter);
      return counter;
   }
}
