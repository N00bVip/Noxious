package noxious.mod.mods;

import net.minecraft.network.play.client.C0BPacketEntityAction;
import noxious.event.Event;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class Sneak extends Mod
{
    public Sneak()
    {
        super("Sneak", 0x00BFFF, Category.PLAYER);
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventPreSendMotionUpdates)
        {
            if (mc.thePlayer.motionX != 0.0D && mc.thePlayer.motionY != 0.0D && mc.thePlayer.motionZ != 0.0D && !mc.thePlayer.isSneaking())
            {
                mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING));
                mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
            }
        }
        else if (event instanceof EventPostSendMotionUpdates && !mc.thePlayer.isSneaking())
        {
            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING));
        }
    }

    public void onDisabled()
    {
        super.onDisabled();

        if (mc.thePlayer != null && !mc.thePlayer.isSneaking())
        {
            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
        }
    }
}
