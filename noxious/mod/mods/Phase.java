package noxious.mod.mods;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventBlockBoundingBox;
import noxious.event.events.EventInsideBlock;
import noxious.event.events.EventPacketReceive;
import noxious.event.events.EventPlayerMovement;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventPushOutOfBlocks;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class Phase extends Mod
{
    private boolean phasing = false;
    private double state;
    private final TimeHelper time = new TimeHelper();
    private final TimeHelper time2 = new TimeHelper();
    private final Value<Boolean> skip = new Value("phase_skip", Boolean.valueOf(true));
    private final Value<Boolean> silent = new Value("phase_silent", Boolean.valueOf(false));
    private final Value<Boolean> instant = new Value("phase_instant", Boolean.valueOf(false));
    private final Value<Double> vertical = new Value("phase_vertical", Double.valueOf(-0.005D));
    private final Value<Double> horizontal = new Value("phase_horizontal", Double.valueOf(0.2D));
    private int phaseTicks;

    public Phase()
    {
        super("Phase", -9868951, Category.EXPLOITS);
        this.setEnabled(false);
        Noxious.getCommandManager().getContents().add(new Command("phasemode", "<instant/silent/skip>", new String[0])
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("silent"))
                {
                    Phase.this.instant.setValue(Boolean.valueOf(false));
                    Phase.this.skip.setValue(Boolean.valueOf(false));
                    Phase.this.silent.setValue(Boolean.valueOf(true));
                    Logger.logChat("Phase Mode set to Silent!");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("instant"))
                {
                    Phase.this.silent.setValue(Boolean.valueOf(false));
                    Phase.this.skip.setValue(Boolean.valueOf(false));
                    Phase.this.instant.setValue(Boolean.valueOf(true));
                    Logger.logChat("Phase Mode set to Instant!");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("skip"))
                {
                    Phase.this.silent.setValue(Boolean.valueOf(false));
                    Phase.this.instant.setValue(Boolean.valueOf(false));
                    Phase.this.skip.setValue(Boolean.valueOf(true));
                    Logger.logChat("Phase Mode set to Skip!");
                }
                else
                {
                    Logger.logChat("Option not valid! Available options: silent, instant, skip.");
                }
            }
        });
        Noxious.getCommandManager().getContents().add(new Command("phasehorizontaloffset", "<offset>", new String[] {"phaseho", "pho"})
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("-d"))
                {
                    Phase.this.horizontal.setValue((Double)Phase.this.horizontal.getDefaultValue());
                }
                else
                {
                    Phase.this.horizontal.setValue(Double.valueOf(Double.parseDouble(message.split(" ")[1])));
                }

                if (((Double)Phase.this.horizontal.getValue()).doubleValue() > 10.0D)
                {
                    Phase.this.horizontal.setValue(Double.valueOf(10.0D));
                }
                else if (((Double)Phase.this.horizontal.getValue()).doubleValue() < -1.0D)
                {
                    Phase.this.horizontal.setValue(Double.valueOf(-1.0D));
                }

                Logger.logChat("Insta-Phase Horizontal Offset set to: " + Phase.this.horizontal.getValue());
            }
        });
        Noxious.getCommandManager().getContents().add(new Command("phaseverticaloffset", "<offset>", new String[] {"phasevo", "pvo"})
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("-d"))
                {
                    Phase.this.vertical.setValue((Double)Phase.this.vertical.getDefaultValue());
                }
                else
                {
                    Phase.this.vertical.setValue(Double.valueOf(Double.parseDouble(message.split(" ")[1])));
                }

                if (((Double)Phase.this.vertical.getValue()).doubleValue() > 10.0D)
                {
                    Phase.this.vertical.setValue(Double.valueOf(10.0D));
                }
                else if (((Double)Phase.this.vertical.getValue()).doubleValue() < -1.0D)
                {
                    Phase.this.vertical.setValue(Double.valueOf(-11.0D));
                }

                Logger.logChat("Insta-Phase Vertical Offset set to: " + Phase.this.vertical.getValue());
            }
        });
    }

    public void doPhase()
    {
        double x = (double)mc.thePlayer.func_174811_aO().getDirectionVec().getX() * 0.1D;
        double z = (double)mc.thePlayer.func_174811_aO().getDirectionVec().getZ() * 0.1D;
        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY - 0.05D, mc.thePlayer.posZ, true));
        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX + x * 4.0D, mc.thePlayer.posY, mc.thePlayer.posZ + z * 4.0D, true));
        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY - 0.05D, mc.thePlayer.posZ, true));
        mc.thePlayer.setPosition(mc.thePlayer.posX + x * 4.0D, mc.thePlayer.posY - 0.05D, mc.thePlayer.posZ + z * 4.0D);
    }

    private boolean isInsideBlock()
    {
        for (int x = MathHelper.floor_double(mc.thePlayer.boundingBox.minX); x < MathHelper.floor_double(mc.thePlayer.boundingBox.maxX) + 1; ++x)
        {
            for (int y = MathHelper.floor_double(mc.thePlayer.boundingBox.minY); y < MathHelper.floor_double(mc.thePlayer.boundingBox.maxY) + 1; ++y)
            {
                for (int z = MathHelper.floor_double(mc.thePlayer.boundingBox.minZ); z < MathHelper.floor_double(mc.thePlayer.boundingBox.maxZ) + 1; ++z)
                {
                    Block block = Minecraft.getMinecraft().theWorld.getBlockState(new BlockPos(x, y, z)).getBlock();

                    if (block != null && !(block instanceof BlockAir))
                    {
                        AxisAlignedBB boundingBox = block.getCollisionBoundingBox(mc.theWorld, new BlockPos(x, y, z), mc.theWorld.getBlockState(new BlockPos(x, y, z)));

                        if (boundingBox != null && mc.thePlayer.boundingBox.intersectsWith(boundingBox))
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public void onEnabled()
    {
        super.onEnabled();

        if (mc.thePlayer != null)
        {
            WorldClient var10000 = mc.theWorld;
        }
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventPreSendMotionUpdates)
        {
            boolean recive = false;

            if (!mc.thePlayer.isCollidedHorizontally)
            {
                this.time.reset();
            }
            else
            {
                this.time2.reset();
            }

            if (((Boolean)this.instant.getValue()).booleanValue())
            {
                EnumFacing dir = EnumFacing.getHorizontal(MathHelper.floor_double((double)(mc.thePlayer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3);
                int hOff = dir.getHorizontalIndex();

                if (mc.thePlayer.moveForward < 0.0F)
                {
                    hOff = dir.getOpposite().getHorizontalIndex();
                }

                if (mc.thePlayer.isCollidedHorizontally && !this.isInsideBlock() && mc.thePlayer.moveForward != 0.0F)
                {
                    if (hOff == 0)
                    {
                        if (mc.thePlayer.capabilities.isCreativeMode)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ + ((Double)this.horizontal.getValue()).doubleValue());
                        }

                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ + ((Double)this.horizontal.getValue()).doubleValue(), true));
                    }
                    else if (hOff == 1)
                    {
                        if (mc.thePlayer.capabilities.isCreativeMode)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX - ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY, mc.thePlayer.posZ);
                        }

                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX - ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY, mc.thePlayer.posZ, true));
                    }
                    else if (hOff == 2)
                    {
                        if (mc.thePlayer.capabilities.isCreativeMode)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ - ((Double)this.horizontal.getValue()).doubleValue());
                        }

                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ - ((Double)this.horizontal.getValue()).doubleValue(), true));
                    }
                    else if (hOff == 3)
                    {
                        if (mc.thePlayer.capabilities.isCreativeMode)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX + ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY, mc.thePlayer.posZ);
                        }

                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX + ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY, mc.thePlayer.posZ, true));
                    }

                    mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY + ((Double)this.vertical.getValue()).doubleValue(), mc.thePlayer.posZ, true));

                    if (!mc.thePlayer.capabilities.isCreativeMode)
                    {
                        if (hOff == 0)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY + ((Double)this.vertical.getValue()).doubleValue(), mc.thePlayer.posZ + ((Double)this.horizontal.getValue()).doubleValue());
                        }
                        else if (hOff == 1)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX - ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY + ((Double)this.vertical.getValue()).doubleValue(), mc.thePlayer.posZ);
                        }
                        else if (hOff == 2)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY + ((Double)this.vertical.getValue()).doubleValue(), mc.thePlayer.posZ - ((Double)this.horizontal.getValue()).doubleValue());
                        }
                        else if (hOff == 3)
                        {
                            mc.thePlayer.setPosition(mc.thePlayer.posX + ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY + ((Double)this.vertical.getValue()).doubleValue(), mc.thePlayer.posZ);
                        }
                    }

                    if (hOff == 0)
                    {
                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ + ((Double)this.horizontal.getValue()).doubleValue(), true));
                    }
                    else if (hOff == 1)
                    {
                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX - ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY, mc.thePlayer.posZ, true));
                    }
                    else if (hOff == 2)
                    {
                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ - ((Double)this.horizontal.getValue()).doubleValue(), true));
                    }
                    else if (hOff == 3)
                    {
                        mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX + ((Double)this.horizontal.getValue()).doubleValue(), mc.thePlayer.posY, mc.thePlayer.posZ, true));
                    }

                    mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY - 0.1D, mc.thePlayer.posZ, true));
                }
                else if (this.isInsideBlock() && mc.thePlayer.moveForward != 0.0F)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING));
                        mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
                    }
                }
            }

            if (((Boolean)this.silent.getValue()).booleanValue() && !this.isInsideBlock())
            {
                ++this.phaseTicks;

                if (this.phaseTicks > 1)
                {
                    mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING));
                    mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, -9.99999999E8D, mc.thePlayer.posZ, false));
                    mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
                    this.phaseTicks = 0;
                }
            }

            if (((Boolean)this.skip.getValue()).booleanValue() && this.time.hasReached(500L) && mc.thePlayer.isCollidedHorizontally && (!this.isInsideBlock() || mc.thePlayer.isSneaking()) && mc.thePlayer.onGround)
            {
                mc.thePlayer.motionX = 0.0D;
                mc.thePlayer.motionY = 0.0D;
                float var17 = mc.thePlayer.rotationYaw;

                if (mc.thePlayer.moveForward < 0.0F)
                {
                    var17 += 180.0F;
                }

                if (mc.thePlayer.moveStrafing > 0.0F)
                {
                    var17 -= 90.0F * (mc.thePlayer.moveForward < 0.0F ? -0.5F : (mc.thePlayer.moveForward > 0.0F ? 0.5F : 1.0F));
                }

                if (mc.thePlayer.moveStrafing < 0.0F)
                {
                    var17 += 90.0F * (mc.thePlayer.moveForward < 0.0F ? -0.5F : (mc.thePlayer.moveForward > 0.0F ? 0.5F : 1.0F));
                }

                double var18 = 0.3D;
                double vOff = 0.2D;
                float xD = (float)((double)((float)Math.cos((double)(var17 + 90.0F) * Math.PI / 180.0D)) * var18);
                float yD = (float)vOff;
                float zD = (float)((double)((float)Math.sin((double)(var17 + 90.0F) * Math.PI / 180.0D)) * var18);
                double[] topkek = new double[] { -0.02500000037252903D, -0.028571428997176036D, -0.033333333830038704D, -0.04000000059604645D, -0.05000000074505806D, -0.06666666766007741D, -0.10000000149011612D, -1000.0D, -0.20000000298023224D, -0.04000000059604645D, -0.033333333830038704D, -0.028571428997176036D, -0.02500000037252903D};

                for (int wasWalking = 0; wasWalking < topkek.length; ++wasWalking)
                {
                    mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY + topkek[wasWalking], mc.thePlayer.posZ, mc.thePlayer.onGround));
                    mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX + (double)(xD * (float)wasWalking), mc.thePlayer.posY, mc.thePlayer.posZ + (double)(zD * (float)wasWalking), mc.thePlayer.onGround));
                }

                mc.thePlayer.setPosition(mc.thePlayer.posX + (double)(xD * 2.0F), mc.thePlayer.posY, mc.thePlayer.posZ + (double)(zD * 2.0F));
                boolean var19 = mc.gameSettings.keyBindForward.pressed;
                KeyBinding var10000 = mc.gameSettings.keyBindForward;
                KeyBinding.setKeyBindState(mc.gameSettings.keyBindForward.getKeyCode(), false);
                recive = true;
            }
        }
        else if (event instanceof EventBlockBoundingBox)
        {
            EventBlockBoundingBox var14 = (EventBlockBoundingBox)event;

            if (mc.thePlayer.isCollidedHorizontally || !mc.thePlayer.onGround || this.isInsideBlock() || (double)var14.getY() > mc.thePlayer.boundingBox.maxY)
            {
                try
                {
                    if (var14.getBoundingBox().maxY > mc.thePlayer.boundingBox.minY && (!((Boolean)this.skip.getValue()).booleanValue() || this.isInsideBlock()))
                    {
                        var14.setBoundingBox((AxisAlignedBB)null);
                    }
                }
                catch (Exception var13)
                {
                    ;
                }
            }
        }
        else if (event instanceof EventPlayerMovement)
        {
            EventPlayerMovement var15 = (EventPlayerMovement)event;
        }
        else if (event instanceof EventPacketReceive)
        {
            EventPacketReceive var16 = (EventPacketReceive)event;

            if (var16.getPacket() instanceof S12PacketEntityVelocity && ((S12PacketEntityVelocity)var16.getPacket()).func_149412_c() == mc.thePlayer.getEntityId() && this.isInsideBlock())
            {
                var16.setCancelled(true);
            }
        }
        else if (event instanceof EventPushOutOfBlocks)
        {
            ((EventPushOutOfBlocks)event).setCancelled(true);
        }
        else if (event instanceof EventInsideBlock)
        {
            ((EventInsideBlock)event).setCancelled(true);
        }
    }
}
