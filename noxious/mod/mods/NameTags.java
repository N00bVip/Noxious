package noxious.mod.mods;

import java.util.ArrayList;
import java.util.Iterator;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.StringUtils;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.NahrFont;
import noxious.util.RenderHelper;
import noxious.value.Value;

public class NameTags extends Mod
{
    public Value<Boolean> armor = new Value("nametags_armor", Boolean.valueOf(true));

    public Value<Boolean> health = new Value("nametags_health", Boolean.valueOf(true));

    public NameTags()
    {
        super("NameTags");
        this.setCategory(Category.RENDER);
        this.setEnabled(true);
        Noxious.getCommandManager().getContents().add(new Command("nametags", "<armorstatus>", new String[0])
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("armorstatus"))
                {
                    NameTags.this.armor.setValue(Boolean.valueOf(!((Boolean)NameTags.this.armor.getValue()).booleanValue()));
                    Logger.logChat("Name Tags will " + (((Boolean)NameTags.this.armor.getValue()).booleanValue() ? "now" : "no longer") + " display the armor above the head.");
                }
            }
        });
        Noxious.getCommandManager().getContents().add(new Command("nametags", "<health>", new String[0])
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("health"))
                {
                    NameTags.this.health.setValue(Boolean.valueOf(!((Boolean)NameTags.this.health.getValue()).booleanValue()));
                    Logger.logChat("Name Tags will " + (((Boolean)NameTags.this.health.getValue()).booleanValue() ? "now" : "no longer") + " render other player's health.");
                }
            }
        });
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventRender3D)
        {
            if (!Minecraft.isGuiEnabled())
            {
                return;
            }

            EventRender3D render = (EventRender3D)event;
            Iterator var4 = mc.theWorld.playerEntities.iterator();

            while (var4.hasNext())
            {
                EntityPlayer player = (EntityPlayer)var4.next();

                if (player != null && player != mc.thePlayer && player.isEntityAlive())
                {
                    double posX = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double)render.getPartialTicks() - RenderManager.renderPosX;
                    double posY = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double)render.getPartialTicks() - RenderManager.renderPosY;
                    double posZ = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double)render.getPartialTicks() - RenderManager.renderPosZ;
                    GL11.glPolygonOffset(-100000.0F, -100000.0F);
                    this.renderNametag(player, posX, posY, posZ);
                    GL11.glPolygonOffset(100000.0F, 100000.0F);
                }
            }
        }
    }

    private String getHealthColor(int health)
    {
        String color;

        if (health > 50)
        {
            color = "2";
        }
        else if (health > 25)
        {
            color = "e";
        }
        else if (health > 10)
        {
            color = "6";
        }
        else
        {
            color = "4";
        }

        return color;
    }

    private boolean isPotting(EntityPlayer player, ItemStack stack)
    {
        return stack == null ? false : player.isUsingItem() && stack.getItem() instanceof ItemPotion;
    }

    private int getNametagColor(EntityPlayer player)
    {
        int color = -2697514;

        if (Noxious.getFriendManager().isFriend(player.getName()))
        {
            color = -8388864;
        }
        else if (this.isPotting(player, player.getItemInUse()))
        {
            color = -1020415;
        }
        else if ((mc.thePlayer.isSneaking() || Noxious.getModManager().getModByName("sneak") != null && Noxious.getModManager().getModByName("sneak").isEnabled()) && !player.canEntityBeSeen(mc.thePlayer))
        {
            color = -13447886;
        }
        else if (player.isSneaking())
        {
            color = -262144;
        }

        return color;
    }

    private String getNametagName(EntityPlayer player)
    {
        String name = player.getDisplayName().getFormattedText();

        /*if (((IRC)Noxious.getModManager().getModByName("IRC")).getUsers().contains(player.getName())) {
        	name = "§f[§bN§f]" + name;
        }*/
        if (Noxious.getFriendManager().isFriend(StringUtils.stripControlCodes(player.getName())))
        {
            name = "§f[§aF§f]" + name;
        }

        /**  int health = Math.round(20.0F * (player.getHealth() / player.getMaxHealth()));
          String hearts = "";
          if(health >= 1 && health <= 2) {
             hearts = "0.5";
          } else if(health >= 1 && health <= 2) {
             hearts = "1";
          } else if(health >= 2 && health <= 3) {
             hearts = "1.5";
          } else if(health >= 3 && health <= 4) {
             hearts = "2";
          } else if(health >= 4 && health <= 5) {
             hearts = "2.5";
          } else if(health >= 5 && health <= 6) {
             hearts = "3";
          } else if(health >= 6 && health <= 7) {
             hearts = "3.5";
          } else if(health >= 7 && health <= 8) {
             hearts = "4";
          } else if(health >= 8 && health <= 9) {
             hearts = "4.5";
          } else if(health >= 9 && health <= 10) {
             hearts = "5";
          } else if(health >= 10 && health <= 11) {
             hearts = "5.5";
          } else if(health >= 11 && health <= 12) {
             hearts = "6";
          } else if(health >= 12 && health <= 13) {
             hearts = "6.5";
          } else if(health >= 13 && health <= 14) {
             hearts = "7";
          } else if(health >= 14 && health <= 15) {
             hearts = "7.5";
          } else if(health >= 15 && health <= 16) {
             hearts = "8";
          } else if(health >= 16 && health <= 17) {
             hearts = "8.5";
          } else if(health >= 17 && health <= 18) {
             hearts = "9";
          } else if(health >= 18 && health <= 19) {
             hearts = "9.5";
          } else if(health >= 19 && health <= 20) {
             hearts = "10";
          } else if(health == 0) {
             hearts = "0.5";
          }**/
        if (health.getValue())
        {
            final int health = Math.round(100 * (player.getHealth() / player
                                                 .getMaxHealth()));
            name = name + " \247" + getHealthColor(health) + health + "%";
        }

        // name = name + " \u00a7f\u00a7" + this.getHealthColor(health) + hearts;
        return name;
    }

    private float getNametagSize(EntityPlayer player)
    {
        float dist = mc.thePlayer.getDistanceToEntity(player) / 4.0F;
        return dist <= 2.0F ? 2.0F : dist;
    }

    protected void renderNametag(EntityPlayer player, double x, double y, double z)
    {
        String name = this.getNametagName(player);
        FontRenderer var12 = mc.fontRendererObj;
        float var13 = this.getNametagSize(player);
        float var14 = 0.016666668F * var13;
        GL11.glPushMatrix();
        GL11.glTranslatef((float)x, (float)y + player.height + 0.5F, (float)z);
        GL11.glNormal3f(0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-RenderManager.playerViewY, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(RenderManager.playerViewX, 1.0F, 0.0F, 0.0F);
        GL11.glScalef(-var14, -var14, var14);
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glDepthMask(false);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_BLEND);
        OpenGlHelper.glBlendFunc(770, 771, 1, 0);
        Tessellator var15 = Tessellator.instance;
        byte var16 = 0;

        if (player.isSneaking())
        {
            var16 = 4;
        }

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        float var17 = RenderHelper.getNahrFont().getStringWidth(StringUtils.stripControlCodes(Noxious.getFriendManager().replaceNames(name, true))) / 2.0F;
        GL11.glPushMatrix();
        GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        RenderHelper.drawBorderedRect((float)(-var12.getStringWidth(name) / 2 - 1), (float)(var16 - (((Boolean)HUD.glucoes.getValue()).booleanValue() ? 7 : 5) + 3), (float)(-var12.getStringWidth(name) / 2) + RenderHelper.getNahrFont().getStringWidth(StringUtils.stripControlCodes(name)) + 1.0F, (float)(var16 - (((Boolean)HUD.glucoes.getValue()).booleanValue() ? 6 : 5) + 14), 1.0F, -16777216, -1728053248);
        RenderHelper.getNahrFont().drawString(name, (float)(-var12.getStringWidth(name) / 2), (float)(var16 - (((Boolean)HUD.glucoes.getValue()).booleanValue() ? 8 : 5)), NahrFont.FontType.SHADOW_THIN, this.getNametagColor(player), -16777216);

        if (((Boolean)this.armor.getValue()).booleanValue() && mc.thePlayer.getDistanceToEntity(player) <= 30.0F)
        {
            ArrayList items = new ArrayList();
            int offset;

            for (offset = 3; offset >= 0; --offset)
            {
                ItemStack xPos = player.inventory.armorInventory[offset];

                if (xPos != null)
                {
                    items.add(xPos);
                }
            }

            if (player.getCurrentEquippedItem() != null)
            {
                items.add(player.getCurrentEquippedItem());
            }

            offset = (int)(var17 - (float)((items.size() - 1) * 9) - 9.0F);
            int var28 = 0;

            for (Iterator var19 = items.iterator(); var19.hasNext(); var28 += 18)
            {
                ItemStack stack = (ItemStack)var19.next();
                GL11.glPushMatrix();
                GlStateManager.clear(256);
                GL11.glDisable(GL11.GL_LIGHTING);
                mc.getRenderItem().zLevel = -100.0F;
                mc.getRenderItem().drawItemWithoutEnchant(stack, (int)(-var17 + (float)offset + (float)var28), var16 - 20);
                Minecraft.getMinecraft().getRenderItem().func_175030_a(Minecraft.getMinecraft().fontRendererObj, stack, (int)(-var17 + (float)offset + (float)var28), var16 - 20);
                mc.getRenderItem().zLevel = 0.0F;
                GlStateManager.enableAlpha();
                GlStateManager.disableBlend();
                GlStateManager.disableCull();
                GlStateManager.clear(256);
                GL11.glDepthMask(true);
                GL11.glPopMatrix();
                NBTTagList enchants = stack.getEnchantmentTagList();
                GL11.glPushMatrix();
                GL11.glDisable(GL11.GL_LIGHTING);
                GL11.glScalef(0.5F, 0.5F, 0.5F);

                if (stack.getItem() == Items.golden_apple && stack.hasEffect())
                {
                    RenderHelper.getNahrFont().drawString("god", (-var17 + (float)offset + (float)var28) * 2.0F, (float)((var16 - 20) * 2), NahrFont.FontType.SHADOW_THIN, -65536, -16777216);
                }
                else if (enchants != null)
                {
                    int ency = 0;

                    if (enchants.tagCount() >= 6)
                    {
                        RenderHelper.getNahrFont().drawString("god", (-var17 + (float)offset + (float)var28) * 2.0F, (float)((var16 - 20) * 2), NahrFont.FontType.SHADOW_THIN, -65536, -16777216);
                    }
                    else
                    {
                        try
                        {
                            for (int index = 0; index < enchants.tagCount(); ++index)
                            {
                                short id = enchants.getCompoundTagAt(index).getShort("id");
                                short level = enchants.getCompoundTagAt(index).getShort("lvl");

                                if (Enchantment.field_180311_a[id] != null)
                                {
                                    Enchantment enc = Enchantment.field_180311_a[id];
                                    String encName = enc.getTranslatedName(level).substring(0, 2).toLowerCase();

                                    if (level > 99)
                                    {
                                        encName = encName + "99+";
                                    }
                                    else
                                    {
                                        encName = encName + level;
                                    }

                                    RenderHelper.getNahrFont().drawString(encName, (-var17 + (float)offset + (float)var28) * 2.0F + 2.0F, (float)((var16 - 20 + ency) * 2), NahrFont.FontType.SHADOW_THICK, -1, -16777216);
                                    ency += mc.fontRendererObj.FONT_HEIGHT / 2 + 1;
                                }
                            }
                        }
                        catch (Exception var27)
                        {
                            ;
                        }
                    }
                }

                GL11.glEnable(GL11.GL_LIGHTING);
                GL11.glPopMatrix();
            }
        }

        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthMask(true);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glPopMatrix();
    }
}
