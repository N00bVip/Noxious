package noxious.mod.mods.aura.modes;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventRender3D;
import noxious.mod.mods.XivKillAura;
import noxious.mod.mods.aura.AuraMode;
import noxious.util.EntityHelper;
import noxious.util.EntityUtils;
import noxious.util.GLUtil;
import noxious.util.Location;
import noxious.util.RenderHelper;
import noxious.util.TimeHelper;

public class Multi extends AuraMode {
	public EntityLivingBase target = null;
	private ArrayList<EntityLivingBase> semiMultiTargets = new ArrayList();
	private ArrayList<EntityLivingBase> targets = new ArrayList();
	private TimeHelper time = new TimeHelper();
	private TimeHelper time1 = new TimeHelper();
	private TimeHelper targetSwitchTime = new TimeHelper();
	private TimeHelper angleSwitchTime = new TimeHelper();
	private EntityLivingBase lastAttackedTarget = null;
	private Random rand = new Random();
	private float lastYaw;
	private float lastPitch;
	private long attackDelay;
	private float rnddelay;
	private boolean Loop;

	public Multi(XivKillAura killAura) {
		super("Multi", killAura);
	}

	public void onPreMotionUpdate(EventPreSendMotionUpdates event)
	{
		this.attackDelay = killAura.getDelay();
			double targetWeight = Double.NEGATIVE_INFINITY;
			EntityLivingBase highestWeightedTarget = null;
			this.targets.clear();
			this.semiMultiTargets.clear();
			this.target = null;
			for (Object o : this.mc.theWorld.loadedEntityList) {
				if ((o instanceof EntityLivingBase))
				{
					EntityLivingBase el = (EntityLivingBase)o;
					if (killAura.isValidEntity(el)) {
						this.targets.add(el);
					}
				}
			}
			for (EntityLivingBase el : this.targets) {
				if ((killAura.isValidEntity(el)) &&
					(getTargetWeight(el) > targetWeight))
				{
					targetWeight = getTargetWeight(el);
					highestWeightedTarget = el;
				}
			}
			if (highestWeightedTarget != null) {
				this.target = highestWeightedTarget;
			}

		if (!killAura.isValidEntity(this.target)) {
			this.target = null;
		}
		if (this.target != null)
		{
			boolean shouldLook = true;
			float[] values = EntityHelper.getAngles(this.target);
			if (shouldLook)
			{
                /*float[] rotations = EntityUtils.getEntityRotations(target);

        		if (!killAura.silent.getValue()) {
                	mc.thePlayer.rotationYaw = rotations[0];
                	mc.thePlayer.rotationPitch = rotations[1];
                } else {
                	event.setYaw(rotations[0]);
                	event.setPitch(rotations[1]);
                }*/

				for (int i = 0; i < this.killAura.maxTarget.getValue(); i++)
				{
					double semiMultiWeight = Double.NEGATIVE_INFINITY;
					EntityLivingBase smHighestWeightedTarget = null;
					for (EntityLivingBase el : this.targets) {
						if ((el != this.target) &&
							(getDirectionCheckVal(el, getLookVecForAngles(values[0], values[1])) < 0.1D) && (el.getDistanceSqToEntity(this.target) < 4.0D) &&
							(killAura.isValidEntity(el)) && (!this.semiMultiTargets.contains(el)) &&
							(getTargetWeight(el) > semiMultiWeight))
						{
							semiMultiWeight = getTargetWeight(el);
							smHighestWeightedTarget = el;
						}
					}
					if (smHighestWeightedTarget != null) {
						this.semiMultiTargets.add(smHighestWeightedTarget);
					}
				}

				double averageX = 0.0D;
		        double averageY = 0.0D;
		        double averageZ = 0.0D;
		        this.semiMultiTargets.add(this.target);
		        for (EntityLivingBase entity : this.semiMultiTargets)
		        {
		          averageX += entity.posX;
		          averageY += entity.posY + entity.getEyeHeight();
		          averageZ += entity.posZ;
		        }
		        averageX /= this.semiMultiTargets.size();
		        averageY /= this.semiMultiTargets.size();
		        averageZ /= this.semiMultiTargets.size();

		        float[] rotations = EntityUtils.facePosition(this.mc.thePlayer, averageX, averageY, averageZ);
		        if (((Boolean)this.killAura.silent.getValue()).booleanValue())
		        {
		          this.mc.thePlayer.rotationYaw = rotations[1];
		          this.mc.thePlayer.rotationPitch = rotations[0];
		        }
		        else
		        {
		          EventPreSendMotionUpdates.setYaw(rotations[1]);
		          EventPreSendMotionUpdates.setPitch(rotations[0]);
		        }
			}
			if ((this.semiMultiTargets.isEmpty())) {
				this.attackDelay -= 20L;
			} else {
				//this.attackDelay += Math.max(this.semiMultiTargets.size() - 2, 0) * 100 - 50;
			}
			if (this.targets.size() == 1) {
				this.attackDelay -= 20L;
			}
		}
	}

	  public double getDirectionCheckVal(Entity e, Vec3 lookVec)
	  {
	    return directionCheck(mc.thePlayer.posX, mc.thePlayer.posY + mc.thePlayer.getEyeHeight(), mc.thePlayer.posZ, lookVec.xCoord, lookVec.yCoord, lookVec.zCoord, e.posX, e.posY + e.height / 2.0D, e.posZ, e.width, e.height, 5.0D);
	  }

	  private double directionCheck(double sourceX, double sourceY, double sourceZ, double dirX, double dirY, double dirZ, double targetX, double targetY, double targetZ, double targetWidth, double targetHeight, double precision)
	  {
	    double dirLength = Math.sqrt(dirX * dirX + dirY * dirY + dirZ * dirZ);
	    if (dirLength == 0.0D) {
	      dirLength = 1.0D;
	    }
	    double dX = targetX - sourceX;
	    double dY = targetY - sourceY;
	    double dZ = targetZ - sourceZ;

	    double targetDist = Math.sqrt(dX * dX + dY * dY + dZ * dZ);

	    double xPrediction = targetDist * dirX / dirLength;
	    double yPrediction = targetDist * dirY / dirLength;
	    double zPrediction = targetDist * dirZ / dirLength;

	    double off = 0.0D;

	    off += Math.max(Math.abs(dX - xPrediction) - (targetWidth / 2.0D + precision), 0.0D);
	    off += Math.max(Math.abs(dZ - zPrediction) - (targetWidth / 2.0D + precision), 0.0D);
	    off += Math.max(Math.abs(dY - yPrediction) - (targetHeight / 2.0D + precision), 0.0D);
	    if (off > 1.0D) {
	      off = Math.sqrt(off);
	    }
	    return off;
	  }

	protected final Vec3 getLookVecForAngles(float yaw, float pitch) {
		float var3 = MathHelper.cos(-pitch * 0.017453292F - 3.1415927F);
		float var4 = MathHelper.sin(-pitch * 0.017453292F - 3.1415927F);
		float var5 = -MathHelper.cos(-yaw * 0.017453292F);
		float var6 = MathHelper.sin(-yaw * 0.017453292F);
		Vec3 vector = new Vec3(var4 * var5, var6, var3 * var5);
		return vector;
	}

	public void onPostMotionUpdate(EventPostSendMotionUpdates event) {
		if ((this.target != null) &&
			(killAura.isValidEntity(this.target)) &&
			(this.time.hasReached(this.attackDelay) && !semiMultiTargets.isEmpty())) {
			this.time.reset();
			//if ((this.mc.thePlayer.isBlocking()) && (!Serenity.getInstance().modManager.getModuleByClass(ModuleNoSlowdown.class).isEnabled())) {
				//mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN));
			//}
			if (!Loop) {
				Loop = true;
				for (int i = 0; i < semiMultiTargets.size(); i++) {
					if (killAura.isValidEntity(semiMultiTargets.get(i))) {
						if (killAura.delaycheck.getValue()) {
							if (time1.hasReached(rnddelay)) {
								time1.reset();
								killAura.attack(semiMultiTargets.get(i));
								rnddelay = rand.nextInt(killAura.randomDelay.getValue().intValue());
							} else {
								i--;
							}
						} else {
							killAura.attack(semiMultiTargets.get(i));
						}
					}
				}
			}

			Loop = false;

			//if ((this.mc.thePlayer.isBlocking()) && (!Serenity.getInstance().modManager.getModuleByClass(ModuleNoSlowdown.class).isEnabled())) {
				//mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(new BlockPos(-1, -1, -1), 255, this.mc.thePlayer.inventory.getCurrentItem(), 0.0F, 0.0F, 0.0F));
			//}
			this.lastAttackedTarget = this.target;
			this.targetSwitchTime.reset();
		}
	}

	public void onMotionPacket(C03PacketPlayer event) {
		if (event.getRotating()) {
			this.lastYaw = event.getYaw();
			this.lastPitch = event.getPitch();
		}
	}

	public void onRender(EventRender3D _event) {
		//if (this.target_Highlight.getValue())
		//{
			//double d = this.target.lastTickPosX + (this.target.posX - this.target.lastTickPosX) * this.mc.timer.renderPartialTicks;
			//double d1 = this.target.lastTickPosY + (this.target.posY - this.target.lastTickPosY) * this.mc.timer.renderPartialTicks;
			//double d2 = this.target.lastTickPosZ + (this.target.posZ - this.target.lastTickPosZ) * this.mc.timer.renderPartialTicks;
			//drawEntityESP(d - this.mc.getRenderManager().renderPosX, d1 - this.mc.getRenderManager().renderPosY, d2 - this.mc.getRenderManager().renderPosZ, this.target, this.target.height - 0.1D, this.target.width - 0.12D);
		//}
	}

	private double getDistToEntitySqPred(Entity e)
	{
		if ((e instanceof EntityPlayer))
		{
			EntityPlayer p = (EntityPlayer)e;
			Location loc = predictEntityLocation(p, this.mc
				.getNetHandler().func_175102_a(this.mc.thePlayer.getUniqueID()).getResponseTime());
			if (loc != null) {
				return Math.min(this.mc.thePlayer.getDistanceSq(loc.x, loc.y, loc.z), this.mc.thePlayer.getDistanceSqToEntity(e));
			}
		}
		return this.mc.thePlayer.getDistanceSqToEntity(e);
	}

	  public Location predictEntityLocation(Entity e, double milliseconds)
	  {
	    if (e != null)
	    {
	      if ((e.posX == e.lastTickPosX) && (e.posY == e.lastTickPosY) && (e.posZ == e.lastTickPosZ)) {
	        return new Location(e.posX, e.posY, e.posZ);
	      }
	      double ticks = milliseconds / 1000.0D;
	      ticks *= 20.0D;
	      return interp(new Location(e.lastTickPosX, e.lastTickPosY, e.lastTickPosZ), new Location(e.posX + e.motionX, e.posY + e.motionY, e.posZ + e.motionZ), ticks);
	    }
	    return null;
	  }

	  public Location interp(Location from, Location to, double pct)
	  {
	    double x = from.x + (to.x - from.x) * pct;
	    double y = from.y + (to.y - from.y) * pct;
	    double z = from.z + (to.z - from.z) * pct;
	    return new Location(x, y, z);
	  }




	public double getTargetWeight(EntityLivingBase el)
	{
		double weight = el.getMaxHealth() - (el.getHealth() + el.getAbsorptionAmount() + el.getTotalArmorValue() * 5);
		weight -= this.mc.thePlayer.getDistanceSqToEntity(el) / 2.0D;
		if ((el instanceof EntityPlayer)) {
			weight += 50.0D;
		}
		if ((el instanceof EntityCreeper)) {
			weight += 35.0D;
		} else if ((el instanceof EntitySkeleton)) {
			weight += 25.0D;
		}
		float distYaw = Math.abs(EntityHelper.getYawChangeToEntity(el));
		float distPitch = Math.abs(EntityHelper.getPitchChangeToEntity(el));

		float distCombined = distYaw + distPitch;
		if (!killAura.anglecheck.getValue()) {
			weight -= distCombined;
		}
		return weight;
	}

	public float getDistanceBetweenAngles(float a, float b)
	{
		float d = Math.abs(a - b) % 360.0F;
		float r = d > 180.0F ? 360.0F - d : d;
		return r;
	}

	public int randomNumberFromRange(int min, int max)
	{
		return this.rand.nextInt(max - min + 1) + max;
	}

	public void drawEntityESP(double d, double d1, double d2, EntityLivingBase ep, double e, double f)
	{
		if ((!(ep instanceof EntityPlayerSP)) && (ep.isEntityAlive()))
		{
			GL11.glPushMatrix();
			GLUtil.setGLCap(3042, true);
			GLUtil.setGLCap(3553, false);
			GLUtil.setGLCap(2896, false);
			GLUtil.setGLCap(2929, false);
			GL11.glDepthMask(false);
			GL11.glLineWidth(1.8F);
			GL11.glBlendFunc(770, 771);
			GLUtil.setGLCap(2848, true);
			if (this.target.hurtTime <= 0) {
				GL11.glColor4f(0.0F, 1.0F, 0.0F, 0.11F);
			} else {
				GL11.glColor4f(1.0F, 0.0F, 0.0F, 0.11F);
			}
			RenderHelper.drawOutlinedBoundingBox(new AxisAlignedBB(d - f, d1 + 0.1D, d2 - f, d + f, d1 + e + 0.25D, d2 + f));
			GL11.glDepthMask(true);
			GLUtil.revertAllCaps();
			GL11.glPopMatrix();
		}
	}

	public boolean isTrueInvisible(EntityLivingBase el)
	{
		if ((el instanceof EntityPlayer))
		{
			EntityPlayer p = (EntityPlayer)el;
			boolean hasArmour = false;
			for (ItemStack stack : p.inventory.armorInventory) {
				if (stack != null) {
					hasArmour = true;
				}
			}
			return (el.isInvisible()) && (el.getHeldItem() == null) && (!hasArmour);
		}
		return (el.isInvisible()) && (el.getHeldItem() == null);
	}

	@Override
	public void onDisabled() {
	}
}
