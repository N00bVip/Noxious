package noxious.mod.mods.aura;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.mod.mods.XivKillAura;

public abstract class AuraMode {
    private String name;
    protected final XivKillAura killAura;
    protected final Minecraft mc = Minecraft.getMinecraft();

    public AuraMode(String name, XivKillAura killAura) {
        this.name = name;
        this.killAura = killAura;
    }

    public String getName() {
        return name;
    }

    public abstract void onPreMotionUpdate(EventPreSendMotionUpdates event);

    public abstract void onPostMotionUpdate(EventPostSendMotionUpdates event);

    public abstract void onMotionPacket(C03PacketPlayer packet);

    public abstract void onDisabled();
}
