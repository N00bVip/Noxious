package noxious.mod.mods;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventDrawTitle;
import noxious.event.events.EventOnUpdate;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPostAttack;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.EntityHelper;
import noxious.util.GLUtil;
import noxious.util.ListenerUtil;
import noxious.util.Logger;
import noxious.util.RenderHelper;
import noxious.util.TimeHelper;
import noxious.value.Value;

public final class KillAura extends Mod {

   public final Value<Boolean> animals = new Value("killaura_animals", Boolean.valueOf(false));
   public final Value<Boolean> esp = new Value("killaura_esp", Boolean.valueOf(true));
   public final Value<Boolean> autohit = new Value("killaura_autohit", Boolean.valueOf(true));
   public final Value<Boolean> autosword = new Value("killaura_autosword", Boolean.valueOf(true));
   public final Value<Boolean> latestncp = new Value("killaura_latestncp", Boolean.valueOf(false));
   public final Value<Boolean> autoaim = new Value("killaura_autoaim", Boolean.valueOf(true));
   public final Value<Long> speed = new Value("killaura_delay", Long.valueOf(183L));
   public final Value<Boolean> gcheat = new Value("killaura_gcheat", Boolean.valueOf(false));
   public final Value<String> ignore = new Value("killaura_ignore", "");
   public final Value<Boolean> invisibles = new Value("killaura_invisibles", Boolean.valueOf(true));
   public final Value<Integer> maxTargets = new Value("killaura_max_targets", Integer.valueOf(1));
   public final Value<Boolean> mobs = new Value("killaura_mobs", Boolean.valueOf(false));
   public final Value<Boolean> swing = new Value("killaura_swing", Boolean.valueOf(false));
   public final Value<Boolean> players = new Value("killaura_players", Boolean.valueOf(true));
   public final Value<Double> reach = new Value("killaura_reach", Double.valueOf(3.8D));
   private final Value<String> mode = new Value("killaura_mode", String.valueOf("none"));
   public final Value<Boolean> dura = new Value("killaura_dura", Boolean.valueOf(false));
   public static final List<Entity> targets = new CopyOnWriteArrayList();
   public final Value<Integer> ticksToWait = new Value("killaura_ticks_to_wait", Integer.valueOf(0));
   private final Value<Boolean> fastattack = new Value("killaura_fastattack", Boolean.valueOf(true));
   private final Value<Boolean> criticals = new Value("killaura_criticals", Boolean.valueOf(false));
   public final Random rnd = new Random();
   private int rnddelay = rnd.nextInt(11);
   private final TimeHelper time = new TimeHelper();
   private final TimeHelper time2 = new TimeHelper();
   private final TimeHelper time3 = new TimeHelper();
   private final TimeHelper targetSwitchTime = new TimeHelper();
   public static String drawtitle = "";
   private float serveryaw;
   private float serverpitch;
   private float spin;
   private int itemSwitchTicks = 0;
   private int ticks = 0;
   private boolean shouldBlock = false;
   private boolean attacking = false;
   public static Entity oldtarget;
   public static Entity nowtarget;


   public KillAura() {
      super("KillAura", -7733248, ModManager.Category.COMBAT);
      this.setTag("Kill Aura");
      Noxious.getCommandManager().getContents().add(new Command("killaura", "<players/invisibles/mobs/swing/animals/silent/autohit/gcheat/autosword/latestncp/fastattack/dura/criticals/switch/mode>", new String[0]) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("players")) {
               KillAura.this.players.setValue(Boolean.valueOf(!((Boolean)KillAura.this.players.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.players.getValue()).booleanValue()?"now":"no longer") + " hit players.");
            } else if(message.split(" ")[1].equalsIgnoreCase("fastattack")) {
               KillAura.this.fastattack.setValue(Boolean.valueOf(!((Boolean)KillAura.this.fastattack.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.fastattack.getValue()).booleanValue()?"now":"no longer") + " attack faster. (Exploit)");
            } else if(message.split(" ")[1].equalsIgnoreCase("autoaim")) {
               KillAura.this.autoaim.setValue(Boolean.valueOf(!((Boolean)KillAura.this.autoaim.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.autoaim.getValue()).booleanValue()?"now":"no longer") + " automatically aim at the target.");
            } else if(message.split(" ")[1].equalsIgnoreCase("esp")) {
               KillAura.this.esp.setValue(Boolean.valueOf(!((Boolean)KillAura.this.esp.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.esp.getValue()).booleanValue()?"now":"no longer") + " render an ESP box over the target.");
            } else if(message.split(" ")[1].equalsIgnoreCase("invisibles")) {
               KillAura.this.invisibles.setValue(Boolean.valueOf(!((Boolean)KillAura.this.invisibles.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.invisibles.getValue()).booleanValue()?"now":"no longer") + " hit invisibles.");
            } else if(message.split(" ")[1].equalsIgnoreCase("mobs")) {
               KillAura.this.mobs.setValue(Boolean.valueOf(!((Boolean)KillAura.this.mobs.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.mobs.getValue()).booleanValue()?"now":"no longer") + " hit mobs.");
            } else if(message.split(" ")[1].equalsIgnoreCase("dura")) {
               KillAura.this.dura.setValue(Boolean.valueOf(!((Boolean)KillAura.this.dura.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.dura.getValue()).booleanValue()?"now":"no longer") + " use the durabillity exploit.");
               if(((Boolean)KillAura.this.dura.getValue()).booleanValue()) {
                  Logger.logChat("Make sure you put the swords in a correct formation!");
               }
            } else if(message.split(" ")[1].equalsIgnoreCase("swing")) {
               KillAura.this.swing.setValue(Boolean.valueOf(!((Boolean)KillAura.this.swing.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.swing.getValue()).booleanValue()?"now":"no longer") + " silently swing. (BETA)");
            } else if(message.split(" ")[1].equalsIgnoreCase("criticals")) {
               KillAura.this.criticals.setValue(Boolean.valueOf(!((Boolean)KillAura.this.criticals.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.criticals.getValue()).booleanValue()?"now":"no longer") + " use criticals.");
               Logger.logChat("\u00a7cONLY USE FOR LATEST NCP");
            } else if(message.split(" ")[1].equalsIgnoreCase("animals")) {
               KillAura.this.animals.setValue(Boolean.valueOf(!((Boolean)KillAura.this.animals.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.animals.getValue()).booleanValue()?"now":"no longer") + " hit animals.");
            } else if(message.split(" ")[1].equalsIgnoreCase("latestncp")) {
               KillAura.this.latestncp.setValue(Boolean.valueOf(!((Boolean)KillAura.this.latestncp.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.latestncp.getValue()).booleanValue()?"now":"no longer") + " bypass the latest version of NCP.");
               Logger.logChat("\u00a7cONLY USE FOR LATEST NCP");
            } else if(message.split(" ")[1].equalsIgnoreCase("autohit")) {
               KillAura.this.autohit.setValue(Boolean.valueOf(!((Boolean)KillAura.this.autohit.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.autohit.getValue()).booleanValue()?"now":"no longer") + " hit automatically.");
            } else if(message.split(" ")[1].equalsIgnoreCase("gcheat")) {
               KillAura.this.gcheat.setValue(Boolean.valueOf(!((Boolean)KillAura.this.gcheat.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.gcheat.getValue()).booleanValue()?"now":"no longer") + " bypass GCheat.");
            } else if(message.split(" ")[1].equalsIgnoreCase("autosword")) {
               KillAura.this.autosword.setValue(Boolean.valueOf(!((Boolean)KillAura.this.autosword.getValue()).booleanValue()));
               Logger.logChat("Kill Aura will " + (((Boolean)KillAura.this.gcheat.getValue()).booleanValue()?"now":"no longer") + " automatically switch to the sword.");
            } else if(message.split(" ")[1].equalsIgnoreCase("mode")) {
            	if (message.split(" ")[2].equalsIgnoreCase("none")) {
            		KillAura.this.mode.setValue("none");
            		Logger.logChat("Kill Aura will no longer bypass");
            	} else if (message.split(" ")[2].equalsIgnoreCase("anni") && !type.equals("0")) {
            		KillAura.this.mode.setValue("anni");
            		Logger.logChat("Kill Aura will now bypass Anni NPC.");
            	} else if (message.split(" ")[2].equalsIgnoreCase("gg") && !type.equals("0")) {
            		KillAura.this.mode.setValue("gg");
            		Logger.logChat("Kill Aura will now bypass GG NPC.");
            	} else if (message.split(" ")[2].equalsIgnoreCase("minez")) {
            		KillAura.this.mode.setValue("minez");
            		Logger.logChat("Kill Aura will now bypass MineZ NPC.");
            	} else if (message.split(" ")[2].equalsIgnoreCase("free")) {
            		KillAura.this.mode.setValue("free");
            		Logger.logChat("Kill Aura will now bypass free.");
            	} else {
            		Logger.logChat("Option not valid! Available options: none, anni, gg, minez, free");
            	}
         	}else {
               Logger.logChat("Option not valid! Available options: players, invisibles, mobs, swing, animals, silent, autohit, gcheat, autosword, latestncp, switch, fastattack, dura, criticals, mode.");
            }

         }
      });
      Noxious.getCommandManager().getContents().add(new Command("killauraignore", "<color>", new String[]{"auraignore", "kaig", "oc"}) {
         public void run(String message) {
            String[] arguments = message.split(" ");
            if(arguments.length == 1) {
               KillAura.this.ignore.setValue("");
               Logger.logChat("Kill Aura ignore colors cleared.");
            }

            KillAura.this.ignore.setValue(message.split(" ")[1].substring(0, 1));
            Logger.logChat("Kill Aura will ignore \u00a7" + (String)KillAura.this.ignore.getValue() + "this" + " \u00a7fcolor");
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("killauradelay", "<milliseconds>", new String[]{"auradelay", "kad"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               KillAura.this.speed.setValue((Long)KillAura.this.speed.getDefaultValue());
            } else {
               KillAura.this.speed.setValue(Long.valueOf(Long.parseLong(message.split(" ")[1])));
            }

            if(((Long)KillAura.this.speed.getValue()).longValue() > 1000L) {
               KillAura.this.speed.setValue(Long.valueOf(1000L));
            } else if(((Long)KillAura.this.speed.getValue()).longValue() < 1L) {
               KillAura.this.speed.setValue(Long.valueOf(1L));
            }

            if(((Long)KillAura.this.speed.getValue()).longValue() < 60L) {
               Logger.logChat("[\u00a7aNOTE\u00a7f] Your delay needs to be over 60 to switch really fast!");
            }

            Logger.logChat("Kill Aura Delay set to: " + KillAura.this.speed.getValue());
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("killaurareach", "<blocks>", new String[]{"aurareach", "kar"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               KillAura.this.reach.setValue((Double)KillAura.this.reach.getDefaultValue());
            } else {
               KillAura.this.reach.setValue(Double.valueOf(Double.parseDouble(message.split(" ")[1])));
            }

            if(((Double)KillAura.this.reach.getValue()).doubleValue() > 6.0D) {
               KillAura.this.reach.setValue(Double.valueOf(6.0D));
            } else if(((Double)KillAura.this.reach.getValue()).doubleValue() < 1.0D) {
               KillAura.this.reach.setValue(Double.valueOf(1.0D));
            }

            Logger.logChat("Kill Aura Reach set to: " + KillAura.this.reach.getValue());
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("killauramaxtargets", "<targets>", new String[]{"auramaxtargets", "kamt"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               KillAura.this.maxTargets.setValue((Integer)KillAura.this.maxTargets.getDefaultValue());
            } else {
               KillAura.this.maxTargets.setValue(Integer.valueOf(Integer.parseInt(message.split(" ")[1])));
            }

            if(((Integer)KillAura.this.maxTargets.getValue()).intValue() > 250) {
               KillAura.this.maxTargets.setValue(Integer.valueOf(250));
            } else if(((Integer)KillAura.this.maxTargets.getValue()).intValue() < 1) {
               KillAura.this.maxTargets.setValue(Integer.valueOf(1));
            }

            Logger.logChat("Kill Aura Max Targets set to: " + KillAura.this.maxTargets.getValue());
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("killauraage", "<ticks-to-wait>", new String[]{"auraage", "kaag"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               KillAura.this.ticksToWait.setValue((Integer)KillAura.this.ticksToWait.getDefaultValue());
            } else {
               KillAura.this.ticksToWait.setValue(Integer.valueOf(Integer.parseInt(message.split(" ")[1])));
            }

            if(((Integer)KillAura.this.ticksToWait.getValue()).intValue() > 100) {
               KillAura.this.ticksToWait.setValue(Integer.valueOf(100));
            } else if(((Integer)KillAura.this.ticksToWait.getValue()).intValue() < 0) {
               KillAura.this.ticksToWait.setValue(Integer.valueOf(0));
            }

            Logger.logChat("Kill Aura will now wait: " + KillAura.this.ticksToWait.getValue() + " ticks for the new entities.");
            Noxious.getFileManager().getFileByName("valueconfig").saveFile();
         }
      });
   }

   private void attack(Entity entity) {
      int original = mc.thePlayer.inventory.currentItem;
      long delay = ((Long)this.speed.getValue()).longValue();
      if (!mode.getValue().equalsIgnoreCase("none")) {
    	delay = delay + rnddelay;
      }
      if(this.time.hasReached(delay)) {
          int itemb4 = mc.thePlayer.inventory.currentItem;
          boolean b4sprinting = mc.thePlayer.isSprinting();
          if(((Boolean)this.autosword.getValue()).booleanValue() && !((Boolean)this.fastattack.getValue()).booleanValue()) {
             mc.thePlayer.inventory.currentItem = EntityHelper.getBestWeapon(entity);
             mc.playerController.updateController();
          }

         int oldDamage = 0;
         if(mc.thePlayer.getCurrentEquippedItem() != null) {
            oldDamage = mc.thePlayer.getCurrentEquippedItem().getItemDamage();
         }

         if(mc.thePlayer.isBlocking()) {
            mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, new BlockPos(0, 0, 0), EnumFacing.UP));
         }

         if(((Boolean)this.autohit.getValue()).booleanValue()) {
            mc.thePlayer.swingItem();
         } else {
            mc.getNetHandler().addToSendQueue(new C0APacketAnimation());
         }

         mc.getNetHandler().getNetworkManager().sendPacket(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));
         if(((Boolean)this.fastattack.getValue()).booleanValue()) {
            for(int i = 0; i < 3; ++i) {
               mc.getNetHandler().addToSendQueue(new C02PacketUseEntity(entity, new Vec3(entity.posX, entity.posY, entity.posZ)));
            }
         }

         if(mc.thePlayer.getCurrentEquippedItem() != null) {
            mc.thePlayer.getCurrentEquippedItem().setItemDamage(oldDamage);
         }

         if(((Boolean)this.latestncp.getValue()).booleanValue() && (double)mc.thePlayer.moveForward > 0.0D) {
            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SPRINTING));
         }

         Noxious.getEventManager().call(new EventPostAttack());
         if (!mode.getValue().equalsIgnoreCase("none")) {
        	 rnddelay = rnd.nextInt(11);
         }
         targets.remove(entity);
         if(targets.size() == 0) {
            this.time.reset();
         }
      }

   }

   public Value<String> getIgnore() {
      return this.ignore;
   }

   public List<Entity> getTargets() {
      return targets;
   }

   private float[] getYawAndPitch(Entity paramEntityPlayer) {
      double var10000 = paramEntityPlayer.posX;
      Noxious.getListenerUtil();
      double d1 = var10000 - ListenerUtil.serverPos.posX;
      var10000 = paramEntityPlayer.posZ;
      Noxious.getListenerUtil();
      double d2 = var10000 - ListenerUtil.serverPos.posZ;
      Noxious.getListenerUtil();
      double d3 = ListenerUtil.serverPos.posY + 0.12D - (paramEntityPlayer.posY + 1.82D);
      double d4 = (double)MathHelper.sqrt_double(d1 + d2);
      float f1 = (float)(Math.atan2(d2, d1) * 180.0D / 3.141592653589793D) - 90.0F;
      float f2 = (float)(Math.atan2(d3, d4) * 180.0D / 3.141592653589793D);
      return new float[]{f1, f2};
   }

   private float getDistanceBetweenAngles(float paramFloat) {
      Noxious.getListenerUtil();
      float f = Math.abs(paramFloat - ListenerUtil.serverPos.rotationYaw) % 360.0F;
      if(f > 180.0F) {
         f = 360.0F - f;
      }

      return f;
   }

   private float[] getYawAndPitch2(Entity paramEntityPlayer) {
      double d1 = paramEntityPlayer.posX - mc.thePlayer.posX;
      double d2 = paramEntityPlayer.posZ - mc.thePlayer.posZ;
      double d3 = mc.thePlayer.posY + 0.12D - (paramEntityPlayer.posY + 1.82D);
      double d4 = (double)MathHelper.sqrt_double(d1 + d2);
      float f1 = (float)(Math.atan2(d2, d1) * 180.0D / 3.141592653589793D) - 90.0F;
      float f2 = (float)(Math.atan2(d3, d4) * 180.0D / 3.141592653589793D);
      return new float[]{f1, f2};
   }

   private float getDistanceBetweenAngles2(float paramFloat) {
      float f = Math.abs(paramFloat - mc.thePlayer.rotationYaw) % 360.0F;
      if(f > 180.0F) {
         f = 360.0F - f;
      }

      return f;
   }

   public boolean isValidTarget(Entity entity) {
      float[] arrayOfFloat = this.getYawAndPitch(mc.thePlayer);
      double d2g = (double)this.getDistanceBetweenAngles(arrayOfFloat[0]);
      double dooble = mc.isSingleplayer()?180.0D:180.0D;
      boolean valid = false;
      if(entity == mc.thePlayer.ridingEntity) {
         return false;
      } else if(entity.isInvisible() && !((Boolean)this.invisibles.getValue()).booleanValue()) {
         return false;
      } else {
         if(entity instanceof EntityPlayer && ((Boolean)this.players.getValue()).booleanValue()) {
                 if(entity.isInvisible() && !((Boolean)this.invisibles.getValue()).booleanValue()) {
              		return false;
                 } else if(this.mode.getValue().equals("minez")) {

                    int armorAir = 0;
                    int offset;
                    for(offset = 3; offset >= 0; --offset) {
                       ItemStack xPos = ((EntityPlayer) entity).inventory.armorInventory[offset];
                       if(xPos == null) {
                    	   armorAir += 1;
                       }
                    }

                    int inventoryAir = 0;
                    int offset1;
                    for(offset1 = 35; offset1 >= 0; --offset1) {
                       ItemStack xPos = ((EntityPlayer) entity).inventory.getStackInSlot(offset1);
                       if(xPos == null) {
                    	   inventoryAir += 1;
                       }
                    }

                    if (inventoryAir >= 36 && armorAir >= 4) {
                    	return false;
                    }
                } else if(this.mode.getValue().equals("gg") && !drawtitle.matches(".*" + entity.getName() + ".*") && !drawtitle.matches(".*" + mc.thePlayer.getName() + ".*") || this.mode.getValue().equals("gg") && entity.getName().equals(mc.thePlayer.getName()) || drawtitle == null) {
            		return false;
            	} else if(this.mode.getValue().equals("anni") && mc.thePlayer.getTeam() == ((EntityPlayer) entity).getTeam() ||this.mode.getValue().equals("anni") && ((EntityPlayer)entity).getTeam() == null) {
            		return false;
            	} else if(this.mode.getValue().equals("free") && mc.thePlayer.getTeam() == ((EntityPlayer) entity).getTeam()) {
            		return false;
            	} else if(this.mode.getValue().equals("none") && !((String)this.ignore.getValue()).equals("") && entity.getDisplayName().getFormattedText().startsWith("\u00a7" + (String)this.ignore.getValue()) ||this.mode.getValue().equals("none") && entity.getName().equalsIgnoreCase("")) {
            		return false;
            	}

            valid = entity != null && !PlayerControllerMP.entities.contains(entity) && entity != mc.thePlayer && entity.ticksExisted > ((Integer)this.ticksToWait.getValue()).intValue() && entity.isEntityAlive() && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue() && !Noxious.getFriendManager().isFriend(entity.getName());
         } else if(entity instanceof IMob && ((Boolean)this.mobs.getValue()).booleanValue()) {
            valid = entity != null && d2g < dooble && entity.ticksExisted > ((Integer)this.ticksToWait.getValue()).intValue() && entity.isEntityAlive() && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue();
         } else if(entity instanceof IAnimals && !(entity instanceof IMob) && ((Boolean)this.animals.getValue()).booleanValue()) {
            valid = entity != null && d2g < dooble && entity.ticksExisted > ((Integer)this.ticksToWait.getValue()).intValue() && entity.isEntityAlive() && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue();
         }

         return valid;
      }
   }

   public void onDisabled() {
      super.onDisabled();
      targets.clear();
      this.itemSwitchTicks = 0;
   }

   public void onEnabled() {
      super.onEnabled();
      this.itemSwitchTicks = 0;
   }

   public boolean isAttacking() {
      return this.attacking;
   }

   public void onEvent(Event event) {
      AutoPot render;
      if(event instanceof EventPreSendMotionUpdates) {
         render = (AutoPot)Noxious.getModManager().getModByName("autopot");
         EventPreSendMotionUpdates entity = (EventPreSendMotionUpdates)event;
         if(targets.isEmpty()) {
            this.populateTargets();
         }

         Iterator posX = targets.iterator();

         while(posX.hasNext()) {
            Entity entity1 = (Entity)posX.next();
            if(this.isValidTarget(entity1) && !render.isPotting()) {
               this.attacking = true;
               if(((Boolean)this.dura.getValue()).booleanValue() && !render.isPotting()) {
                  ++this.itemSwitchTicks;
                  switch(this.itemSwitchTicks) {
                  case 10:
                     ItemStack rotations;
                     if(mc.thePlayer.inventoryContainer.getSlot(9).getHasStack()) {
                        rotations = mc.thePlayer.inventoryContainer.getSlot(9).getStack();
                        if(rotations != null && rotations.getItem() instanceof ItemSword || rotations.getItem() instanceof ItemAxe) {
                           mc.playerController.windowClick(0, 9, 0, 2, mc.thePlayer);
                        }
                     }

                     if(mc.thePlayer.inventoryContainer.getSlot(18).getHasStack()) {
                        rotations = mc.thePlayer.inventoryContainer.getSlot(18).getStack();
                        if(rotations != null && rotations.getItem() instanceof ItemSword || rotations.getItem() instanceof ItemAxe) {
                           mc.playerController.windowClick(0, 18, 0, 2, mc.thePlayer);
                        }
                     }

                     if(mc.thePlayer.inventoryContainer.getSlot(27).getHasStack()) {
                        rotations = mc.thePlayer.inventoryContainer.getSlot(27).getStack();
                        if(rotations != null && rotations.getItem() instanceof ItemSword || rotations.getItem() instanceof ItemAxe) {
                           mc.playerController.windowClick(0, 27, 0, 2, mc.thePlayer);
                        }
                     }

                     this.itemSwitchTicks = 0;
                  }
               }

               float[] posX1 = EntityHelper.getEntityRotations(mc.thePlayer, nowtarget);

               entity.setYaw(posX1[0]);
               entity.setPitch(posX1[1]);

               float[] var22 = EntityHelper.getEntityRotations(mc.thePlayer, entity1);
               float posY = ListenerUtil.serverPos.rotationYaw;
               if(mc.thePlayer.moveForward < 0.0F) {
                  posY += 180.0F;
               }

               if(mc.thePlayer.moveStrafing > 0.0F) {
                  posY -= 90.0F * (mc.thePlayer.moveForward < 0.0F?-0.5F:(mc.thePlayer.moveForward > 0.0F?0.5F:1.0F));
               }

               if(mc.thePlayer.moveStrafing < 0.0F) {
                  posY += 90.0F * (mc.thePlayer.moveForward < 0.0F?-0.5F:(mc.thePlayer.moveForward > 0.0F?0.5F:1.0F));
               }

               double hOff = 4.0D;
               float var10000 = (float)((double)((float)Math.cos((double)(posY + 90.0F) * 3.141592653589793D / 180.0D)) * hOff);
               var10000 = (float)((double)((float)Math.sin((double)(posY + 90.0F) * 3.141592653589793D / 180.0D)) * hOff);
            } else {
               targets.remove(entity1);
            }
         }
      } else {
         Iterator var17;
         if(event instanceof EventOnUpdate) {
            render = (AutoPot)Noxious.getModManager().getModByName("autopot");
            var17 = targets.iterator();

            while(var17.hasNext()) {
               Entity var14 = (Entity)var17.next();
               if(!this.isValidTarget(var14)) {
                  targets.remove(var14);
               } else if((((Boolean)this.autohit.getValue()).booleanValue() || mc.thePlayer.isSwingInProgress) && ((Boolean)this.autoaim.getValue()).booleanValue() && !render.isPotting() && (((Boolean)this.autohit.getValue()).booleanValue() || mc.objectMouseOver.typeOfHit != MovingObjectPosition.MovingObjectType.ENTITY || mc.objectMouseOver.entityHit != var14)) {
                  if(this.time2.hasReached(((Long)this.speed.getValue()).longValue() + 10L)) {
                     this.time2.reset();
                  }

                  this.attack(var14);
                  this.attacking = false;
               } else if((((Boolean)this.autohit.getValue()).booleanValue() || mc.thePlayer.isSwingInProgress) && !((Boolean)this.autoaim.getValue()).booleanValue() && !render.isPotting() && !Objects.isNull(mc.objectMouseOver.entityHit)) {
                  if(this.time2.hasReached(((Long)this.speed.getValue()).longValue() + 10L)) {
                     this.time2.reset();
                  }

                  this.attack(var14);
                  this.attacking = false;
               }
            }
         } else if(event instanceof EventPacketSent) {
            EventPacketSent var12 = (EventPacketSent)event;
            AutoPot var15 = (AutoPot)Noxious.getModManager().getModByName("autopot");
            if(var12.getPacket() instanceof C03PacketPlayer) {
               C03PacketPlayer var18 = (C03PacketPlayer)var12.getPacket();
               this.serveryaw = var18.getYaw();
               this.serverpitch = var18.getPitch();
               Iterator var23 = targets.iterator();

               while(var23.hasNext()) {
                  Entity var20 = (Entity)var23.next();
                  if(this.isValidTarget(var20) && !var15.isPotting() && ((Boolean)this.autoaim.getValue()).booleanValue()) {
                     float[] var24 = EntityHelper.getEntityRotations(mc.thePlayer, var20);
                  }
               }
            }

            if(var12.getPacket() instanceof C03PacketPlayer.C05PacketPlayerLook) {
               var12.setCancelled(((Boolean)this.swing.getValue()).booleanValue());
            }

            if(var12.getPacket() instanceof C03PacketPlayer.C06PacketPlayerPosLook) {
               var12.setCancelled(((Boolean)this.swing.getValue()).booleanValue());
            }

            if(var12.getPacket() instanceof C03PacketPlayer.C05PacketPlayerLook) {
               var12.setCancelled(((Boolean)this.swing.getValue()).booleanValue());
            }

            if(var12.getPacket() instanceof C02PacketUseEntity) {
               C02PacketUseEntity var19 = (C02PacketUseEntity)var12.getPacket();
               var19.getAction();
               C02PacketUseEntity.Action var26 = C02PacketUseEntity.Action.ATTACK;
            }
         }else if (event instanceof EventDrawTitle){
        	this.drawtitle = EventDrawTitle.getTitle();
         } else if(event instanceof EventRender3D && ((Boolean)this.esp.getValue()).booleanValue()) {
        	 EventRender3D var13 = (EventRender3D)event;
               GL11.glPushMatrix();
               GL11.glDisable(3553);
               GL11.glDisable(2896);
               GL11.glEnable(3042);
               GL11.glBlendFunc(770, 771);
               GL11.glDisable(2929);
               GL11.glEnable(2848);
               GL11.glDepthMask(false);

                  EntityLivingBase var16 = (EntityLivingBase)nowtarget;
                  if(var16 != null && !var16.isDead && (double)var16.getDistanceToEntity(mc.thePlayer) <= ((Double)this.reach.getValue()).doubleValue()) {
                     double var21 = var16.lastTickPosX + (var16.posX - var16.lastTickPosX) * (double)var13.getPartialTicks() - RenderManager.renderPosX;
                     double var25 = var16.lastTickPosY + (var16.posY - var16.lastTickPosY) * (double)var13.getPartialTicks() - RenderManager.renderPosY;
                     double posZ = var16.lastTickPosZ + (var16.posZ - var16.lastTickPosZ) * (double)var13.getPartialTicks() - RenderManager.renderPosZ;
                     GL11.glPushMatrix();
                     GL11.glTranslated(var21, var25, posZ);
                     GL11.glRotatef(-var16.rotationYaw, 0.0F, var16.height, 0.0F);
                     GL11.glTranslated(-var21, -var25, -posZ);
                     this.renderBox(var16, var21, var25, posZ);
                     GL11.glPopMatrix();
                  }

               GL11.glDepthMask(true);
               GL11.glDisable(2848);
               GL11.glEnable(2929);
               GL11.glEnable(2896);
               GL11.glDisable(3042);
               GL11.glEnable(3553);
               GL11.glPopMatrix();
         }
      }

   }

   private void renderBox(Entity entity, double x, double y, double z) {
      AxisAlignedBB box = AxisAlignedBB.fromBounds(x - (double)entity.width, y, z - (double)entity.width, x + (double)entity.width, y + (double)entity.height + 0.2D, z + (double)entity.width);

      float distance = mc.thePlayer.getDistanceToEntity(entity);
      float[] color;
      if(entity instanceof EntityLivingBase && ((EntityLivingBase)entity).hurtTime > 0) {
         color = new float[]{1.0F, 0.66F, 0.0F};
      } else {
         color = new float[]{0.0F, 0.9F, 0.0F};
      }

      GL11.glColor4f(color[0], color[1], color[2], 0.8F);
      RenderHelper.drawOutlinedBoundingBox(box);
      GL11.glColor4f(color[0], color[1], color[2], 0.15F);
      RenderHelper.drawFilledBox(box);
   }

   private void populateTargets() {
	  int loop = 1;
      Iterator var2 = mc.theWorld.loadedEntityList.iterator();

      while(var2.hasNext()) {
         Object o = var2.next();
         Entity entity = (Entity)o;
         if(loop != 1 && mc.thePlayer.getDistanceToEntity(entity) <= mc.thePlayer.getDistanceToEntity(oldtarget) && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue() && !entity.getName().equals(mc.thePlayer.getName()) && this.isValidTarget(entity) && targets.size() < (((Boolean)this.gcheat.getValue()).booleanValue()?1:((Integer)this.maxTargets.getValue()).intValue()) && entity.hurtResistantTime <= 1) {
            oldtarget = entity;
         } else  if (loop == 1 && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue() && mc.thePlayer != entity && this.isValidTarget(entity) && targets.size() < (((Boolean)this.gcheat.getValue()).booleanValue()?1:((Integer)this.maxTargets.getValue()).intValue()) && entity.hurtResistantTime <= 1) {
        	 oldtarget = entity;
        	 loop += 1;
         }
      }
      if(this.isValidTarget(oldtarget) && targets.size() < (((Boolean)this.gcheat.getValue()).booleanValue()?1:((Integer)this.maxTargets.getValue()).intValue())) {
    	  targets.add(oldtarget);
          nowtarget = oldtarget;
       }
   }

   public void drawEntityESP(double d, double d1, double d2, EntityLivingBase ep, double e, double f) {
      if(!(ep instanceof EntityPlayerSP) && !ep.isDead) {
         GL11.glPushMatrix();
         GLUtil.setGLCap(3042, true);
         GLUtil.setGLCap(3553, false);
         GLUtil.setGLCap(2896, false);
         GLUtil.setGLCap(2929, false);
         GL11.glDepthMask(false);
         GL11.glLineWidth(1.8F);
         GL11.glBlendFunc(770, 771);
         GLUtil.setGLCap(2848, true);
         GL11.glColor4f(2.55F, 0.69F, 0.0F, 0.2392157F);
         RenderHelper.drawFilledBox(new AxisAlignedBB(d - f, d1 + 0.1D, d2 - f, d + f, d1 + e + 0.25D, d2 + f));
         GL11.glColor4f(0.55F, 0.55F, 0.55F, 1.0F);
         RenderHelper.drawOutlinedBoundingBox(new AxisAlignedBB(d - f, d1 + 0.1D, d2 - f, d + f, d1 + e + 0.25D, d2 + f));
         GL11.glDepthMask(true);
         GLUtil.revertAllCaps();
         GL11.glPopMatrix();
      }

   }
}
