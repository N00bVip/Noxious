package noxious.mod.mods;

import net.minecraft.network.play.server.S12PacketEntityVelocity;
import net.minecraft.network.play.server.S27PacketExplosion;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPacketReceive;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.value.Value;

public class NoVelocity extends Mod {

   private final Value<Float> velocity = new Value("novelocity_velocity", Float.valueOf(0.0F));


   public NoVelocity() {
      super("NoVelocity", -4731698, ModManager.Category.PLAYER);
      this.setTag("No Velocity");
      Noxious.getCommandManager().getContents().add(new Command("novelocityvelocity", "<amount>", new String[]{"novelvel", "nvv"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               NoVelocity.this.velocity.setValue((Float)NoVelocity.this.velocity.getDefaultValue());
            } else {
               NoVelocity.this.velocity.setValue(Float.valueOf(Float.parseFloat(message.split(" ")[1])));
            }

            if(((Float)NoVelocity.this.velocity.getValue()).floatValue() > 10.0F) {
               NoVelocity.this.velocity.setValue(Float.valueOf(10.0F));
            } else if(((Float)NoVelocity.this.velocity.getValue()).floatValue() < -10.0F) {
               NoVelocity.this.velocity.setValue(Float.valueOf(-10.0F));
            }

            Logger.logChat("No Velocity Velocity set to: " + NoVelocity.this.velocity.getValue());
         }
      });
   }

   public void onEvent(Event e) {
      if(e instanceof EventPacketReceive) {
    	  EventPacketReceive event = (EventPacketReceive)e;
         if(event.getPacket() instanceof S12PacketEntityVelocity && ((S12PacketEntityVelocity)event.getPacket()).func_149412_c() == mc.thePlayer.getEntityId()) {
            S12PacketEntityVelocity packet = (S12PacketEntityVelocity)event.getPacket();
            packet.field_149415_b = (int)((float)packet.field_149415_b * ((Float)this.velocity.getValue()).floatValue());
            packet.field_149416_c = (int)((float)packet.field_149416_c * ((Float)this.velocity.getValue()).floatValue());
            packet.field_149414_d = (int)((float)packet.field_149414_d * ((Float)this.velocity.getValue()).floatValue());
            if(packet.field_149415_b == 0 && packet.field_149416_c == 0 && packet.field_149414_d == 0) {
               event.setCancelled(true);
            }
         }

         if(event.getPacket() instanceof S27PacketExplosion) {
            S27PacketExplosion packet1 = (S27PacketExplosion)event.getPacket();
            packet1.field_149152_f *= ((Float)this.velocity.getValue()).floatValue();
            packet1.field_149153_g *= ((Float)this.velocity.getValue()).floatValue();
            packet1.field_149159_h *= ((Float)this.velocity.getValue()).floatValue();
         }
      }

   }
}
