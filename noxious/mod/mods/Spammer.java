package noxious.mod.mods;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Pattern;

import net.minecraft.network.play.client.C01PacketChatMessage;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventTick;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public final class Spammer extends Mod
{
    public Value<Long> delay = new Value("spammer_delay", Long.valueOf(2000L));
    public Value<Boolean> filemode = new Value("spammer_file", Boolean.valueOf(false));
    public Value<String> message = new Value("spammer_message", "");
    public List<String> messages = new CopyOnWriteArrayList();
    public Random rand = new Random();
    public TimeHelper time = new TimeHelper();

    public Spammer()
    {
        super("Spammer");
        Noxious.getCommandManager().getContents().add(new Command("spam", "<message/file/delay>", new String[] {"s"})
        {
            public void run(String message)
            {
                String[] arguments = message.split(" ");
                String arg = arguments[1];

                if (arg.equalsIgnoreCase("message"))
                {
                    Spammer.this.message.setValue(message.substring((".spam " + arg + " ").length()));
                    Logger.logChat("Spammer Message set to: \"" + (String)Spammer.this.message.getValue() + "\"");
                }
                else if (arg.equalsIgnoreCase("file"))
                {
                    Spammer.this.filemode.setValue(Boolean.valueOf(!((Boolean)Spammer.this.filemode.getValue()).booleanValue()));
                    Logger.logChat("Spammer will " + (((Boolean)Spammer.this.filemode.getValue()).booleanValue() ? "now" : "no longer") + " use messages in the \"spam.txt\" file.");
                }
                else if (arg.equalsIgnoreCase("delay"))
                {
                    if (message.split(" ")[2].equalsIgnoreCase("-d"))
                    {
                        Spammer.this.delay.setValue((Long)Spammer.this.delay.getDefaultValue());
                    }
                    else
                    {
                        Spammer.this.delay.setValue(Long.valueOf(Long.parseLong(arguments[2])));
                    }

                    if (((Long)Spammer.this.delay.getValue()).longValue() < 500L)
                    {
                        Spammer.this.delay.setValue(Long.valueOf(500L));
                    }

                    Logger.logChat("Spammer Delay set to: " + Spammer.this.delay.getValue());
                }
                else
                {
                    Logger.logChat("Invalid option! Valid options: message, file, delay");
                }
            }
        });
    }

    public List<String> getMessages()
    {
        return this.messages;
    }

    public void onEnabled()
    {
        super.onEnabled();

        if (this.messages.isEmpty())
        {
            Noxious.getFileManager().getFileByName("spammessages").loadFile();
        }
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventTick)
        {
            if (mc.thePlayer == null || mc.theWorld == null)
            {
                return;
            }

            if (this.time.hasReached((float)((Long)this.delay.getValue()).longValue()))
            {
                Random randomNum = new Random();

                for (int msg = 1; msg <= 10; ++msg)
                {
                    int var4 = randomNum.nextInt(100);
                }

                String var5 = "" + randomNum;

                if (((Boolean)this.filemode.getValue()).booleanValue())
                {
                    if (this.messages.isEmpty())
                    {
                        Logger.logChat("There are no messages to spam! File is either empty, or file spammer has complete.");
                        this.toggle();
                        return;
                    }

                    if (!this.messages.isEmpty())
                    {
                        var5 = (String)this.messages.get(0);
                        this.messages.remove(this.messages.get(0));
                    }
                }
                else
                {
                    if (((String)this.message.getValue()).equals(""))
                    {
                        Logger.logChat("You need to set a spam message! Type \".spam message <message>\"");
                        this.toggle();
                        return;
                    }

                    var5 = (String)this.message.getValue();
                }

                mc.getNetHandler().addToSendQueue(new C01PacketChatMessage(var5.replaceAll(Pattern.quote("$r"), "" + this.rand.nextInt(9999))));
                this.time.reset();
            }
        }
    }
}
