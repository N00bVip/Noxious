package noxious.mod.mods;

import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class Firion extends Mod {

   private final Value<Boolean> fire = new Value("firion_antifire", Boolean.valueOf(false));
   private final Value<Boolean> potion = new Value("firion_antipotion", Boolean.valueOf(true));
   private String potionName = "";
   private final TimeHelper time = new TimeHelper();
   private final TimeHelper time2 = new TimeHelper();


   public Firion() {
      super("Firion", -8388608, ModManager.Category.EXPLOITS);
      Noxious.getCommandManager().getContents().add(new Command("firion", "<antifire/antipotions>", new String[0]) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("antifire")) {
               Firion.this.fire.setValue(Boolean.valueOf(!((Boolean)Firion.this.fire.getValue()).booleanValue()));
               Logger.logChat("Firion will " + (((Boolean)Firion.this.fire.getValue()).booleanValue()?"now":"no longer") + " get rid of fire.");
            } else if(message.split(" ")[1].equalsIgnoreCase("antipotions")) {
               Firion.this.potion.setValue(Boolean.valueOf(!((Boolean)Firion.this.potion.getValue()).booleanValue()));
               Logger.logChat("Firion will " + (((Boolean)Firion.this.potion.getValue()).booleanValue()?"now":"no longer") + " get rid of bad potion effects.");
            } else {
               Logger.logChat("Option not valid! Available options: antifire, anitpotions.");
            }

         }
      });
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
         if(((Boolean)this.potion.getValue()).booleanValue()) {
            Potion[] var5 = Potion.potionTypes;
            int var4 = Potion.potionTypes.length;

            for(int var3 = 0; var3 < var4; ++var3) {
               Potion x = var5[var3];
               if(x != null && x.isBadEffect()) {
                  this.potionName = x.getName();
                  PotionEffect effect = mc.thePlayer.getActivePotionEffect(x);
                  if(effect != null) {
                     if(this.time.hasReached(1000L) && effect.getDuration() < 10000) {
                        this.time.reset();
                     }

                     if (Potion.potionTypes[effect.getPotionID()].getId() == Potion.moveSlowdown.getId()) {
  	        			 Potion.moveSlowdown.removeAttributesModifiersFromEntity(mc.thePlayer, mc.thePlayer.getAttributeMap(),  255);
  	        			 mc.thePlayer.setAIMoveSpeed(0.13000001F);
                	 }

                     if (Potion.potionTypes[effect.getPotionID()].getId() == Potion.jump.getId()) {
                    	 mc.thePlayer.getDataWatcher().updateObject(8, Byte.valueOf((byte) 1));
                         mc.thePlayer.removePotionEffectClient(Potion.jump.id);
                         Potion.jump.removeAttributesModifiersFromEntity(mc.thePlayer, mc.thePlayer.getAttributeMap(),  255);
                	 }

                     if(effect.getDuration() < 10000 && mc.thePlayer.onGround) {
                        for(int i = 0; i < effect.getDuration() / 20; ++i) {
                           mc.getNetHandler().addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
                        }
                     }
                  }
               }
            }
         }

         if(((Boolean)this.fire.getValue()).booleanValue() && mc.thePlayer.isBurning()) {
            if(this.time2.hasReached(5000L) && mc.thePlayer.getActivePotionEffect(Potion.fireResistance) == null) {
               this.time2.reset();
            }

            if(mc.thePlayer.getActivePotionEffect(Potion.fireResistance) == null) {
               for(int var9 = 0; var9 < 120; ++var9) {
                  mc.getNetHandler().addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
               }
            }
         }
      }

   }
}
