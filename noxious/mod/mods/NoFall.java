package noxious.mod.mods;

import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPacketSent;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class NoFall
  extends Mod
{
  private Value<Boolean> nocheat = new Value("nofall_nocheat", Boolean.valueOf(false));
  private final TimeHelper time = new TimeHelper();

  public NoFall()
  {
    super("NoFall", -3730043, ModManager.Category.PLAYER);
    setTag("No Fall");
    Noxious.getCommandManager().getContents().add(new Command("nofallnocheat", "none", new String[] { "nfnocheat", "nfnc" })
    {
      public void run(String message)
      {
        NoFall.this.nocheat.setValue(Boolean.valueOf(!((Boolean)NoFall.this.nocheat.getValue()).booleanValue()));
        Logger.logChat("No Fall will " + (((Boolean)NoFall.this.nocheat.getValue()).booleanValue() ? "now" : "no longer") + " bypass nocheat.");
      }
    });
  }

  public void onEnabled()
  {
    super.onEnabled();
    if (mc.thePlayer != null)
    {
      if (((Boolean)this.nocheat.getValue()).booleanValue())
      {
        double x = mc.thePlayer.posX;
        double y = mc.thePlayer.posY;
        double z = mc.thePlayer.posZ;
        for (int i = 0; i < 4; i++)
        {
          Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y + 1.01D, z, false));
          Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y, z, false));
        }
        Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y + 0.8D, z, false));
        mc.thePlayer.motionY = 0.1D;
      }
      this.time.reset();
      Logger.logChat("You have to hit yourself before using it, after you touch the ground you have to walk until you are in a water pool and after that untoggle it. If you toggle it before, you will take fall damage.");
    }
  }

  public void onEvent(Event event)
  {
    if ((event instanceof EventPacketSent))
    {
      EventPacketSent sent = (EventPacketSent)event;
      if ((sent.getPacket() instanceof C03PacketPlayer))
      {
        C03PacketPlayer player = (C03PacketPlayer)sent.getPacket();
        if (((Boolean)this.nocheat.getValue()).booleanValue())
        {
          float offset = 0.6F;
          if ((mc.thePlayer.onGround) && (offset >= 0.1F) && (this.time.hasReached(200.0F)))
          {
            offset = (float)(offset - 0.1D);
            this.time.reset();
          }
          if (((Boolean)this.nocheat.getValue()).booleanValue())
          {
            player.x = mc.thePlayer.posX;
            player.y = (mc.thePlayer.posY + 0.6000000238418579D);
            player.field_149474_g = false;
          }
        }
        else if (mc.thePlayer.fallDistance >= 3.0F)
        {
          player.field_149474_g = true;
        }
      }
    }
  }
}
