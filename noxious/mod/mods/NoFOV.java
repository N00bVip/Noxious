package noxious.mod.mods;

import net.minecraft.init.Items;
import noxious.event.Event;
import noxious.event.events.EventFOVChange;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class NoFOV extends Mod {

   public NoFOV() {
      super("NoFOV");
      this.setCategory(Category.PLAYER);
   }

   public void onEvent(Event event) {
      if(event instanceof EventFOVChange) {
    	  EventFOVChange fov = (EventFOVChange)event;
         float newfov = 1.0F;
         if(mc.thePlayer.isUsingItem() && mc.thePlayer.getItemInUse().getItem() == Items.bow) {
            int duration = mc.thePlayer.getItemInUseDuration();
            float dd = (float)duration / 20.0F;
            if(dd > 1.0F) {
               dd = 1.0F;
            } else {
               dd *= dd;
            }

            newfov *= 1.0F - dd * 0.15F;
         }

         fov.setFOV(newfov);
      }

   }
}
