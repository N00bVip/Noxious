package noxious.mod.mods;

import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.value.Value;

public class Sprint extends Mod {
	private final Value<Boolean> particle = new Value("sprint_particle", Boolean.valueOf(false));

public Sprint() {
      super("Sprint", -7536856, ModManager.Category.MOVEMENT);
      this.setEnabled(true);
      Noxious.getCommandManager().getContents().add(new Command("sprint", "<particle>", new String[0]) {
          public void run(String message) {
             if(message.split(" ")[1].equalsIgnoreCase("particle") && !type.equals("0")) {
                Sprint.this.particle.setValue(Boolean.valueOf(!((Boolean)Sprint.this.particle.getValue()).booleanValue()));
                Logger.logChat("Sprint will " + (((Boolean)Sprint.this.particle.getValue()).booleanValue()?"now":"no longer") + " don't spawn  sprint particle");
             }
          }
       });
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
    	  if(mc.thePlayer != null && mc.thePlayer.getFoodStats().getFoodLevel() > 6 && !mc.gameSettings.keyBindSneak.pressed && (mc.gameSettings.keyBindForward.pressed || mc.gameSettings.keyBindLeft.pressed || mc.gameSettings.keyBindRight.pressed || mc.gameSettings.keyBindBack.pressed)) {
    		  this.setColor(-7536856);
    		  mc.thePlayer.setSprinting(true);
    	  } else {
    		  this.setColor(-8355712);
    	  }
      }
   }

   public Boolean getParticle() {
	   return particle.getValue();
   }
}
