package noxious.mod.mods;

import noxious.event.Event;
import noxious.event.events.EventHurtCam;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class AntiHurtCam extends Mod
{
    public AntiHurtCam()
    {
        super("AntiHurtCam");
        this.setTag("Anti Hurt Cam");
        this.setCategory(Category.WORLD);
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventHurtCam)
        {
        	EventHurtCam hc = (EventHurtCam)event;
            hc.setCancelled(true);
        }
    }
}
