package noxious.mod.mods;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.BlockAir;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.item.ItemBlock;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C0APacketAnimation;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import noxious.event.Event;
import noxious.event.events.EventBlockBreaking;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.RenderHelper;

public class TestCivBreak
  extends Mod
{
  private BlockPos block;
  private EnumFacing side;

  public TestCivBreak()
  {
    super("TestCivBreak", -6165654, ModManager.Category.EXPLOITS);
    setTag("Test CivBreak");
  }

  public void onEvent(Event event)
  {
    if ((event instanceof EventBlockBreaking))
    {
      EventBlockBreaking x = (EventBlockBreaking)event;
      if (x.getState() == EventBlockBreaking.EnumBlock.CLICK)
      {
        this.block = x.getPos();
        this.side = x.getSide();
      }
    }
    if (((event instanceof EventPreSendMotionUpdates)))  {
      EventPreSendMotionUpdates var15 = (EventPreSendMotionUpdates)event;
      if (this.block != null && mc.thePlayer.getDistanceSq(this.block) < 22.399999618530273D) {
	      float[] rotations = BlockHelper.getBlockRotations(this.block.getX(), this.block.getY(), this.block.getZ());
	      var15.setYaw(rotations[0]);
	      var15.setPitch(rotations[1]);
	      for (int i = 0; i < 10; i++) {
	    	  if (this.block != null && mc.thePlayer.getDistanceSq(this.block) < 22.399999618530273D) {
	    		  mc.thePlayer.swingItem();
	    		  mc.playerController.func_180512_c(this.block, this.side);
	    		  if (mc.playerController.blockHitDelay > 0) {
	    			  mc.playerController.blockHitDelay = 0;
	    		  }
	    		  mc.getNetHandler().addToSendQueue(new C0APacketAnimation());
	    		  mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, this.block, this.side));
	    		  mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, this.block, this.side));
	    		  if ((mc.thePlayer.getHeldItem() != null) && (!(mc.thePlayer.getHeldItem().getItem() instanceof ItemBlock))) {
	    			  mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(this.block, -1, mc.thePlayer.getCurrentEquippedItem(), 0.0F, 0.0F, 0.0F));
	    		  }
	    	  } else {
	    		  this.block = null;
	    	  }
	      }
      } else {
    	  this.block = null;
      }
    }
    if (((event instanceof EventRender3D)) && (this.block != null)) {
      GL11.glDisable(2896);
      GL11.glDisable(3553);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      GL11.glDisable(2929);
      GL11.glEnable(2848);
      GL11.glDepthMask(false);
      GL11.glLineWidth(0.75F);
      if ((this.block != null) && (mc.thePlayer.getDistanceSq(this.block) >= 22.399999618530273D)) {
        GL11.glColor4f(1.0F, 0.2F, 0.0F, 1.0F);
      } else if ((mc.theWorld.getBlockState(this.block).getBlock() instanceof BlockAir)) {
        GL11.glColor4f(1.0F, 0.7F, 0.0F, 1.0F);
      } else {
        GL11.glColor4f(0.2F, 0.9F, 0.0F, 1.0F);
      }
      double var10000 = this.block.getX();
      mc.getRenderManager();
      double var17 = var10000 - RenderManager.renderPosX;
      var10000 = this.block.getY();
      mc.getRenderManager();
      double y = var10000 - RenderManager.renderPosY;
      var10000 = this.block.getZ();
      mc.getRenderManager();
      double z = var10000 - RenderManager.renderPosZ;
      double xo = 1.0D;
      double yo = 1.0D;
      double zo = 1.0D;
      RenderHelper.drawLines(new AxisAlignedBB(var17, y, z, var17 + xo, y + yo, z + zo));
      RenderHelper.drawOutlinedBoundingBox(new AxisAlignedBB(var17, y, z, var17 + xo, y + yo, z + zo));
      if ((this.block != null) && (mc.thePlayer.getDistanceSq(this.block) >= 22.399999618530273D)) {
        GL11.glColor4f(1.0F, 0.2F, 0.0F, 0.11F);
      } else if ((mc.theWorld.getBlockState(this.block).getBlock() instanceof BlockAir)) {
        GL11.glColor4f(1.0F, 0.7F, 0.0F, 0.11F);
      } else {
        GL11.glColor4f(0.2F, 0.9F, 0.0F, 0.11F);
      }
      RenderHelper.drawFilledBox(new AxisAlignedBB(var17, y, z, var17 + xo, y + yo, z + zo));
      GL11.glDepthMask(true);
      GL11.glDisable(2848);
      GL11.glEnable(2929);
      GL11.glDisable(3042);
      GL11.glEnable(2896);
      GL11.glEnable(3553);
    }
  }
}
