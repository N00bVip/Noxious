package noxious.mod.mods;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.settings.KeyBinding;
import noxious.event.Event;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;

public class InventoryMove extends Mod {

   public InventoryMove() {
      super("InventoryMove", -3308225, ModManager.Category.MOVEMENT);
      this.setTag("Inventory Move");
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates && mc.currentScreen != null && !(mc.currentScreen instanceof GuiChat)) {
         KeyBinding[] moveKeys = new KeyBinding[]{mc.gameSettings.keyBindForward, mc.gameSettings.keyBindBack, mc.gameSettings.keyBindLeft, mc.gameSettings.keyBindRight, mc.gameSettings.keyBindJump};
         KeyBinding[] array = moveKeys;
         int length = moveKeys.length;

         for(int i = 0; i < length; ++i) {
            KeyBinding bind = array[i];
            KeyBinding.setKeyBindState(bind.getKeyCode(), Keyboard.isKeyDown(bind.getKeyCode()));
         }
      }

   }
}
