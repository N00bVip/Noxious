package noxious.mod.mods;

import java.util.Iterator;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntityBrewingStand;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityDispenser;
import net.minecraft.tileentity.TileEntityDropper;
import net.minecraft.tileentity.TileEntityEnderChest;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.tileentity.TileEntityHopper;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import noxious.event.Event;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.RenderHelper;

public class StorageESP extends Mod {

   public StorageESP() {
      super("StorageESP", -12621684, ModManager.Category.RENDER);
      this.setTag("Storage ESP");
   }

   public void draw(Block block, double x, double y, double z, double xo, double yo, double zo) {
      GL11.glDisable(2896);
      GL11.glDisable(3553);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      GL11.glDisable(2929);
      GL11.glEnable(2848);
      GL11.glDepthMask(false);
      GL11.glLineWidth(0.75F);
      if(block == Blocks.ender_chest) {
         GL11.glColor4f(0.4F, 0.2F, 1.0F, 1.0F);
         x += 0.05000000074505806D;
         y += 0.0D;
         z += 0.05000000074505806D;
         xo -= 0.10000000149011612D;
         yo -= 0.10000000149011612D;
         zo -= 0.10000000149011612D;
      } else if(block == Blocks.chest) {
         GL11.glColor4f(1.0F, 1.0F, 0.0F, 1.0F);
         x += 0.05000000074505806D;
         y += 0.0D;
         z += 0.05000000074505806D;
         xo -= 0.10000000149011612D;
         yo -= 0.10000000149011612D;
         zo -= 0.10000000149011612D;
      } else if(block == Blocks.trapped_chest) {
         GL11.glColor4f(1.0F, 0.6F, 0.0F, 1.0F);
         x += 0.05000000074505806D;
         y += 0.0D;
         z += 0.05000000074505806D;
         xo -= 0.10000000149011612D;
         yo -= 0.10000000149011612D;
         zo -= 0.10000000149011612D;
      } else if(block == Blocks.brewing_stand) {
         GL11.glColor4f(1.0F, 0.3F, 0.3F, 1.0F);
         x += 0.05000000074505806D;
         y += 0.0D;
         z += 0.05000000074505806D;
         xo -= 0.10000000149011612D;
         yo -= 0.10000000149011612D;
         zo -= 0.10000000149011612D;
      } else if(block == Blocks.furnace) {
         GL11.glColor4f(0.6F, 0.6F, 0.6F, 1.0F);
      } else if(block == Blocks.lit_furnace) {
         GL11.glColor4f(1.0F, 0.4F, 0.0F, 1.0F);
      } else if(block == Blocks.hopper || block == Blocks.dispenser || block == Blocks.dropper) {
         GL11.glColor4f(0.3F, 0.3F, 0.3F, 1.0F);
      }

      RenderHelper.drawLines(new AxisAlignedBB(x, y, z, x + xo, y + yo, z + zo));
      RenderHelper.drawOutlinedBoundingBox(new AxisAlignedBB(x, y, z, x + xo, y + yo, z + zo));
      if(block == Blocks.ender_chest) {
         GL11.glColor4f(0.4F, 0.2F, 1.0F, 0.11F);
      } else if(block == Blocks.chest) {
         GL11.glColor4f(1.0F, 1.0F, 0.0F, 0.11F);
      } else if(block == Blocks.trapped_chest) {
         GL11.glColor4f(1.0F, 0.6F, 0.0F, 0.11F);
      } else if(block == Blocks.brewing_stand) {
         GL11.glColor4f(1.0F, 0.3F, 0.3F, 0.11F);
      } else if(block == Blocks.furnace) {
         GL11.glColor4f(0.6F, 0.6F, 0.6F, 0.11F);
      } else if(block == Blocks.lit_furnace) {
         GL11.glColor4f(1.0F, 0.4F, 0.0F, 0.11F);
      } else if(block == Blocks.hopper || block == Blocks.dispenser || block == Blocks.dropper) {
         GL11.glColor4f(0.3F, 0.3F, 0.3F, 0.11F);
      }

      RenderHelper.drawFilledBox(new AxisAlignedBB(x, y, z, x + xo, y + yo, z + zo));
      GL11.glDepthMask(true);
      GL11.glDisable(2848);
      GL11.glEnable(2929);
      GL11.glDisable(3042);
      GL11.glEnable(2896);
      GL11.glEnable(3553);
   }

   public void onEvent(Event event) {
      if(event instanceof EventRender3D) {
         Iterator var3 = mc.theWorld.loadedTileEntityList.iterator();

         while(var3.hasNext()) {
            Object o = var3.next();
            if(o instanceof TileEntityChest) {
               TileEntityChest entity = (TileEntityChest)o;
               Block x = mc.theWorld.getBlockState(entity.getPos()).getBlock();
               Block border = mc.theWorld.getBlockState(new BlockPos(entity.getPos().getX(), entity.getPos().getY(), entity.getPos().getZ() - 1)).getBlock();
               Block y = mc.theWorld.getBlockState(new BlockPos(entity.getPos().getX(), entity.getPos().getY(), entity.getPos().getZ() + 1)).getBlock();
               Block border3 = mc.theWorld.getBlockState(new BlockPos(entity.getPos().getX() - 1, entity.getPos().getY(), entity.getPos().getZ())).getBlock();
               Block z = mc.theWorld.getBlockState(new BlockPos(entity.getPos().getX() + 1, entity.getPos().getY(), entity.getPos().getZ())).getBlock();
               double x1 = (double)entity.getPos().getX() - mc.getRenderManager().viewerPosX;
               double y1 = (double)entity.getPos().getY() - mc.getRenderManager().viewerPosY;
               double z1 = (double)entity.getPos().getZ() - mc.getRenderManager().viewerPosZ;
               GL11.glPushMatrix();
               net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
               if(x == Blocks.chest) {
                  if(border != Blocks.chest) {
                     if(y == Blocks.chest) {
                        this.draw(Blocks.chest, x1, y1, z1, 1.0D, 1.0D, 2.0D);
                     } else if(z == Blocks.chest) {
                        this.draw(Blocks.chest, x1, y1, z1, 2.0D, 1.0D, 1.0D);
                     } else if(z == Blocks.chest) {
                        this.draw(Blocks.chest, x1, y1, z1, 1.0D, 1.0D, 1.0D);
                     } else if(border != Blocks.chest && y != Blocks.chest && border3 != Blocks.chest && z != Blocks.chest) {
                        this.draw(Blocks.chest, x1, y1, z1, 1.0D, 1.0D, 1.0D);
                     }
                  }
               } else if(x == Blocks.trapped_chest && border != Blocks.trapped_chest) {
                  if(y == Blocks.trapped_chest) {
                     this.draw(Blocks.trapped_chest, x1, y1, z1, 1.0D, 1.0D, 2.0D);
                  } else if(z == Blocks.trapped_chest) {
                     this.draw(Blocks.trapped_chest, x1, y1, z1, 2.0D, 1.0D, 1.0D);
                  } else if(z == Blocks.trapped_chest) {
                     this.draw(Blocks.trapped_chest, x1, y1, z1, 1.0D, 1.0D, 1.0D);
                  } else if(border != Blocks.trapped_chest && y != Blocks.trapped_chest && border3 != Blocks.trapped_chest && z != Blocks.trapped_chest) {
                     this.draw(Blocks.trapped_chest, x1, y1, z1, 1.0D, 1.0D, 1.0D);
                  }
               }

               net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
               GL11.glPopMatrix();
            } else {
               double x2;
               double y2;
               double z2;
               if(o instanceof TileEntityEnderChest) {
                  TileEntityEnderChest entity1 = (TileEntityEnderChest)o;
                  x2 = (double)entity1.getPos().getX() - mc.getRenderManager().viewerPosX;
                  y2 = (double)entity1.getPos().getY() - mc.getRenderManager().viewerPosY;
                  z2 = (double)entity1.getPos().getZ() - mc.getRenderManager().viewerPosZ;
                  GL11.glPushMatrix();
                  net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                  this.draw(Blocks.ender_chest, x2, y2, z2, 1.0D, 1.0D, 1.0D);
                  net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
                  GL11.glPopMatrix();
               } else if(o instanceof TileEntityDropper) {
                  TileEntityDropper entity2 = (TileEntityDropper)o;
                  x2 = (double)entity2.getPos().getX() - mc.getRenderManager().viewerPosX;
                  y2 = (double)entity2.getPos().getY() - mc.getRenderManager().viewerPosY;
                  z2 = (double)entity2.getPos().getZ() - mc.getRenderManager().viewerPosZ;
                  GL11.glPushMatrix();
                  net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                  this.draw(Blocks.dropper, x2, y2, z2, 1.0D, 1.0D, 1.0D);
                  net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
                  GL11.glPopMatrix();
               } else if(o instanceof TileEntityDispenser) {
                  TileEntityDispenser entity3 = (TileEntityDispenser)o;
                  x2 = (double)entity3.getPos().getX() - mc.getRenderManager().viewerPosX;
                  y2 = (double)entity3.getPos().getY() - mc.getRenderManager().viewerPosY;
                  z2 = (double)entity3.getPos().getZ() - mc.getRenderManager().viewerPosZ;
                  GL11.glPushMatrix();
                  net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                  this.draw(Blocks.dispenser, x2, y2, z2, 1.0D, 1.0D, 1.0D);
                  net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
                  GL11.glPopMatrix();
               } else if(o instanceof TileEntityHopper) {
                  TileEntityHopper entity4 = (TileEntityHopper)o;
                  x2 = (double)entity4.getPos().getX() - mc.getRenderManager().viewerPosX;
                  y2 = (double)entity4.getPos().getY() - mc.getRenderManager().viewerPosY;
                  z2 = (double)entity4.getPos().getZ() - mc.getRenderManager().viewerPosZ;
                  GL11.glPushMatrix();
                  net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                  this.draw(Blocks.hopper, x2, y2, z2, 1.0D, 1.0D, 1.0D);
                  net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
                  GL11.glPopMatrix();
               } else if(o instanceof TileEntityFurnace) {
                  TileEntityFurnace entity5 = (TileEntityFurnace)o;
                  x2 = (double)entity5.getPos().getX() - mc.getRenderManager().viewerPosX;
                  y2 = (double)entity5.getPos().getY() - mc.getRenderManager().viewerPosY;
                  z2 = (double)entity5.getPos().getZ() - mc.getRenderManager().viewerPosZ;
                  GL11.glPushMatrix();
                  net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                  if(entity5.getBlockType() == Blocks.lit_furnace) {
                     this.draw(Blocks.lit_furnace, x2, y2, z2, 1.0D, 1.0D, 1.0D);
                  } else {
                     this.draw(Blocks.furnace, x2, y2, z2, 1.0D, 1.0D, 1.0D);
                  }

                  net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
                  GL11.glPopMatrix();
               } else if(o instanceof TileEntityBrewingStand) {
                  TileEntityBrewingStand entity6 = (TileEntityBrewingStand)o;
                  x2 = (double)entity6.getPos().getX() - mc.getRenderManager().viewerPosX;
                  y2 = (double)entity6.getPos().getY() - mc.getRenderManager().viewerPosY;
                  z2 = (double)entity6.getPos().getZ() - mc.getRenderManager().viewerPosZ;
                  GL11.glPushMatrix();
                  net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                  this.draw(Blocks.brewing_stand, x2, y2, z2, 1.0D, 1.0D, 1.0D);
                  net.minecraft.client.renderer.RenderHelper.enableStandardItemLighting();
                  GL11.glPopMatrix();
               }
            }
         }
      }

   }
}
