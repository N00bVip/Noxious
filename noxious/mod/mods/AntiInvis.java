package noxious.mod.mods;

import java.util.Iterator;

import net.minecraft.entity.Entity;
import noxious.event.Event;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class AntiInvis extends Mod
{
    public AntiInvis()
    {
        super("AntiInvis");
        this.setEnabled(true);
        this.setCategory(Category.RENDER);
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventPreSendMotionUpdates)
        {
            Iterator var3 = mc.theWorld.loadedEntityList.iterator();

            while (var3.hasNext())
            {
                Object o = var3.next();
                Entity e = (Entity)o;

                if (e != null && e != mc.thePlayer && !e.isDead)
                {
                    e.setInvisible(false);
                }
            }
        }
    }
}
