package noxious.mod.mods;

import net.minecraft.block.BlockCactus;
import net.minecraft.util.AxisAlignedBB;
import noxious.event.Event;
import noxious.event.events.EventBlockBoundingBox;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class AntiCactus extends Mod
{
    public AntiCactus()
    {
        super("AntiCactus");
        this.setEnabled(true);
        this.setCategory(Category.WORLD);
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventBlockBoundingBox)
        {
            EventBlockBoundingBox bb = (EventBlockBoundingBox)event;

            if (bb.getBlock() instanceof BlockCactus)
            {
                bb.setBoundingBox(new AxisAlignedBB((double)bb.getX(), (double)bb.getY(), (double)bb.getZ(), (double)((float)bb.getX() + 1.0F), (double)((float)bb.getY() + 1.0F), (double)((float)bb.getZ() + 1.0F)));
            }
        }
    }
}
