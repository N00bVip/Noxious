package noxious.mod.mods;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventWalking;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockData;
import noxious.util.EntityHelper;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class ScaffoldWalk extends Mod {

   private final Value<Boolean> slowmode = new Value("scaffoldwalk_slowmode", Boolean.valueOf(false));
   private BlockData blockData = null;
   private TimeHelper time = new TimeHelper();


   public ScaffoldWalk() {
      super("ScaffoldWalk", -4539718, ModManager.Category.WORLD);
      this.setTag("Scaffold Walk");
      Noxious.getCommandManager().getContents().add(new Command("scaffoldwalkslowmode", "none", new String[]{"sfwslowmode", "swsm"}) {
         public void run(String message) {
            ScaffoldWalk.this.slowmode.setValue(Boolean.valueOf(!((Boolean)ScaffoldWalk.this.slowmode.getValue()).booleanValue()));
            Logger.logChat("Scaffold Walk will " + (((Boolean)ScaffoldWalk.this.slowmode.getValue()).booleanValue()?"now":"no longer") + " slowly place the blocks.");
         }
      });
   }

   public void onEnabled() {
      super.onEnabled();
      this.blockData = null;
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
    	  EventPreSendMotionUpdates pre = (EventPreSendMotionUpdates)event;
         this.blockData = null;
         if(mc.thePlayer.getHeldItem() != null && !mc.thePlayer.isSneaking() && mc.thePlayer.getHeldItem().getItem() instanceof ItemBlock) {
            BlockPos blockBelow = new BlockPos(mc.thePlayer.posX, mc.thePlayer.posY - 1.0D, mc.thePlayer.posZ);
            if(mc.theWorld.getBlockState(blockBelow).getBlock() == Blocks.air) {
               this.blockData = this.getBlockData(blockBelow);
               if(this.blockData != null) {
                  float[] values = EntityHelper.getFacingRotations(this.blockData.position.getX(), this.blockData.position.getY(), this.blockData.position.getZ(), this.blockData.face);
                  pre.setYaw(values[0]);
                  pre.setPitch(values[1]);
               }
            }
         }
      } else if(event instanceof EventPostSendMotionUpdates) {
         if(this.blockData == null) {
            return;
         }

         if(this.time.hasReached(((Boolean)this.slowmode.getValue()).booleanValue()?250L:75L)) {
            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING));
            if(mc.playerController.func_178890_a(mc.thePlayer, mc.theWorld, mc.thePlayer.getHeldItem(), this.blockData.position, this.blockData.face, new Vec3((double)this.blockData.position.getX(), (double)this.blockData.position.getY(), (double)this.blockData.position.getZ()))) {
               mc.thePlayer.swingItem();
            }

            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING));
            this.time.reset();
         }
      } else if(event instanceof EventWalking) {
         ((EventWalking)event).setSafeWalk(true);
      }

   }

   public BlockData getBlockData(BlockPos pos) {
      return mc.theWorld.getBlockState(pos.add(0, -1, 0)).getBlock() != Blocks.air?new BlockData(pos.add(0, -1, 0), EnumFacing.UP):(mc.theWorld.getBlockState(pos.add(-1, 0, 0)).getBlock() != Blocks.air?new BlockData(pos.add(-1, 0, 0), EnumFacing.EAST):(mc.theWorld.getBlockState(pos.add(1, 0, 0)).getBlock() != Blocks.air?new BlockData(pos.add(1, 0, 0), EnumFacing.WEST):(mc.theWorld.getBlockState(pos.add(0, 0, -1)).getBlock() != Blocks.air?new BlockData(pos.add(0, 0, -1), EnumFacing.SOUTH):(mc.theWorld.getBlockState(pos.add(0, 0, 1)).getBlock() != Blocks.air?new BlockData(pos.add(0, 0, 1), EnumFacing.NORTH):null))));
   }
}
