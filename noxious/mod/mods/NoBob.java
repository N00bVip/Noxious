package noxious.mod.mods;

import noxious.event.Event;
import noxious.event.events.EventOnUpdate;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class NoBob extends Mod {

   public NoBob() {
      super("NoBob");
      setCategory(Category.PLAYER);
   }

   public void onEvent(Event event) {
      if(event instanceof EventOnUpdate) {
         mc.thePlayer.distanceWalkedModified = 0.0F;
      }

   }
}
