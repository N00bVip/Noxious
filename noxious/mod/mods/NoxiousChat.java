package noxious.mod.mods;

import net.minecraft.client.gui.GuiNewChat;
import noxious.event.Event;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.mod.mods.addons.NoxiousNewChat;

public class NoxiousChat extends Mod {

   public NoxiousChat() {
      super("NoxiousChat");
      this.setEnabled(true);
      this.setCategory(Category.EXPLOITS);
   }

   public void onDisabled() {
      mc.ingameGUI.persistantChatGUI = new GuiNewChat(mc);
   }

   public void onEnabled() {
      mc.ingameGUI.persistantChatGUI = new NoxiousNewChat(mc);
   }

   public void onEvent(Event event) {}
}
