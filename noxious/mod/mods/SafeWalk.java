package noxious.mod.mods;

import noxious.event.Event;
import noxious.event.events.EventWalking;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;

public class SafeWalk extends Mod {

   public SafeWalk() {
      super("SafeWalk", -8388608, ModManager.Category.PLAYER);
      this.setTag("Safe Walk");
   }

   public void onEvent(Event event) {
      if(event instanceof EventWalking) {
         ((EventWalking)event).setSafeWalk(true);
      }

   }
}
