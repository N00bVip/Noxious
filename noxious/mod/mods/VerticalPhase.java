package noxious.mod.mods;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import noxious.event.Event;
import noxious.event.events.EventBlockBoundingBox;
import noxious.event.events.EventInsideBlock;
import noxious.event.events.EventPacketReceive;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventPushOutOfBlocks;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;

public class VerticalPhase extends Mod {

   public VerticalPhase() {
      super("VerticalPhase", -9868951, ModManager.Category.EXPLOITS);
      this.setEnabled(false);
   }

   private boolean isInsideBlock() {
      for(int x = MathHelper.floor_double(mc.thePlayer.boundingBox.minX); x < MathHelper.floor_double(mc.thePlayer.boundingBox.maxX) + 1; ++x) {
         for(int y = MathHelper.floor_double(mc.thePlayer.boundingBox.minY); y < MathHelper.floor_double(mc.thePlayer.boundingBox.maxY) + 1; ++y) {
            for(int z = MathHelper.floor_double(mc.thePlayer.boundingBox.minZ); z < MathHelper.floor_double(mc.thePlayer.boundingBox.maxZ) + 1; ++z) {
               Block block = Minecraft.getMinecraft().theWorld.getBlockState(new BlockPos(x, y, z)).getBlock();
               if(block != null && !(block instanceof BlockAir)) {
                  AxisAlignedBB boundingBox = block.getCollisionBoundingBox(mc.theWorld, new BlockPos(x, y, z), mc.theWorld.getBlockState(new BlockPos(x, y, z)));
                  if(boundingBox != null && mc.thePlayer.boundingBox.intersectsWith(boundingBox)) {
                     return true;
                  }
               }
            }
         }
      }

      return false;
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
         if(this.isInsideBlock()) {
            this.setColor(-2448096);

            for(int sent = 0; sent < 4; ++sent) {
               Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY + 0.2D, mc.thePlayer.posZ, false));
            }
         } else {
            this.setColor(-9868951);
         }
      }

      if(event instanceof EventPostSendMotionUpdates) {
         if(!this.isInsideBlock()) {
            mc.getNetHandler().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY - 0.02D, mc.thePlayer.posZ, true));
         }
      } else if(event instanceof EventBlockBoundingBox) {
    	  EventBlockBoundingBox var4 = (EventBlockBoundingBox)event;
         if((double)var4.getY() > mc.thePlayer.boundingBox.minY - 0.3D && (double)var4.getY() < mc.thePlayer.boundingBox.maxY && !this.isInsideBlock() && mc.thePlayer.isCollidedHorizontally) {
            var4.setBoundingBox((AxisAlignedBB)null);
         }
      } else if(event instanceof EventPacketReceive) {
    	  EventPacketReceive var5 = (EventPacketReceive)event;
         if(var5.getPacket() instanceof S12PacketEntityVelocity && ((S12PacketEntityVelocity)var5.getPacket()).func_149412_c() == mc.thePlayer.getEntityId() && this.isInsideBlock()) {
            var5.setCancelled(true);
         }
      } else if(event instanceof EventPacketSent) {
    	  EventPacketSent var6 = (EventPacketSent)event;
         if(var6.getPacket() instanceof C03PacketPlayer) {
            C03PacketPlayer var3 = (C03PacketPlayer)var6.getPacket();
         }
      } else if(event instanceof EventPushOutOfBlocks) {
         ((EventPushOutOfBlocks)event).setCancelled(true);
      } else if(event instanceof EventInsideBlock) {
         ((EventInsideBlock)event).setCancelled(true);
      }

   }
}
