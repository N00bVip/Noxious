package noxious.mod.mods;

import net.minecraft.client.multiplayer.WorldClient;
import noxious.event.Event;
import noxious.event.events.EventWorldLoad;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class FullBright extends Mod
{
    public FullBright()
    {
        super("FullBright");
        this.setEnabled(true);
        this.setCategory(Category.RENDER);
    }

    public void editTable(WorldClient world, float value)
    {
        if (world != null)
        {
            float[] light = world.provider.lightBrightnessTable;

            for (int index = 0; index < light.length; ++index)
            {
                if (light[index] <= value)
                {
                    light[index] = value;
                }
            }
        }
    }

    public void onDisabled()
    {
        super.onDisabled();

        if (mc.theWorld != null)
        {
            mc.gameSettings.gammaSetting = 0.5F;
        }
    }

    public void onEnabled()
    {
        super.onEnabled();

        if (mc.theWorld != null)
        {
            mc.gameSettings.gammaSetting = 99.0F;
        }
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventWorldLoad)
        {
            mc.gameSettings.gammaSetting = 99.0F;
        }
    }
}
