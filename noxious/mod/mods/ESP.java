package noxious.mod.mods;

import java.util.Iterator;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityEnderPearl;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.RenderHelper;
import noxious.value.Value;

public class ESP extends Mod
{
    private final Value<Boolean> animals = new Value("esp_animals", Boolean.valueOf(false));
    private final Value<Boolean> enderpearls = new Value("esp_enderpearls", Boolean.valueOf(true));
    private final Value<Boolean> invisibles = new Value("esp_invisibles", Boolean.valueOf(true));
    private final Value<Boolean> items = new Value("esp_items", Boolean.valueOf(false));
    private final Value<Boolean> mobs = new Value("esp_mobs", Boolean.valueOf(false));
    private final Value<Boolean> players = new Value("esp_players", Boolean.valueOf(true));

    public ESP()
    {
        super("ESP", -12799119, Category.RENDER);
        Noxious.getCommandManager().getContents().add(new Command("esp", "<animals/enderpearls/invisibles/items/mobs/players>", new String[0])
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("animals"))
                {
                    ESP.this.animals.setValue(Boolean.valueOf(!((Boolean)ESP.this.animals.getValue()).booleanValue()));
                    Logger.logChat("ESP will " + (((Boolean)ESP.this.animals.getValue()).booleanValue() ? "now" : "no longer") + " render a boxes for animals.");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("enderpearls"))
                {
                    ESP.this.enderpearls.setValue(Boolean.valueOf(!((Boolean)ESP.this.enderpearls.getValue()).booleanValue()));
                    Logger.logChat("ESP will " + (((Boolean)ESP.this.enderpearls.getValue()).booleanValue() ? "now" : "no longer") + " render boxes for enderpearls.");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("invisibles"))
                {
                    ESP.this.invisibles.setValue(Boolean.valueOf(!((Boolean)ESP.this.invisibles.getValue()).booleanValue()));
                    Logger.logChat("ESP will " + (((Boolean)ESP.this.invisibles.getValue()).booleanValue() ? "now" : "no longer") + " render boxes for invisible entities.");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("items"))
                {
                    ESP.this.items.setValue(Boolean.valueOf(!((Boolean)ESP.this.items.getValue()).booleanValue()));
                    Logger.logChat("ESP will " + (((Boolean)ESP.this.items.getValue()).booleanValue() ? "now" : "no longer") + " render boxes for items.");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("mobs"))
                {
                    ESP.this.mobs.setValue(Boolean.valueOf(!((Boolean)ESP.this.mobs.getValue()).booleanValue()));
                    Logger.logChat("ESP will " + (((Boolean)ESP.this.mobs.getValue()).booleanValue() ? "now" : "no longer") + " render boxes for mobs.");
                }
                else if (message.split(" ")[1].equalsIgnoreCase("players"))
                {
                    ESP.this.players.setValue(Boolean.valueOf(!((Boolean)ESP.this.players.getValue()).booleanValue()));
                    Logger.logChat("ESP will " + (((Boolean)ESP.this.players.getValue()).booleanValue() ? "now" : "no longer") + " render boxes for players.");
                }
                else
                {
                    Logger.logChat("Option not valid! Available options: animals, enderpearls, invisibles, items, mobs, players.");
                }
            }
        });
    }

    private boolean isValidTarget(Entity entity)
    {
        boolean valid = false;
        String ignore = (String)((KillAura)Noxious.getModManager().getModByName("killaura")).getIgnore().getValue();

        if (!ignore.equals("") && entity.getDisplayName().getFormattedText().startsWith("\u00a7" + ignore) && entity instanceof EntityPlayer && !Noxious.getFriendManager().isFriend(entity.getName()))
        {
            return false;
        }
        else if (entity.isInvisible() && !((Boolean)this.invisibles.getValue()).booleanValue())
        {
            return false;
        }
        else
        {
            if (entity instanceof EntityPlayer && ((Boolean)this.players.getValue()).booleanValue())
            {
                valid = entity != null && entity != mc.thePlayer && entity.isEntityAlive() && entity.ticksExisted > 20;
            }
            else if (entity instanceof IMob && ((Boolean)this.mobs.getValue()).booleanValue())
            {
                valid = entity != null && entity.isEntityAlive() && mc.thePlayer.getDistanceToEntity(entity) <= 30.0F && entity.ticksExisted > 20;
            }
            else if (entity instanceof IAnimals && !(entity instanceof IMob) && ((Boolean)this.animals.getValue()).booleanValue())
            {
                valid = entity != null && entity.isEntityAlive() && mc.thePlayer.getDistanceToEntity(entity) <= 30.0F && entity.ticksExisted > 20;
            }
            else if (entity instanceof EntityEnderPearl && ((Boolean)this.enderpearls.getValue()).booleanValue())
            {
                valid = entity != null && entity.isEntityAlive();
            }
            else if (entity instanceof EntityItem && ((Boolean)this.items.getValue()).booleanValue())
            {
                valid = entity != null && entity.isEntityAlive() && mc.thePlayer.getDistanceToEntity(entity) <= 30.0F;
            }

            return valid;
        }
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventRender3D)
        {
            if (!Minecraft.isGuiEnabled())
            {
                return;
            }

            EventRender3D render = (EventRender3D)event;
            GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL11.GL_LINE_SMOOTH);
            GL11.glDepthMask(false);
            Iterator var4 = mc.theWorld.loadedEntityList.iterator();

            while (var4.hasNext())
            {
                Entity entity = (Entity)var4.next();

                if (this.isValidTarget(entity))
                {
                    if (entity.getDistanceToEntity(mc.thePlayer) <= 64.0F)
                    {
                        GL11.glLineWidth(1.5F);
                    }

                    double posX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * (double)render.getPartialTicks() - RenderManager.renderPosX;
                    double posY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * (double)render.getPartialTicks() - RenderManager.renderPosY;
                    double posZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * (double)render.getPartialTicks() - RenderManager.renderPosZ;
                    GL11.glPushMatrix();
                    GL11.glTranslated(posX, posY, posZ);
                    GL11.glRotatef(-entity.rotationYaw, 0.0F, entity.height, 0.0F);
                    GL11.glTranslated(-posX, -posY, -posZ);
                    this.renderBox(entity, posX, posY, posZ);
                    GL11.glPopMatrix();
                }
            }

            GL11.glDepthMask(true);
            GL11.glDisable(GL11.GL_LINE_SMOOTH);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glPopMatrix();
        }
    }

    private void renderBox(Entity entity, double x, double y, double z)
    {
        AxisAlignedBB box = AxisAlignedBB.fromBounds(x - (double)entity.width, y, z - (double)entity.width, x + (double)entity.width, y + (double)entity.height + 0.2D, z + (double)entity.width);

        if (entity instanceof EntityLivingBase)
        {
            box = AxisAlignedBB.fromBounds(x - (double)entity.width + 0.2D, y, z - (double)entity.width + 0.2D, x + (double)entity.width - 0.2D, y + (double)entity.height + (entity.isSneaking() ? 0.02D : 0.2D), z + (double)entity.width - 0.2D);
        }

        float distance = mc.thePlayer.getDistanceToEntity(entity);
        float[] color;

        if (entity instanceof EntityPlayer && Noxious.getFriendManager().isFriend(entity.getName()))
        {
            color = new float[] {0.9F, 2.55F, 0.5F};
        }
        else if (entity.isInvisibleToPlayer(mc.thePlayer))
        {
            color = new float[] {1.0F, 0.9F, 0.0F};
        }
        else
        {
            color = new float[] {0.0F, 0.9F, 0.0F};
        }

        if (entity instanceof EntityLivingBase && ((EntityLivingBase)entity).hurtTime > 0)
        {
            color = new float[] {1.0F, 0.66F, 0.0F};
        }

        GL11.glColor4f(color[0], color[1], color[2], 0.6F);
        RenderHelper.drawLines(box);
        GL11.glColor4f(color[0], color[1], color[2], 0.8F);
        RenderHelper.drawOutlinedBoundingBox(box);
    }
}
