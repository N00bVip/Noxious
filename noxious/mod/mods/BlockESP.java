package noxious.mod.mods;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventBlockRender;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.RenderHelper;
import noxious.value.Value;

public class BlockESP extends Mod
{
    public final List<Block> blocks = new ArrayList();
    private final List<Integer[]> cache = new CopyOnWriteArrayList();
    private final Value<Integer> limit = new Value("blockesp_limit", Integer.valueOf(5000));
    private final Value<Integer> range = new Value("blockesp_range", Integer.valueOf(256));
    private boolean zrue = true;

    public BlockESP()
    {
        super("BlockESP", 0xFFFF00, Category.RENDER);
        this.setTag("Block ESP");
        Noxious.getCommandManager().getContents().add(new Command("blockesplimit", "<block limit>", new String[] {"besplimit", "bel"})
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("-d"))
                {
                    BlockESP.this.limit.setValue((Integer)BlockESP.this.limit.getDefaultValue());
                }
                else
                {
                    BlockESP.this.limit.setValue(Integer.valueOf(Integer.parseInt(message.split(" ")[1])));
                }

                if (((Integer)BlockESP.this.limit.getValue()).intValue() > 100000)
                {
                    BlockESP.this.limit.setValue(Integer.valueOf(100000));
                }
                else if (((Integer)BlockESP.this.limit.getValue()).intValue() < 10)
                {
                    BlockESP.this.limit.setValue(Integer.valueOf(10));
                }

                Logger.logChat("BlockESP Limit set to: " + BlockESP.this.limit.getValue());
            }
        });
        Noxious.getCommandManager().getContents().add(new Command("blockesprange", "<block range>", new String[] {"besprange", "ber"})
        {
            public void run(String message)
            {
                if (message.split(" ")[1].equalsIgnoreCase("-d"))
                {
                    BlockESP.this.range.setValue((Integer)BlockESP.this.range.getDefaultValue());
                }
                else
                {
                    BlockESP.this.range.setValue(Integer.valueOf(Integer.parseInt(message.split(" ")[1])));
                }

                if (((Integer)BlockESP.this.range.getValue()).intValue() > 512)
                {
                    BlockESP.this.range.setValue(Integer.valueOf(512));
                }
                else if (((Integer)BlockESP.this.range.getValue()).intValue() < 10)
                {
                    BlockESP.this.range.setValue(Integer.valueOf(10));
                }

                Logger.logChat("BlockESP Range set to: " + BlockESP.this.range.getValue());
            }
        });
        Noxious.getCommandManager().getContents().add(new Command("blockesp", "<block name/id>", new String[] {"besp", "be"})
        {
            public boolean isInteger(String string)
            {
                try
                {
                    Integer.parseInt(string);
                    return true;
                }
                catch (Exception var3)
                {
                    return false;
                }
            }
            public void run(String message)
            {
                String[] arguments = message.split(" ");

                if (arguments.length == 1)
                {
                    BlockESP.this.blocks.clear();
                    BlockESP.this.cache.clear();
                    Logger.logChat("Block list cleared.");
                }
                else
                {
                    String input = message.substring((arguments[0] + " ").length());
                    Block block = null;

                    if (this.isInteger(input))
                    {
                        block = Block.getBlockById(Integer.parseInt(input));
                    }
                    else
                    {
                        Iterator var6 = Block.blockRegistry.iterator();

                        while (var6.hasNext())
                        {
                            Object o = var6.next();
                            Block blockk = (Block)o;
                            String name = blockk.getLocalizedName().replaceAll("tile.", "").replaceAll(".name", "");

                            if (name.toLowerCase().startsWith(input.toLowerCase()))
                            {
                                block = blockk;
                                break;
                            }
                        }
                    }

                    if (block == null)
                    {
                        Logger.logChat("Invalid block.");
                    }
                    else
                    {
                        if (BlockESP.this.blocks.contains(block))
                        {
                            Logger.logChat("No longer searching for [" + block.getLocalizedName() + "]");
                            BlockESP.this.blocks.remove(BlockESP.this.blocks.indexOf(block));
                            BlockESP.this.cache.clear();
                        }
                        else
                        {
                            Logger.logChat("Searching for [" + block.getLocalizedName() + "]");
                            BlockESP.this.blocks.add(block);
                            BlockESP.this.cache.clear();
                        }

                        mc.renderGlobal.loadRenderers();
                    }
                }
            }
        });
    }

    private int getBlockColor(Block block)
    {
        int color = block.getMaterial().getMaterialMapColor().colorValue;

        switch (Block.getIdFromBlock(block))
        {
            case 14:
            case 41:
                color = 16576075;
                break;

            case 15:
                color = this.zrue ? -576083543 : -574570304;
                break;

            case 16:
            case 173:
                color = 3618615;
                break;

            case 21:
            case 22:
                color = 1525445;
                break;

            case 49:
                color = 3944534;
                break;

            case 54:
                color = 16760576;
                break;

            case 56:
            case 57:
            case 116:
                color = 6155509;
                break;

            case 61:
            case 62:
                color = 16658167;
                break;

            case 73:
            case 74:
            case 152:
                color = 16711680;
                break;

            case 129:
            case 133:
                color = 1564002;
                break;

            case 130:
                color = 14614999;
                break;

            case 146:
                color = 13474867;
        }

        return color == 0 ? 1216104 : color;
    }

    private boolean isCached(double x, double y, double z)
    {
        Iterator var8 = this.cache.iterator();
        Integer[] block;

        do
        {
            if (!var8.hasNext())
            {
                return false;
            }

            block = (Integer[])var8.next();
        }
        while ((double)block[0].intValue() != x || (double)block[1].intValue() != y || (double)block[2].intValue() != z);

        return true;
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventRender3D)
        {
            GL11.glPushMatrix();
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL11.GL_LINE_SMOOTH);
            GL11.glDepthMask(false);
            GL11.glLineWidth(1.0F);
            Iterator x = this.cache.iterator();

            while (x.hasNext())
            {
                Integer[] blockRender = (Integer[])x.next();
                Block y = mc.theWorld.getBlockState(new BlockPos(blockRender[0].intValue(), blockRender[1].intValue(), blockRender[2].intValue())).getBlock();

                if (mc.thePlayer.getDistance((double)blockRender[0].intValue(), (double)blockRender[1].intValue(), (double)blockRender[2].intValue()) <= (double)((Integer)this.range.getValue()).intValue() && this.blocks.contains(y))
                {
                    this.renderESP(blockRender, y);
                }
                else
                {
                    this.cache.remove(blockRender);
                }
            }

            GL11.glDepthMask(true);
            GL11.glDisable(GL11.GL_LINE_SMOOTH);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glPopMatrix();
        }
        else if (event instanceof EventBlockRender)
        {
            if (this.blocks.isEmpty())
            {
                return;
            }

            EventBlockRender blockRender1 = (EventBlockRender)event;
            int x1 = blockRender1.getX();
            int y1 = blockRender1.getY();
            int z = blockRender1.getZ();
            Block block = mc.theWorld.getBlockState(new BlockPos(x1, y1, z)).getBlock();

            if (!this.isCached((double)x1, (double)y1, (double)z) && this.blocks.contains(block) && this.cache.size() < ((Integer)this.limit.getValue()).intValue())
            {
                this.cache.add(new Integer[] {Integer.valueOf(x1), Integer.valueOf(y1), Integer.valueOf(z)});
            }
        }
    }

    private void renderESP(Integer[] cache, Block block)
    {
        if (this.blocks.contains(block))
        {
            double x = (double)cache[0].intValue() - RenderManager.renderPosX;
            double y = (double)cache[1].intValue() - RenderManager.renderPosY;
            double z = (double)cache[2].intValue() - RenderManager.renderPosZ;
            int color = this.getBlockColor(block);
            float red = (float)(color >> 16 & 255) / 255.0F;
            float green = (float)(color >> 8 & 255) / 255.0F;
            float blue = (float)(color & 255) / 255.0F;
            GL11.glColor4f(red, green, blue, 0.2F);
            RenderHelper.drawFilledBox(new AxisAlignedBB(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
            GL11.glColor4f(red, green, blue, 1.0F);
            RenderHelper.drawOutlinedBoundingBox(new AxisAlignedBB(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
            GL11.glColor4f(red, green, blue, 0.5F);
            RenderHelper.drawLines(new AxisAlignedBB(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
        }
    }
}
