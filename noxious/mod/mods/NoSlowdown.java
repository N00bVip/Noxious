package noxious.mod.mods;

import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import noxious.event.Event;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;

public class NoSlowdown extends Mod {

   public NoSlowdown() {
      super("NoSlowdown", -12302521, ModManager.Category.MOVEMENT);
      this.setTag("No Slowdown");
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
         if(mc.thePlayer.isUsingItem() &&  !mc.gameSettings.keyBindSneak.pressed && (mc.gameSettings.keyBindForward.pressed || mc.gameSettings.keyBindLeft.pressed || mc.gameSettings.keyBindRight.pressed || mc.gameSettings.keyBindBack.pressed)) {
        	 this.setColor(-7536856);
        	 if (mc.thePlayer.isBlocking()) {
        		 mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, new BlockPos(0, 0, 0), EnumFacing.UP));
        	 }
         } else {
        	 this.setColor(-8355712);
         }
      } else if(event instanceof EventPostSendMotionUpdates && mc.thePlayer.isBlocking()) {
         mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(mc.thePlayer.inventory.getCurrentItem()));
      }

   }
}
