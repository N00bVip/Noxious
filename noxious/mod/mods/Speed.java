package noxious.mod.mods;

import java.util.Iterator;
import java.util.Objects;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Timer;
import net.minecraft.util.Vec3;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventOnUpdate;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class Speed extends Mod {

   private int ticks = 0;
   private Value<Double> multiplier = new Value("speed_multiplier", Double.valueOf(3.15D));
   private Value<Double> timer = new Value("speed_timer", Double.valueOf(1.355D));
   private final Value<Boolean> newspeed = new Value("speed_new", Boolean.valueOf(false));
   private final Value<Boolean> oldspeed = new Value("speed_old", Boolean.valueOf(false));
   private final Value<Boolean> otherspeed = new Value("speed_other", Boolean.valueOf(true));
   private int state = 0;
   private float ground = 0.0F;
   public boolean canStep = false;
   public boolean attacking = false;
   private TimeHelper time = new TimeHelper();
   public boolean bf;
   public boolean ae;


   public Speed() {
      super("Speed", 0x00BFFF, Category.MOVEMENT);
      Noxious.getCommandManager().getContents().add(new Command("speedmode", "<new/old/other>", new String[0]) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("new")) {
                 Speed.this.oldspeed.setValue(Boolean.valueOf(false));
                 Speed.this.otherspeed.setValue(Boolean.valueOf(false));
                 Speed.this.newspeed.setValue(Boolean.valueOf(true));
               Logger.logChat("Speed Mode set to New!");
            } else if(message.split(" ")[1].equalsIgnoreCase("old")) {
                 Speed.this.oldspeed.setValue(Boolean.valueOf(true));
                 Speed.this.otherspeed.setValue(Boolean.valueOf(false));
                 Speed.this.newspeed.setValue(Boolean.valueOf(false));
               Logger.logChat("Speed Mode set to Old!");
            }else if(message.split(" ")[1].equalsIgnoreCase("other"))
            {
                Speed.this.otherspeed.setValue(Boolean.valueOf(true));
                Speed.this.oldspeed.setValue(Boolean.valueOf(false));
                Speed.this.newspeed.setValue(Boolean.valueOf(false));


                Logger.logChat("Speed Mode set to other!");
            } else {
               Logger.logChat("Option not valid! Available options: new, old, other.");
            }

         }
      });
   }




/**   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
         if(this.shouldSpeedUp()) {
            this.setColor(-7536856);
         } else {
            this.setColor(-6158647);
         }

         if(((Boolean)this.oldspeed.getValue()).booleanValue()) {
             double speed = 3.15D;
             double slow = 1.49D;
             double offset = 4.9D;
             if(mc.gameSettings.keyBindForward.getIsKeyPressed() || mc.gameSettings.keyBindBack.getIsKeyPressed() || mc.gameSettings.keyBindLeft.getIsKeyPressed() || mc.gameSettings.keyBindRight.getIsKeyPressed()) {
                this.canStep = false;
                if(noxious.getModManager().getModByName("step").isEnabled()) {
                   Iterator ncT = mc.theWorld.getCollidingBoundingBoxes(mc.thePlayer, mc.thePlayer.getEntityBoundingBox().expand(0.5D, 0.0D, 0.5D)).iterator();

                   while(ncT.hasNext()) {
                      Object timer = ncT.next();
                      if(timer instanceof AxisAlignedBB) {
                         AxisAlignedBB bb = (AxisAlignedBB)timer;
                         if(!Objects.isNull(bb)) {
                            this.canStep = true;
                         }
                      }
                   }
                }

                if(mc.thePlayer.onGround && this.ground < 1.0F) {
                   this.ground += 0.2F;
                }

                if(!mc.thePlayer.onGround) {
                   this.ground = 0.0F;
                }

                if(this.ground == 1.0F && this.shouldSpeedUp()) {
                   Timer var14 = mc.timer;

                   if(mc.thePlayer.moveStrafing != 0.0F) {
                      speed -= 0.1D;
                   }

                   if(mc.thePlayer.isInWater()) {
                      speed -= 0.1D;
                   }

                   double var15 = 1.15D;
                   ++this.state;
                   if(this.state == 1) {
                      mc.thePlayer.motionX *= speed;
                      mc.thePlayer.motionZ *= speed;
                   } else if(this.state == 2) {
                      mc.timer.timerSpeed = 1.0F;
                      mc.thePlayer.motionX /= slow;
                      mc.thePlayer.motionZ /= slow;
                   } else if(this.state != 3 && this.state == 4) {
                      mc.timer.timerSpeed = 1.0F;
                      this.state = 0;
                   }

                   mc.timer = var14;
                }
             } else {
                 this.state = 0;
             }
         } else {
             if(((Boolean)this.newspeed.getValue()).booleanValue()) {
                 double speed = 3.15D;
                 double slow = 1.49D;
                 double offset = 4.9D;
                 if(mc.gameSettings.keyBindForward.getIsKeyPressed() || mc.gameSettings.keyBindBack.getIsKeyPressed() || mc.gameSettings.keyBindLeft.getIsKeyPressed() || mc.gameSettings.keyBindRight.getIsKeyPressed()) {
                     if(mc.gameSettings.keyBindForward.getIsKeyPressed()) {
                        speed = 1.75D;
                     }
                    this.canStep = false;
                    if(noxious.getModManager().getModByName("step").isEnabled()) {
                       Iterator ncT = mc.theWorld.getCollidingBoundingBoxes(mc.thePlayer, mc.thePlayer.getEntityBoundingBox().expand(0.5D, 0.0D, 0.5D)).iterator();

                       while(ncT.hasNext()) {
                          Object timer = ncT.next();
                          if(timer instanceof AxisAlignedBB) {
                             AxisAlignedBB bb = (AxisAlignedBB)timer;
                             if(!Objects.isNull(bb)) {
                                this.canStep = true;
                             }
                          }
                       }
                    }

                    if(mc.thePlayer.onGround && this.ground < 1.0F) {
                       this.ground += 0.2F;
                    }

                    if(!mc.thePlayer.onGround) {
                       this.ground = 0.0F;
                    }

                    if(this.ground == 1.0F && this.shouldSpeedUp()) {
                       Timer var14 = mc.timer;

                       if(mc.thePlayer.moveStrafing != 0.0F) {
                          speed -= 0.1D;
                       }

                       if(mc.thePlayer.isInWater()) {
                          speed -= 0.1D;
                       }

                       double var15 = 1.4D;
                       ++this.state;
                       if(this.state == 1) {
                          mc.timer.timerSpeed = 1.0F;
                          mc.thePlayer.motionX *= speed;
                          mc.thePlayer.motionZ *= speed;
                           if (mc.gameSettings.keyBindForward.getIsKeyPressed() && !allowStep()) {
                              mc.thePlayer.jump();
                              mc.thePlayer.motionY =- 0.42F;
                           }
                       } else if(this.state == 2) {
                          mc.timer.timerSpeed = 1.0F;
                          mc.thePlayer.motionX /= slow;
                           mc.thePlayer.motionZ /= slow;
                       } else if (this.state == 4) {
                          mc.timer.timerSpeed = 1.0F;
                          this.state = 0;
                       }

                       mc.timer = var14;
                    }
                 }
             }
         }
      } else if(event instanceof EventAttack) {
          this.attacking = true;
      } else if(event instanceof EventPostAttack) {
          this.attacking = false;
      }
   }*/

   public void onDisabled() {
      super.onDisabled();
      if(mc.thePlayer != null) {
         mc.timer.timerSpeed = 1.0F;
      }

   }

   boolean shouldSpeedUp() {
      boolean moving = mc.thePlayer.movementInput.moveForward != 0.0F || mc.thePlayer.movementInput.moveStrafe != 0.0F;
      return !mc.thePlayer.isInWater() && !BlockHelper.isInLiquid() && !BlockHelper.isOnLiquid() && !mc.thePlayer.isCollidedHorizontally && !BlockHelper.isOnIce() && !BlockHelper.isOnLadder() && !mc.thePlayer.isSneaking() && mc.thePlayer.onGround && moving;
   }

   public boolean allowStep() {

       final Step step = (Step)Noxious.getModManager().getModByName("step");
       final boolean b = Speed.mc.thePlayer.movementInput.moveForward != 0.0f || Speed.mc.thePlayer.movementInput.moveStrafe != 0.0f;
       return !Speed.mc.thePlayer.isInWater() && !BlockHelper.isInLiquid() && !BlockHelper.isOnLiquid() && !Speed.mc.thePlayer.isCollidedHorizontally && !BlockHelper.isOnIce() && !BlockHelper.isOnLadder() && !Speed.mc.thePlayer.isSneaking() && Speed.mc.thePlayer.onGround && b;
   }

   private boolean isInBlock(AxisAlignedBB aabb) {
      MovingObjectPosition mop = mc.theWorld.rayTraceBlocks(new Vec3(aabb.minX, aabb.minY, aabb.minZ), new Vec3(aabb.maxX, aabb.maxY, aabb.maxZ));
      return mop != null && mop.typeOfHit != MovingObjectPosition.MovingObjectType.MISS;
   }

   public void onEvent(Event event)
   {
       if (event instanceof EventPreSendMotionUpdates) {
           if ((boolean)this.newspeed.getValue() && this.ae && !Speed.mc.thePlayer.isCollidedHorizontally && !BlockHelper.isOnLiquid() && !Speed.mc.thePlayer.onGround && !Speed.mc.gameSettings.keyBindJump.getIsKeyPressed() && !Speed.mc.thePlayer.isOnLadder()) {
               Speed.mc.thePlayer.motionY = Math.min(-0.2, Speed.mc.thePlayer.motionY);
               this.ae = false;
           }
           if (this.otherspeed.getValue()) {
               if (!this.allowStep()) {
            	   if (!mc.thePlayer.onGround) {
            		   Speed.mc.timer.timerSpeed = 1.0f;
            		   return;
            	   }
                   Speed.mc.timer.timerSpeed = 1.0f;
                   final EntityPlayerSP thePlayer = Speed.mc.thePlayer;
                   thePlayer.motionX *= 0.98;
                   final EntityPlayerSP thePlayer2 = Speed.mc.thePlayer;
                   thePlayer2.motionZ *= 0.98;
                   this.state = 0;
                   return;
               }
               switch (this.state) {
                   case 1: {
                       Speed.mc.timer.timerSpeed = 1.0f;
                       final EntityPlayerSP thePlayer3 = Speed.mc.thePlayer;
                       thePlayer3.motionX /= 3.6;
                       final EntityPlayerSP thePlayer4 = Speed.mc.thePlayer;
                       thePlayer4.motionZ /= 3.6;
                       break;
                   }
                   case 2: {
                	   if (!mc.thePlayer.onGround) {
                		   Speed.mc.timer.timerSpeed = 1.0f;
                		   return;
                	   }
                       double n = 5.45;
                       for (Iterator iterator = mc.thePlayer.getActivePotionEffects().iterator(); iterator.hasNext();){
                           PotionEffect potionEffect = (PotionEffect) iterator.next();
                           if (Potion.potionTypes[potionEffect.getPotionID()].getId() == Potion.moveSpeed.getId()) {
                               if (potionEffect.getAmplifier() == 0) {
                                   n = 4.7;
                               }
                               else if (potionEffect.getAmplifier() == 1) {
                                   n = 3.7;
                               }
                               else if (potionEffect.getAmplifier() == 2) {
                                   n = 2.7;
                               }
                               else {
                                   if (potionEffect.getAmplifier() < 3) {
                                       continue;
                                   }
                                   n = 1.7;
                               }
                           }
                       }
                       if (Speed.mc.thePlayer.movementInput.moveStrafe != 0.0f) {
                           n -= 0.1;
                       }
                       Speed.mc.timer.timerSpeed = ((Speed.mc.thePlayer.getHealth() == Speed.mc.thePlayer.getMaxHealth()) ? 1.3f : 1.0f);
                       mc.timer.timerSpeed = 1.5f;
                       final EntityPlayerSP thePlayer5 = Speed.mc.thePlayer;
                       thePlayer5.motionX *= n;
                       final EntityPlayerSP thePlayer6 = Speed.mc.thePlayer;
                       thePlayer6.motionZ *= n;
                       break;
                   }
                   case 3: {
                       Speed.mc.timer.timerSpeed = 1.0f;
                       final EntityPlayerSP thePlayer7 = Speed.mc.thePlayer;
                       thePlayer7.motionX /= 1.3;
                       final EntityPlayerSP thePlayer8 = Speed.mc.thePlayer;
                       thePlayer8.motionZ /= 1.3;
                       break;
                   }
                   case 4: {
                	   this.state = 0;
                       break;
                   }

                   default: {
                	   if (!mc.thePlayer.onGround) {
                		   Speed.mc.timer.timerSpeed = 1.0f;
                		   return;
                	   }
                       Speed.mc.timer.timerSpeed = 1.0f;
                       final EntityPlayerSP thePlayer9 = Speed.mc.thePlayer;
                       thePlayer9.motionX *= 0.98;
                       final EntityPlayerSP thePlayer10 = Speed.mc.thePlayer;
                       thePlayer10.motionZ *= 0.98;
                       break;
                   }
               }
               ++this.state;
           }else{
           if ((Boolean)this.oldspeed.getValue().booleanValue())
           {
             double speed = 3.15D;
             double slow = 1.49D;
             double offset = 4.9D;
             if(mc.gameSettings.keyBindForward.getIsKeyPressed() || mc.gameSettings.keyBindBack.getIsKeyPressed() || mc.gameSettings.keyBindLeft.getIsKeyPressed() || mc.gameSettings.keyBindRight.getIsKeyPressed())
             {
                 if(mc.gameSettings.keyBindForward.getIsKeyPressed())
                 {
                     speed = 1.75D;
                     this.canStep = false;
                     if(Noxious.getModManager().getModByName("step").isEnabled())
                     {
                         Iterator ncT = mc.theWorld.getCollidingBoundingBoxes(mc.thePlayer, mc.thePlayer.getEntityBoundingBox().expand(0.5D, 0D, 0.5D)).iterator();
                         while(ncT.hasNext())
                         {
                             Object timer = ncT.next();
                             if(timer instanceof AxisAlignedBB) {
                                AxisAlignedBB bb = (AxisAlignedBB)timer;
                                if(!Objects.isNull(bb)) {
                                   this.canStep = true;
                                }
                             }
                         }

                     }

                     if(mc.thePlayer.onGround && this.ground < 1.0F) {
                         this.ground += 0.2F;
                      }

                      if(!mc.thePlayer.onGround) {
                         this.ground = 0.0F;
                      }

                      if(this.ground == 1.0F && this.shouldSpeedUp()) {
                         Timer var14 = mc.timer;

                         if(mc.thePlayer.moveStrafing != 0.0F) {
                            speed -= 0.1D;
                         }

                         if(mc.thePlayer.isInWater()) {
                            speed -= 0.1D;
                         }

                         double var15 = 1.4D;
                         ++this.state;
                         if(this.state == 1) {
                          mc.timer.timerSpeed = 1.0F;
                          mc.thePlayer.motionX *= speed;
                          mc.thePlayer.motionZ *= speed;
                             if (mc.gameSettings.keyBindForward.getIsKeyPressed() && !allowStep()) {
                              mc.thePlayer.jump();
                              mc.thePlayer.motionY =- 0.42F;
                             }
                         } else if(this.state == 2) {
                          mc.timer.timerSpeed = 1.0F;
                          mc.thePlayer.motionX /= slow;
                             mc.thePlayer.motionZ /= slow;
                         } else if (this.state == 4) {
                          mc.timer.timerSpeed = 1.0F;
                          this.state = 0;
                         }

                         mc.timer = var14;
                      }

                 }
             }

           }
       }
       }
       else if (event instanceof EventOnUpdate && (boolean)this.newspeed.getValue() && this.allowStep()) {
           final double n4 = (Speed.mc.thePlayer.rotationYaw + 90.0f + ((Speed.mc.thePlayer.moveForward < 0.0f) ? (180 + ((Speed.mc.thePlayer.moveStrafing < 0.0f) ? -45 : ((Speed.mc.thePlayer.moveStrafing > 0.0f) ? 45 : 0))) : ((Speed.mc.thePlayer.moveForward > 0.0f) ? (0 + ((Speed.mc.thePlayer.moveStrafing < 0.0f) ? 45 : ((Speed.mc.thePlayer.moveStrafing > 0.0f) ? -45 : 0))) : (0 + ((Speed.mc.thePlayer.moveStrafing < 0.0f) ? 90 : ((Speed.mc.thePlayer.moveStrafing > 0.0f) ? -90 : 0)))))) * 3.141592653589793 / 180.0;
           final double n5 = Math.cos(n4) * 0.27;
           final double n6 = Math.sin(n4) * 0.27;
           final EntityPlayerSP thePlayer17 = Speed.mc.thePlayer;
           thePlayer17.motionX += n5;
           Speed.mc.thePlayer.motionY = 0.145;
           final EntityPlayerSP thePlayer18 = Speed.mc.thePlayer;
           thePlayer18.motionZ += n6;
           this.ae = true;
       }
   }





























}