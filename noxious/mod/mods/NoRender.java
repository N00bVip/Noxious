package noxious.mod.mods;

import java.util.Iterator;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import noxious.event.Event;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class NoRender extends Mod {

   public NoRender() {
      super("NoRender");
      this.setCategory(Category.RENDER);
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
         Iterator var3 = mc.theWorld.loadedEntityList.iterator();

         while(var3.hasNext()) {
            Object o = var3.next();
            Entity entity = (Entity)o;
            if(entity instanceof EntityItem) {
               mc.theWorld.removeEntity(entity);
            }
         }
      }

   }
}
