package noxious.mod.mods;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.input.Keyboard;

import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventChatSent;
import noxious.event.events.EventPressedKey;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.mod.mods.addons.Macro;
import noxious.util.Logger;

public class Macros extends Mod {

   private final List<Macro> macros = new CopyOnWriteArrayList();


   public Macros() {
      super("Macros");
      this.setEnabled(true);
      this.setCategory(Category.EXPLOITS);
      Noxious.getCommandManager().getContents().add(new Command("macroadd", "<key> <command>", new String[]{"madd", "ma"}) {
         public void run(String message) {
            String[] arguments = message.split(" ");
            int key = Keyboard.getKeyIndex(arguments[1].toUpperCase());
            if(key == 0) {
               Logger.logChat("You can\'t macro the key \"NONE\".");
            } else {
               String command = message.substring((arguments[0] + " " + arguments[1] + " ").length());
               if(command.startsWith(".")) {
                  command = command.substring(1);
               }

               Iterator var6 = Macros.this.macros.iterator();

               while(var6.hasNext()) {
                  Macro macro = (Macro)var6.next();
                  if(macro.getKey() == key) {
                     Macros.this.macros.remove(macro);
                  }
               }

               Macros.this.macros.add(new Macro(command, key));
               Noxious.getFileManager().getFileByName("macros").saveFile();
               Logger.logChat("Macro \"" + Keyboard.getKeyName(key) + "\" added with command \"" + command + "\".");
            }
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("macrodel", "<key>", new String[]{"mdel", "md"}) {
         public void run(String message) {
            int key = Keyboard.getKeyIndex(message.split(" ")[1].toUpperCase());
            if(key == 0) {
               Logger.logChat("You can\'t macro the key \"NONE\".");
            } else {
               boolean found = false;
               Iterator var5 = Macros.this.macros.iterator();

               while(var5.hasNext()) {
                  Macro macro = (Macro)var5.next();
                  if(key == macro.getKey()) {
                     Macros.this.macros.remove(macro);
                     Logger.logChat("Macro \"" + Keyboard.getKeyName(key) + "\" removed.");
                     Noxious.getFileManager().getFileByName("macros").saveFile();
                     found = true;
                     break;
                  }
               }

               if(!found) {
                  Logger.logChat("Macro \"" + Keyboard.getKeyName(key) + "\" not found.");
               }

            }
         }
      });
   }

   public final List<Macro> getMacros() {
      return this.macros;
   }

   public void onEvent(Event event) {
      if(event instanceof EventPressedKey) {
    	  EventPressedKey pressed = (EventPressedKey)event;
         Iterator var4 = this.macros.iterator();

         while(var4.hasNext()) {
            Macro macro = (Macro)var4.next();
            if(pressed.getKey() == macro.getKey()) {
               EventChatSent sent = new EventChatSent("." + macro.getCommand());
               Noxious.getEventManager().call(sent);
               sent.checkForCommands();
            }
         }
      }

   }
}
