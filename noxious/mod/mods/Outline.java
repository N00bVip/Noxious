package noxious.mod.mods;

import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.culling.Frustrum;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityLockable;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Camera;
import noxious.util.Logger;
import noxious.util.Stencil;
import noxious.value.Value;
public class Outline
  extends Mod
{
  private final Value<Boolean> entities = new Value("outlinedesp_entities", Boolean.valueOf(true));
  private final Value<Boolean> chests = new Value("outlinedesp_chests", Boolean.valueOf(true));
  private final Value<Float> width = new Value("outlinedesp_linewidth", Float.valueOf(3.0F));

  public Outline()
  {
    super("Outline", -1, ModManager.Category.RENDER);
    setTag("Outline");

    Noxious.getCommandManager()
      .getContents()
      .add(new Command(
      "outline",
      "<entities/chests>",
      new String[0])
      {
        public void run(String message)
        {
          if (message.split(" ")[1].equalsIgnoreCase("entities"))
          {
            Outline.this.entities.setValue(Boolean.valueOf(!((Boolean)Outline.this.entities.getValue()).booleanValue()));
            Logger.logChat("Outlined ESP will " + (
              ((Boolean)Outline.this.entities.getValue()).booleanValue() ? "now" : "no longer") +
              " outline entities.");
          }
          else if (message.split(" ")[1].equalsIgnoreCase("chests"))
          {
            Outline.this.chests.setValue(Boolean.valueOf(!((Boolean)Outline.this.chests.getValue()).booleanValue()));
            Logger.logChat("Outlined ESP will " + (
              ((Boolean)Outline.this.chests.getValue()).booleanValue() ? "now" : "no longer") +
              " outline chests.");
          }
          else
          {
            Logger.logChat("Option not valid! Available options: entities, chests.");
          }
        }
      });Noxious.getCommandManager().getContents()
      .add(new Command("outlinedesplinewidth", "<width>", new String[] {
      "esplinewidth", "oelw" })
      {
        public void run(String message)
        {
          if (message.split(" ")[1].equalsIgnoreCase("-d")) {
            Outline.this.width.setValue((Float)Outline.this.width.getDefaultValue());
          } else {
            Outline.this.width.setValue(Float.valueOf(Float.parseFloat(message.split(" ")[1])));
          }
          if (((Float)Outline.this.width.getValue()).floatValue() > 15.0F) {
            Outline.this.width.setValue(Float.valueOf(15.0F));
          } else if (((Float)Outline.this.width.getValue()).floatValue() < 1.0F) {
            Outline.this.width.setValue(Float.valueOf(1.0F));
          }
          Logger.logChat(
            "Outliend ESP Line Width set to: " + Outline.this.width.getValue());
        }
      });
  }

  public void onEvent(Event event)
  {
    if (((event instanceof EventRender3D)) &&
      (mc.theWorld != null))
    {
      checkSetupFBO();
      if (((Boolean)this.entities.getValue()).booleanValue()) {
        drawEntityOutlines(((EventRender3D)event).getPartialTicks());
      }
      if (((Boolean)this.chests.getValue()).booleanValue()) {
        drawChestOutlines(((EventRender3D)event).getPartialTicks());
      }
      mc.getFramebuffer().bindFramebuffer(true);
      mc.getFramebuffer().bindFramebufferTexture();
    }
  }

  public static void checkSetupFBO()
  {
    Framebuffer fbo = Minecraft.getMinecraft().getFramebuffer();
    if (fbo != null) {
      if (fbo.depthBuffer > -1)
      {
        setupFBO(fbo);

        fbo.depthBuffer = -1;
      }
    }
  }

  public static void setupFBO(Framebuffer fbo)
  {
    EXTFramebufferObject.glDeleteRenderbuffersEXT(fbo.depthBuffer);

    int stencil_depth_buffer_ID = EXTFramebufferObject.glGenRenderbuffersEXT();

    EXTFramebufferObject.glBindRenderbufferEXT(36161, stencil_depth_buffer_ID);

    EXTFramebufferObject.glRenderbufferStorageEXT(36161, 34041, Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);

    EXTFramebufferObject.glFramebufferRenderbufferEXT(36160, 36128, 36161, stencil_depth_buffer_ID);

    EXTFramebufferObject.glFramebufferRenderbufferEXT(36160, 36096, 36161, stencil_depth_buffer_ID);
  }

  public void drawEntityOutlines(float partialTicks)
  {
    int entityDispList = GL11.glGenLists(1);

    Stencil.getInstance().startLayer();

    GL11.glPushMatrix();

    mc.entityRenderer.setupCameraTransform(partialTicks, 0);
    GL11.glMatrixMode(5888);
    RenderHelper.enableStandardItemLighting();
    GL11.glEnable(2884);
    Camera playerCam = new Camera(Minecraft.getMinecraft().thePlayer);
    Frustrum frustrum = new Frustrum();
    frustrum.setPosition(playerCam.getPosX(), playerCam.getPosY(), playerCam.getPosZ());

    GL11.glDisable(2929);
    GL11.glDepthMask(true);

    Stencil.getInstance().setBuffer(true);

    GL11.glNewList(entityDispList, 4864);
    label412:
    for (Object obj : Minecraft.getMinecraft().theWorld.loadedEntityList)
    {
      Entity entity = (Entity)obj;
      if (entity != Minecraft.getMinecraft().thePlayer)
      {
        GL11.glLineWidth(((Float)this.width.getValue()).floatValue());
        GL11.glEnable(3042);
        GL11.glEnable(2848);
        Camera entityCam = new Camera(entity);
        GL11.glPushMatrix();
        GL11.glDisable(3553);
        GL11.glTranslated(entityCam.getPosX() - playerCam.getPosX(), entityCam.getPosY() - playerCam.getPosY(), entityCam.getPosZ() - playerCam.getPosZ());
        Render entityRender = mc.getRenderManager().getEntityRenderObject(entity);
        if (entityRender != null)
        {
          float distance = mc.thePlayer.getDistanceToEntity(entity);
          if ((entity instanceof EntityLivingBase))
          {
            if ((entity instanceof EntityPlayer)) {
              if (Noxious.getFriendManager().isFriend(entity.getName()))
              {
                GL11.glColor4f(0.92F, 0.72F, 0.0F, 1.0F);
                break label412;
              }
            }
            if ((Noxious.getModManager().getModByName("paralyze").isEnabled()) &&
              (distance <= 0.5D)) {
              GL11.glColor4f(2.55F, 2.55F, 0.0F, 1.0F);
            } else if (((EntityLivingBase)entity).hurtTime > 0) {
              GL11.glColor4f(2.55F, 0.0F, 0.0F, 1.0F);
            } else {
              GL11.glColor4f(0.0F, 2.55F, 2.55F, 1.0F);
            }
            net.minecraft.client.renderer.entity.RendererLivingEntity.shouldRenderParts = false;
            mc.getRenderManager().renderEntitySimple(entity, partialTicks);
            net.minecraft.client.renderer.entity.RendererLivingEntity.shouldRenderParts = true;
          }
        }
        GL11.glEnable(3553);
        GL11.glPopMatrix();
      }
    }
    GL11.glEndList();

    GL11.glPolygonMode(1032, 6913);
    GL11.glCallList(entityDispList);

    GL11.glPolygonMode(1032, 6912);
    GL11.glCallList(entityDispList);

    Stencil.getInstance().setBuffer(false);

    GL11.glPolygonMode(1032, 6914);
    GL11.glCallList(entityDispList);

    Stencil.getInstance().cropInside();

    GL11.glPolygonMode(1032, 6913);
    GL11.glCallList(entityDispList);

    GL11.glPolygonMode(1032, 6912);
    GL11.glCallList(entityDispList);

    GL11.glPolygonMode(1032, 6914);

    Minecraft.getMinecraft().entityRenderer.func_175072_h();
    RenderHelper.disableStandardItemLighting();
    Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();

    Stencil.getInstance().stopLayer();
    GL11.glDisable(2960);
    GL11.glPopMatrix();

    Minecraft.getMinecraft().entityRenderer.func_175072_h();
    RenderHelper.disableStandardItemLighting();
    Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();

    GL11.glDeleteLists(entityDispList, 1);
  }

  public void drawChestOutlines(float partialTicks)
  {
    int entityDispList = GL11.glGenLists(1);

    Stencil.getInstance().startLayer();

    GL11.glPushMatrix();

    mc.entityRenderer.setupCameraTransform(partialTicks, 0);
    GL11.glMatrixMode(5888);
    RenderHelper.enableStandardItemLighting();
    GL11.glEnable(2884);
    Camera playerCam = new Camera(Minecraft.getMinecraft().thePlayer);
    Frustrum frustrum = new Frustrum();
    frustrum.setPosition(playerCam.getPosX(), playerCam.getPosY(), playerCam.getPosZ());

    GL11.glDisable(2929);
    GL11.glDepthMask(true);

    Stencil.getInstance().setBuffer(true);

    GL11.glNewList(entityDispList, 4864);
    for (Object obj : Minecraft.getMinecraft().theWorld.loadedTileEntityList)
    {
      TileEntity entity = (TileEntity)obj;
      if ((entity instanceof TileEntityLockable))
      {
        GL11.glLineWidth(((Float)this.width.getValue()).floatValue());
        GL11.glEnable(3042);
        GL11.glEnable(2848);
        GL11.glDisable(3553);
        GL11.glPushMatrix();
        GL11.glTranslated(entity.getPos().getX() - RenderManager.renderPosX,
          entity.getPos().getY() - RenderManager.renderPosY, entity.getPos().getZ() - RenderManager.renderPosZ);
        GL11.glColor4f(2.18F, 1.65F, 0.32F, 1.0F);
        TileEntityRendererDispatcher.instance.renderTileEntityAt(entity, 0.0D, 0.0D, 0.0D, partialTicks);
        GL11.glEnable(3553);
        GL11.glPopMatrix();
      }
    }
    GL11.glEndList();

    GL11.glPolygonMode(1032, 6913);
    GL11.glCallList(entityDispList);

    GL11.glPolygonMode(1032, 6912);
    GL11.glCallList(entityDispList);

    Stencil.getInstance().setBuffer(false);

    GL11.glPolygonMode(1032, 6914);
    GL11.glCallList(entityDispList);

    Stencil.getInstance().cropInside();

    GL11.glPolygonMode(1032, 6913);
    GL11.glCallList(entityDispList);

    GL11.glPolygonMode(1032, 6912);
    GL11.glCallList(entityDispList);

    GL11.glPolygonMode(1032, 6914);

    Minecraft.getMinecraft().entityRenderer.func_175072_h();
    RenderHelper.disableStandardItemLighting();
    Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();

    Stencil.getInstance().stopLayer();
    GL11.glDisable(2960);
    GL11.glPopMatrix();

    Minecraft.getMinecraft().entityRenderer.func_175072_h();
    RenderHelper.disableStandardItemLighting();
    Minecraft.getMinecraft().entityRenderer.setupOverlayRendering();

    GL11.glDeleteLists(entityDispList, 1);
  }
}
