package noxious.mod.mods;

import net.minecraft.network.play.server.S02PacketChat;
import noxious.event.Event;
import noxious.event.events.EventDrawScreen;
import noxious.event.events.EventPacketReceive;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.util.NahrFont;
import noxious.util.RenderHelper;
import noxious.util.TimeHelper;

public class LagDetector extends Mod {

   private final TimeHelper time = new TimeHelper();


   public TimeHelper getTime() {
      return this.time;
   }

   public LagDetector() {
      super("LagDetector");
      this.setEnabled(true);
      this.setCategory(Category.HUD);
   }

   public void onEvent(Event event) {
      if(event instanceof EventDrawScreen) {
         if(this.time.hasReached(1000L)) {
            RenderHelper.getNahrFont().drawString("Lag: \u00a77" + (this.time.getCurrentMS() - this.time.getLastMS()) + "ms", 2.0F, -2.0F, NahrFont.FontType.SHADOW_THICK, -1, -16777216);
         }
      } else if(event instanceof EventPacketReceive) {
    	  EventPacketReceive receive = (EventPacketReceive)event;
         if(!(receive.getPacket() instanceof S02PacketChat)) {
            this.time.reset();
         }
      }

   }
}
