package noxious.mod.mods;

import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.network.play.client.C0DPacketCloseWindow;
import noxious.event.Event;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.TimeHelper;

public class XCarry extends Mod {

   private final TimeHelper time = new TimeHelper();
   private boolean inInventory;


   public XCarry() {
      super("XCarry", -6724045, ModManager.Category.PLAYER);
      this.setTag("XCarry");
   }

   public void onEvent(Event event) {
      if(event instanceof EventPacketSent) {
    	  EventPacketSent sent = (EventPacketSent)event;
         if(sent.getPacket() instanceof C0DPacketCloseWindow) {
            sent.setCancelled(true);
         }
      } else if(event instanceof EventPreSendMotionUpdates) {
         if(mc.currentScreen instanceof GuiInventory) {
            this.inInventory = true;
         } else {
            this.inInventory = false;
         }

         if(mc.currentScreen instanceof GuiInventory) {
            mc.playerController.updateController();
         }
      }

   }
}
