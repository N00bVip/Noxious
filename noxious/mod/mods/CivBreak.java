package noxious.mod.mods;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.BlockAir;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.item.ItemBlock;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventBlockBreaking;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.util.RenderHelper;
import noxious.value.Value;

public class CivBreak extends Mod {

   private BlockPos block;
   private EnumFacing side;
   private final Value<Integer> speed = new Value("civbreak_speed", Integer.valueOf(1));


   public CivBreak() {
      super("CivBreak", -6165654, ModManager.Category.EXPLOITS);
      Noxious.getCommandManager().getContents().add(new Command("civbreakradius", "<speed>", new String[]{"civradius", "cvr"}) {
          public void run(String message) {
             if(message.split(" ")[1].equalsIgnoreCase("-d")) {
                CivBreak.this.speed.setValue((Integer)CivBreak.this.speed.getDefaultValue());
             } else {
                CivBreak.this.speed.setValue(Integer.valueOf(Integer.parseInt(message.split(" ")[1])));
             }

             if(((Integer)CivBreak.this.speed.getValue()).intValue() > 3) {
                CivBreak.this.speed.setValue(Integer.valueOf(3));
             } else if(((Integer)CivBreak.this.speed.getValue()).intValue() < 1) {
                CivBreak.this.speed.setValue(Integer.valueOf(1));
             }

             Logger.logChat("CivBreak Speed set to: " + CivBreak.this.speed.getValue());
          }
       });
      Noxious.getCommandManager().getContents().add(new Command("civbreakspeed", "<speed>", new String[]{"civspeed", "cvs"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               CivBreak.this.speed.setValue((Integer)CivBreak.this.speed.getDefaultValue());
            } else {
               CivBreak.this.speed.setValue(Integer.valueOf(Integer.parseInt(message.split(" ")[1])));
            }

            if(((Integer)CivBreak.this.speed.getValue()).intValue() > 3) {
               CivBreak.this.speed.setValue(Integer.valueOf(3));
            } else if(((Integer)CivBreak.this.speed.getValue()).intValue() < 1) {
               CivBreak.this.speed.setValue(Integer.valueOf(1));
            }

            Logger.logChat("CivBreak Speed set to: " + CivBreak.this.speed.getValue());
         }
      });
   }

   public void onDisabled() {
      super.onDisabled();

      try {
         this.block = null;
         this.side = null;
      } catch (Exception var2) {
         ;
      }

   }

   public void onEvent(Event event) {
      if(event instanceof EventBlockBreaking) {
         try {
            EventBlockBreaking x = (EventBlockBreaking)event;
            if(x.getState() == EventBlockBreaking.EnumBlock.CLICK) {
               this.block = x.getPos();
               this.side = x.getSide();
               mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, this.block, this.side));
               mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, this.block, this.side));
               if(!(mc.thePlayer.getHeldItem().getItem() instanceof ItemBlock)) {
                  mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(this.block, -1, mc.thePlayer.getCurrentEquippedItem(), 0.0F, 0.0F, 0.0F));
               }
            }
         } catch (Exception var14) {
            ;
         }
      } else if(event instanceof EventPreSendMotionUpdates) {
    	  EventPreSendMotionUpdates var15 = (EventPreSendMotionUpdates)event;
         if(this.block != null) {
            float[] rotations = BlockHelper.getBlockRotations((double)this.block.getX(), (double)this.block.getY(), (double)this.block.getZ());
            var15.setYaw(rotations[0]);
            var15.setPitch(rotations[1]);
         }
      } else if(event instanceof EventPostSendMotionUpdates) {
         if(this.block != null && mc.thePlayer.getDistanceSq(this.block) < 22.399999618530273D) {
            for(int var16 = 0; var16 < ((Integer)this.speed.getValue()).intValue(); ++var16) {
               mc.thePlayer.swingItem();
               mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, this.block, this.side));
               mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, this.block, this.side));
               if(mc.thePlayer.getHeldItem() != null && !(mc.thePlayer.getHeldItem().getItem() instanceof ItemBlock)) {
                  mc.getNetHandler().addToSendQueue(new C08PacketPlayerBlockPlacement(this.block, -1, mc.thePlayer.getCurrentEquippedItem(), 0.0F, 0.0F, 0.0F));
               }

               mc.playerController.func_180512_c(this.block, this.side);
            }
         }
      } else if(event instanceof EventRender3D && this.block != null) {
         GL11.glDisable(2896);
         GL11.glDisable(3553);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         GL11.glDisable(2929);
         GL11.glEnable(2848);
         GL11.glDepthMask(false);
         GL11.glLineWidth(0.75F);
         if(this.block != null && mc.thePlayer.getDistanceSq(this.block) >= 22.399999618530273D) {
            GL11.glColor4f(1.0F, 0.2F, 0.0F, 1.0F);
         } else if(mc.theWorld.getBlockState(this.block).getBlock() instanceof BlockAir) {
            GL11.glColor4f(1.0F, 0.7F, 0.0F, 1.0F);
         } else {
            GL11.glColor4f(0.2F, 0.9F, 0.0F, 1.0F);
         }

         double var10000 = (double)this.block.getX();
         mc.getRenderManager();
         double var17 = var10000 - RenderManager.renderPosX;
         var10000 = (double)this.block.getY();
         mc.getRenderManager();
         double y = var10000 - RenderManager.renderPosY;
         var10000 = (double)this.block.getZ();
         mc.getRenderManager();
         double z = var10000 - RenderManager.renderPosZ;
         double xo = 1.0D;
         double yo = 1.0D;
         double zo = 1.0D;
         RenderHelper.drawLines(new AxisAlignedBB(var17, y, z, var17 + xo, y + yo, z + zo));
         RenderHelper.drawOutlinedBoundingBox(new AxisAlignedBB(var17, y, z, var17 + xo, y + yo, z + zo));
         if(this.block != null && mc.thePlayer.getDistanceSq(this.block) >= 22.399999618530273D) {
            GL11.glColor4f(1.0F, 0.2F, 0.0F, 0.11F);
         } else if(mc.theWorld.getBlockState(this.block).getBlock() instanceof BlockAir) {
            GL11.glColor4f(1.0F, 0.7F, 0.0F, 0.11F);
         } else {
            GL11.glColor4f(0.2F, 0.9F, 0.0F, 0.11F);
         }

         RenderHelper.drawFilledBox(new AxisAlignedBB(var17, y, z, var17 + xo, y + yo, z + zo));
         GL11.glDepthMask(true);
         GL11.glDisable(2848);
         GL11.glEnable(2929);
         GL11.glDisable(3042);
         GL11.glEnable(2896);
         GL11.glEnable(3553);
      }

   }
}
