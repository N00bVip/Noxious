package noxious.mod.mods;

import org.lwjgl.input.Keyboard;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.BlockPos;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPlayerMovement;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.util.TimeHelper;
import noxious.value.Value;

public class Fly
  extends Mod
{
  private Value<Float> speed = new Value("fly_speed", Float.valueOf(0.5F));
  private Value<Float> motion = new Value("fly_motion", Float.valueOf(1.75F));
  private Value<Boolean> nocheat = new Value("fly_nocheat", Boolean.valueOf(true));
  private int uptime = 0;
  private double lasty;
  private double firsty;
  private float height;
  private boolean setLoc;
  private final TimeHelper time = new TimeHelper();

  public Fly()
  {
    super("Fly", -9868951, ModManager.Category.MOVEMENT);
    this.setEnabled(false);
    Noxious.getCommandManager().getContents().add(new Command("flyspeed", "<speed-value>", new String[] { "flysp", "fs" })
    {
      public void run(String message)
      {
        if (message.split(" ")[1].equalsIgnoreCase("-d")) {
          Fly.this.speed.setValue((Float)Fly.this.speed.getDefaultValue());
        } else {
          Fly.this.speed.setValue(Float.valueOf(Float.parseFloat(message.split(" ")[1])));
        }
        if (((Float)Fly.this.speed.getValue()).floatValue() > 10.0F) {
          Fly.this.speed.setValue(Float.valueOf(10.0F));
        } else if (((Float)Fly.this.speed.getValue()).floatValue() < 0.5F) {
          Fly.this.speed.setValue(Float.valueOf(0.5F));
        }
        Logger.logChat("Fly Speed set to: " + Fly.this.speed.getValue());
      }
    });
    Noxious.getCommandManager().getContents().add(new Command("flymotion", "<factor>", new String[] { "flym", "fm" })
    {
      public void run(String message)
      {
        if (message.split(" ")[1].equalsIgnoreCase("-d")) {
          Fly.this.motion.setValue((Float)Fly.this.motion.getDefaultValue());
        } else {
          Fly.this.motion.setValue(Float.valueOf(Float.parseFloat(message.split(" ")[1])));
        }
        if (((Float)Fly.this.motion.getValue()).floatValue() > 5.0F) {
          Fly.this.motion.setValue(Float.valueOf(5.0F));
        } else if (((Float)Fly.this.motion.getValue()).floatValue() < 0.0F) {
          Fly.this.motion.setValue(Float.valueOf(0.001F));
        }
        Logger.logChat("Fly Motion Speed has been multiplied by: " + Fly.this.motion.getValue());
      }
    });
    Noxious.getCommandManager().getContents().add(new Command("flynocheat", "none", new String[] { "fnocheat", "fnc" })
    {
      public void run(String message)
      {
        Fly.this.nocheat.setValue(Boolean.valueOf(!((Boolean)Fly.this.nocheat.getValue()).booleanValue()));
        Logger.logChat("Fly will " + (((Boolean)Fly.this.nocheat.getValue()).booleanValue() ? "now" : "no longer") + " bypass nocheat.");
      }
    });
  }

  public void onEnabled()
  {
    super.onEnabled();
    if ((mc.thePlayer != null) && (((Boolean)this.nocheat.getValue()).booleanValue()))
    {
      this.time.reset();
      double x = mc.thePlayer.posX;
      double y = mc.thePlayer.posY;
      double z = mc.thePlayer.posZ;
      for (int i = 0; i < 4; i++)
      {
        Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y + 1.01D, z, false));
        Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y, z, false));
      }
      Minecraft.getMinecraft().thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x, y + 0.8D, z, false));
      Logger.logChat("Make your counter doesn't go above §a80§f, else you'll get kicked.");
      mc.thePlayer.motionY = 0.1D;
      this.firsty = mc.thePlayer.posY;
    }
  }

  public void onDisabled()
  {
    super.onDisabled();
    if (mc.thePlayer != null)
    {
      if (mc.thePlayer.capabilities.isFlying) {
        mc.thePlayer.capabilities.isFlying = false;
      }
      mc.thePlayer.capabilities.setFlySpeed(0.05F);
    }
  }

  public void onEvent(Event event)
  {
    if (((event instanceof EventPreSendMotionUpdates)) && (!((Boolean)this.nocheat.getValue()).booleanValue()))
    {
      if (mc.thePlayer.onGround) {
        setColor(-9868951);
      } else {
        setColor(-7876885);
      }
      setTag("Fly");
      if (!mc.thePlayer.capabilities.isFlying) {
        mc.thePlayer.capabilities.isFlying = true;
      }
      mc.thePlayer.capabilities.setFlySpeed(((Float)this.speed.getValue()).floatValue() / 10.0F);
      if (mc.gameSettings.keyBindJump.pressed) {
        mc.thePlayer.motionY = (((Float)this.speed.getValue()).floatValue() / 2.0F);
      }
      if (mc.gameSettings.keyBindSneak.pressed) {
        mc.thePlayer.motionY = (-((Float)this.speed.getValue()).floatValue() / 2.0F);
      }
    }
    else if (((event instanceof EventPreSendMotionUpdates)) && (((Boolean)this.nocheat.getValue()).booleanValue()))
    {
      if (mc.thePlayer.onGround) {
        setColor(-9868951);
      } else {
        setColor(-7876885);
      }
      if (Keyboard.isKeyDown(21))
      {
        mc.thePlayer.onGround = false;
        mc.thePlayer.motionY = 9.999999747378752E-5D;
      }
      setTag("Fly §fNCP §7" + this.uptime);
      if (mc.gameSettings.keyBindJump.getIsKeyPressed()) {
        mc.thePlayer.motionY = 0.5D;
      } else if (mc.gameSettings.keyBindSneak.getIsKeyPressed()) {
        mc.thePlayer.motionY = -0.5D;
      }
      if (mc.thePlayer.fallDistance < 2.0F) {
        mc.thePlayer.fallDistance = 2.0F;
      }
      if ((!mc.thePlayer.onGround) && (!mc.gameSettings.keyBindJump.getIsKeyPressed()) && (!mc.gameSettings.keyBindSneak.getIsKeyPressed()))
      {
        mc.thePlayer.motionY = -0.03D;
        Block var4 = mc.theWorld.getBlockState(new BlockPos((int)Math.round(mc.thePlayer.posX), (int)Math.round(mc.thePlayer.boundingBox.minY - 1.5D), (int)Math.round(mc.thePlayer.posZ))).getBlock();
        if (!(var4 instanceof BlockAir))
        {
          this.setLoc = true;
        }
        else
        {
          this.setLoc = false;
          this.height = 0.6F;
        }
      }
      else if ((this.setLoc) && (mc.thePlayer.onGround) && (this.height >= 0.11D))
      {
        this.height = ((float)(this.height - 0.005D));
      }
    }
    else if ((event instanceof EventPacketSent))
    {
      if (((Boolean)this.nocheat.getValue()).booleanValue())
      {
        EventPacketSent sent = (EventPacketSent)event;
        if ((sent.getPacket() instanceof C03PacketPlayer))
        {
          C03PacketPlayer player = (C03PacketPlayer)sent.getPacket();
          if ((player.getPositionY() > this.lasty) && (!mc.thePlayer.onGround)) {
            this.uptime += 1;
          }
          if (Keyboard.isKeyDown(21)) {
            player.field_149474_g = false;
          }
          if (mc.thePlayer.onGround) {
            this.uptime = 0;
          }
          this.lasty = player.getPositionY();
          if (this.time.hasReached(1000.0F))
          {
            player.y = (mc.thePlayer.posY + this.height);
            player.field_149474_g = false;
          }
        }
      }
    }
    else if (((event instanceof EventPlayerMovement)) && (((Boolean)this.nocheat.getValue()).booleanValue()) && (this.time.hasReached(100.0F)) && (!mc.gameSettings.keyBindJump.pressed) && (!mc.gameSettings.keyBindSneak.pressed))
    {
      ((EventPlayerMovement)event).setY(((EventPlayerMovement)event).getY() * ((Float)this.motion.getValue()).floatValue());
    }
  }
}
