package noxious.mod.mods;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C0BPacketEntityAction;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventOnUpdate;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.event.events.EventRender3D;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.mod.mods.aura.AuraMode;
import noxious.mod.mods.aura.modes.Multi;
import noxious.util.EntityUtils;
import noxious.util.Logger;
import noxious.util.RenderHelper;
import noxious.value.ClampedValue;
import noxious.value.Value;

public class XivKillAura extends Mod {
	public static final List<EntityLivingBase> targets = new CopyOnWriteArrayList();
	public final ClampedValue<Long> delay = new ClampedValue<>("xivkillaura_delay", 200L, 0L, 1000L);
	public final ClampedValue<Long> randomDelay = new ClampedValue<>("xivkillaura_random_delay", 5L, 0L, 10000L);
	public final ClampedValue<Double> range = new ClampedValue<>("xivkillaura_range", 3.8D, 3.0D, 6.0D);
	public final ClampedValue<Integer> fov = new ClampedValue<>("xivkillaura_fov", 360, 0, 360);
	public final ClampedValue<Integer> ticksToWait = new ClampedValue<>("xivkillaura_ticks_to_wait", 20, 0, 1000);
	public final ClampedValue<Integer> maxTarget = new ClampedValue<>("xivkillaura_maxTarget", 4, 0, 10);
	private final Value<String> type = new Value("killaura_mode", String.valueOf("anni"));
	public final Value<Boolean> esp = new Value("xivkillaura_esp", Boolean.valueOf(true));
	private final Value<Boolean> players = new Value<>("xivkillaura_players", true);
	private final Value<Boolean> mobs = new Value<>("xivkillaura_mobs", false);
	private final Value<Boolean> animals = new Value<>("xivkillaura_animals", true);
	private final Value<Boolean> invisible = new Value<>("xivkillaura_invisible", false);
	private final Value<Boolean> friends = new Value<>("xivkillaura_friends", false);
	public final Value<Boolean> silent = new Value<>("xivkillaura_silent", false);
	public final Value<Boolean> anglecheck = new Value<>("xivkillaura_anglecheck", true);
	public final Value<Boolean> delaycheck = new Value<>("xivkillaura_anglecheck", false);
	private final Random random = new Random();
	private final Value<AuraMode> currentMode = new Value<>("xivkillaura_mode", new Multi(this));
	public static EntityLivingBase entityToAttack;

	public XivKillAura() {
		super("XivKillAura", -7733248, ModManager.Category.COMBAT);
		setTag("Xiv Kill Aura");

	      Noxious.getCommandManager().getContents().add(new Command("xivkillaura", "<players/invisibles/mobs/animals/type>", new String[0]) {
	          public void run(String message) {
	             if(message.split(" ")[1].equalsIgnoreCase("players")) {
	                players.setValue(Boolean.valueOf(!((Boolean)players.getValue()).booleanValue()));
	                Logger.logChat("Kill Aura will " + (((Boolean)players.getValue()).booleanValue()?"now":"no longer") + " hit players.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("esp")) {
	                esp.setValue(Boolean.valueOf(!((Boolean)esp.getValue()).booleanValue()));
	                Logger.logChat("Kill Aura will " + (((Boolean)esp.getValue()).booleanValue()?"now":"no longer") + " render an ESP box over the target.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("invisibles")) {
	            	 invisible.setValue(Boolean.valueOf(!((Boolean)invisible.getValue()).booleanValue()));
	                Logger.logChat("Kill Aura will " + (((Boolean)invisible.getValue()).booleanValue()?"now":"no longer") + " hit invisibles.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("silent")) {
	            	 silent.setValue(Boolean.valueOf(!((Boolean)silent.getValue()).booleanValue()));
	            	 Logger.logChat("Kill Aura will " + (((Boolean)silent.getValue()).booleanValue()?"now":"no longer") + " silent.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("mobs")) {
	                mobs.setValue(Boolean.valueOf(!((Boolean)mobs.getValue()).booleanValue()));
	                Logger.logChat("Kill Aura will " + (((Boolean)mobs.getValue()).booleanValue()?"now":"no longer") + " hit mobs.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("anglecheck")) {
		                anglecheck.setValue(Boolean.valueOf(!((Boolean)anglecheck.getValue()).booleanValue()));
		                Logger.logChat("Kill Aura will " + (((Boolean)anglecheck.getValue()).booleanValue()?"now":"no longer") + " angle check.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("delaycheck")) {
	            	 delaycheck.setValue(Boolean.valueOf(!((Boolean)delaycheck.getValue()).booleanValue()));
	            	 Logger.logChat("Kill Aura will " + (((Boolean)delaycheck.getValue()).booleanValue()?"now":"no longer") + " delay check.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("range")) {
		                if(message.split(" ")[2].equalsIgnoreCase("-d")) {
		                	range.setValue((Double)range.getDefaultValue());
		                 } else {
		                    range.setValue(Double.valueOf(Double.parseDouble(message.split(" ")[2])));
		                 }

		                 if(((Double)range.getValue()).doubleValue() > 6.0D) {
		                    range.setValue(Double.valueOf(6.0D));
		                 } else if(((Double)range.getValue()).doubleValue() < 1.0D) {
		                	range.setValue(Double.valueOf(1.0D));
		                 }

		                 Logger.logChat("Kill Aura Reach set to: " + range.getValue());
	             } else if(message.split(" ")[1].equalsIgnoreCase("randomdelay")) {
	            	 if(message.split(" ")[2].equalsIgnoreCase("-d")) {
	            		 randomDelay.setValue((Long)randomDelay.getDefaultValue());
	                  } else {
	                	  randomDelay.setValue(Long.valueOf(Long.parseLong(message.split(" ")[2])));
	                  }

	                  if(((Long)randomDelay.getValue()).longValue() > 1000L) {
	                     randomDelay.setValue(Long.valueOf(1000L));
	                  } else if(((Long)randomDelay.getValue()).longValue() < 0L) {
	                     randomDelay.setValue(Long.valueOf(0L));
	                  }

	                  Logger.logChat("Kill Aura random Delay set to: " + randomDelay.getValue());
	             } else if(message.split(" ")[1].equalsIgnoreCase("delay")) {
	            	 if(message.split(" ")[2].equalsIgnoreCase("-d")) {
	            		 delay.setValue((Long)delay.getDefaultValue());
	                  } else {
	                	  delay.setValue(Long.valueOf(Long.parseLong(message.split(" ")[2])));
	                  }

	                  if(((Long)delay.getValue()).longValue() > 10000L) {
	                	  delay.setValue(Long.valueOf(10000L));
	                  } else if(((Long)delay.getValue()).longValue() <1L) {
	                	  delay.setValue(Long.valueOf(1L));
	                  }

	                  Logger.logChat("Kill Aura Delay set to: " + delay.getValue());
	             } else if(message.split(" ")[1].equalsIgnoreCase("animals")) {
	                animals.setValue(Boolean.valueOf(!((Boolean)animals.getValue()).booleanValue()));
	                Logger.logChat("Kill Aura will " + (((Boolean)animals.getValue()).booleanValue()?"now":"no longer") + " hit animals.");
	             } else if(message.split(" ")[1].equalsIgnoreCase("type")) {
	             	if (message.split(" ")[2].equalsIgnoreCase("none")) {
	             		type.setValue("none");
	             		Logger.logChat("Kill Aura will no longer bypass");
	             	} else if (message.split(" ")[2].equalsIgnoreCase("anni")) {
	             		type.setValue("anni");
	             		Logger.logChat("Kill Aura will now bypass Anni NPC.");
	             	} else if (message.split(" ")[2].equalsIgnoreCase("minez")) {
	             		type.setValue("minez");
	             		Logger.logChat("Kill Aura will now bypass MineZ NPC.");
	             	} else {
	             		Logger.logChat("Option not valid! Available options: none, anni, gg, minez, free");
	             	}
	          	}else {
	                Logger.logChat("Option not valid! Available options: players, invisibles, mobs, animals, silent, type.");
	             }

	          }
	       });
	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventPreSendMotionUpdates) {
			EventPreSendMotionUpdates pre = ((EventPreSendMotionUpdates) event);
			currentMode.getValue().onPreMotionUpdate(pre);
		} else if(event instanceof EventPostSendMotionUpdates) {
			EventPostSendMotionUpdates post = ((EventPostSendMotionUpdates) event);
			currentMode.getValue().onPostMotionUpdate(post);
		}

		if (event instanceof EventPacketSent) {
			EventPacketSent packet = (EventPacketSent)event;
			if (packet.getPacket() instanceof C03PacketPlayer) {
				C03PacketPlayer player = (C03PacketPlayer)packet.getPacket();
				currentMode.getValue().onMotionPacket(player);
			}
		}
		if (event instanceof EventOnUpdate) {
			EventOnUpdate update = (EventOnUpdate)event;
			for (EntityLivingBase entity : targets) {
				if (!isValidEntity(entity) || entity.hurtResistantTime == 0 | entityToAttack != null && !targets.contains(entityToAttack)) {
					targets.remove(entity);
				}
			}
		}
		if (event instanceof EventRender3D && ((Boolean)esp.getValue()).booleanValue()) {
       	 	EventRender3D var13 = (EventRender3D)event;
       	 	GL11.glPushMatrix();
       	 	GL11.glDisable(3553);
       	 	GL11.glDisable(2896);
       	 	GL11.glEnable(3042);
       	 	GL11.glBlendFunc(770, 771);
       	 	GL11.glDisable(2929);
       	 	GL11.glEnable(2848);
       	 	GL11.glDepthMask(false);

       	 	for (EntityLivingBase var16 : targets) {
       	 		if(var16 != null && !var16.isDead && (double)var16.getDistanceToEntity(mc.thePlayer) <= ((Double)range.getValue()).doubleValue()) {
       	 			double var21 = var16.lastTickPosX + (var16.posX - var16.lastTickPosX) * (double)var13.getPartialTicks() - RenderManager.renderPosX;
       	 			double var25 = var16.lastTickPosY + (var16.posY - var16.lastTickPosY) * (double)var13.getPartialTicks() - RenderManager.renderPosY;
       	 			double posZ = var16.lastTickPosZ + (var16.posZ - var16.lastTickPosZ) * (double)var13.getPartialTicks() - RenderManager.renderPosZ;
       	 			GL11.glPushMatrix();
	      	        GL11.glTranslated(var21, var25, posZ);
       	 			GL11.glRotatef(-var16.rotationYaw, 0.0F, var16.height, 0.0F);
       	 			GL11.glTranslated(-var21, -var25, -posZ);
       	 			renderBox(var16, var21, var25, posZ);
       	 			GL11.glPopMatrix();
       	 		}
       	 	}

       	 	GL11.glDepthMask(true);
       	 	GL11.glDisable(2848);
       	 	GL11.glEnable(2929);
       	 	GL11.glEnable(2896);
       	 	GL11.glDisable(3042);
       	 	GL11.glEnable(3553);
       	 	GL11.glPopMatrix();
		}
	}

	   public void attack(Entity entity) {
		      int original = mc.thePlayer.inventory.currentItem;
		          int itemb4 = mc.thePlayer.inventory.currentItem;
		          boolean b4sprinting = mc.thePlayer.isSprinting();

		         int oldDamage = 0;
		         if(mc.thePlayer.getCurrentEquippedItem() != null) {
		            oldDamage = mc.thePlayer.getCurrentEquippedItem().getItemDamage();
		         }

		         if(mc.thePlayer.isBlocking()) {
		            mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, new BlockPos(0, 0, 0), EnumFacing.UP));
		         }
		         mc.thePlayer.swingItem();

		         mc.getNetHandler().getNetworkManager().sendPacket(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));

		         if(mc.thePlayer.getCurrentEquippedItem() != null) {
		            mc.thePlayer.getCurrentEquippedItem().setItemDamage(oldDamage);
		         }

		         if( (double)mc.thePlayer.moveForward > 0.0D) {
		            mc.getNetHandler().addToSendQueue(new C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.START_SPRINTING));
		         }


		   }

	public boolean isValidEntity(EntityLivingBase entity) {
		return isValidEntity(entity, range.getValue());
	}

	public boolean isValidEntity(EntityLivingBase entity, double range) {
		if (entity == null)
			return false;
		if (entity == mc.thePlayer)
			return false;
		if (entity == EntityUtils.getReference())
			return false;
		if (EntityUtils.getAngle(EntityUtils.getEntityRotations(entity)) > fov.getValue())
			return false;
		if (!entity.isEntityAlive())
			return false;
		if (entity.ticksExisted < ticksToWait.getValue())
			return false;
		if (mc.thePlayer.getDistanceToEntity(entity) > range)
			return false;
		if (!invisible.getValue() && entity.isInvisibleToPlayer(mc.thePlayer))
			return false;
		if (entity instanceof EntityPlayer) {
			if (entity.getName() == mc.thePlayer.getName())
				return false;
			if (type.getValue().equalsIgnoreCase("anni") && (entity.getTeam() == null || entity.getTeam().isSameTeam(mc.thePlayer.getTeam())))
				return false;
			if(type.getValue().equals("minez")) {

                int armorAir = 0;
                int offset;
                for(offset = 3; offset >= 0; --offset) {
                   ItemStack xPos = ((EntityPlayer) entity).inventory.armorInventory[offset];
                   if(xPos == null) {
                	   armorAir += 1;
                   }
                }

                int inventoryAir = 0;
                int offset1;
                for(offset1 = 35; offset1 >= 0; --offset1) {
                   ItemStack xPos = ((EntityPlayer) entity).inventory.getStackInSlot(offset1);
                   if(xPos == null) {
                	   inventoryAir += 1;
                   }
                }

                if (inventoryAir >= 36 && armorAir >= 4) {
                	return false;
                }
			}
			if (!friends.getValue() && Noxious.getFriendManager().isFriend(entity.getName()))
				return false;
			return players.getValue();

		} else if (entity instanceof IAnimals && !(entity instanceof IMob)) {
			if (entity instanceof EntityHorse) {
				EntityHorse horse = (EntityHorse) entity;
				return animals.getValue() && horse.riddenByEntity != mc.thePlayer;
			}
			return animals.getValue();
		} else if (entity instanceof IMob) {
			return mobs.getValue();
		}

		return false;
	}

	public Long getDelay() {
		if (randomDelay.getValue() == 0)
			return delay.getValue();
		return delay.getValue() + random.nextInt(randomDelay.getValue().intValue());
	}

	public boolean isHealing() {
		return false;
	}

	private void renderBox(Entity entity, double x, double y, double z) {
		AxisAlignedBB box = AxisAlignedBB.fromBounds(x - (double)entity.width, y, z - (double)entity.width, x + (double)entity.width, y + (double)entity.height + 0.2D, z + (double)entity.width);
		float distance = mc.thePlayer.getDistanceToEntity(entity);
		float[] color;
		if(entity instanceof EntityLivingBase && ((EntityLivingBase)entity).hurtTime > 0) {
			color = new float[]{1.0F, 0.66F, 0.0F};
		} else {
			color = new float[]{0.0F, 0.9F, 0.0F};
		}
		GL11.glColor4f(color[0], color[1], color[2], 0.8F);
		RenderHelper.drawOutlinedBoundingBox(box);
		GL11.glColor4f(color[0], color[1], color[2], 0.15F);
		RenderHelper.drawFilledBox(box);
	}

	public void onDisabled() {
		super.onDisabled();
		currentMode.getValue().onDisabled();
	}
}
