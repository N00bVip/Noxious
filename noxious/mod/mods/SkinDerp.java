package noxious.mod.mods;

import java.util.Set;

import net.minecraft.entity.player.EnumPlayerModelParts;
import noxious.event.Event;
import noxious.event.events.EventTick;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class SkinDerp extends Mod {

   private Set original;


   public SkinDerp() {
      super("SkinDerp");
      this.setEnabled(true);
      this.setCategory(Category.PLAYER);
   }

   public void onDisabled() {
      super.onDisabled();
      if(mc.thePlayer != null && mc.thePlayer != null) {
         EnumPlayerModelParts[] parts = EnumPlayerModelParts.values();
         if(parts != null) {
            EnumPlayerModelParts[] arrayOfEnumPlayerModelParts1 = parts;
            int j = parts.length;

            for(int i = 0; i < j; ++i) {
               EnumPlayerModelParts part = arrayOfEnumPlayerModelParts1[i];
               mc.gameSettings.func_178878_a(part, true);
            }
         }
      }

   }

   public void onEvent(Event event) {
      if(event instanceof EventTick && mc.thePlayer != null) {
         EnumPlayerModelParts[] parts = EnumPlayerModelParts.values();
         if(parts != null) {
            EnumPlayerModelParts[] arrayOfEnumPlayerModelParts1 = parts;
            int j = parts.length;

            for(int i = 0; i < j; ++i) {
               EnumPlayerModelParts part = arrayOfEnumPlayerModelParts1[i];
               mc.gameSettings.func_178878_a(part, getRandom().nextBoolean());
            }
         }
      }

   }
}
