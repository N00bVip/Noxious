package noxious.mod.mods;

import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventBlockBoundingBox;
import noxious.event.events.EventInsideBlock;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPlayerMovement;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.value.Value;

public class MotionClip extends Mod {

   private final Value<Double> offset = new Value("noclip_offset", Double.valueOf(-1.45D));
   private int ticks;


   public MotionClip() {
      super("MotionClip", -9868951, ModManager.Category.EXPLOITS);
      this.setTag("Motion Clip");
      this.setEnabled(false);
      Noxious.getCommandManager().getContents().add(new Command("motionclipoffset", "<offset>", new String[]{"mcoffset", "mco"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               MotionClip.this.offset.setValue((Double)MotionClip.this.offset.getDefaultValue());
            } else {
               MotionClip.this.offset.setValue(Double.valueOf(Double.parseDouble(message.split(" ")[1])));
            }

            if(((Double)MotionClip.this.offset.getValue()).doubleValue() < -1.45D) {
               MotionClip.this.offset.setValue(Double.valueOf(-1.45D));
            } else if(((Double)MotionClip.this.offset.getValue()).doubleValue() > 1.45D) {
               MotionClip.this.offset.setValue(Double.valueOf(1.45D));
            }

            Logger.logChat("Motion Clip Offset set to: " + MotionClip.this.offset.getValue());
         }
      });
   }

   public void onEvent(Event event) {
      if(event instanceof EventBlockBoundingBox) {
         if(mc.thePlayer == null) {
            return;
         }

         EventBlockBoundingBox sent = (EventBlockBoundingBox)event;
         if((int)(mc.thePlayer.boundingBox.minY - 0.5D) < sent.getY()) {
            sent.setBoundingBox((AxisAlignedBB)null);
         }
      } else if(event instanceof EventPlayerMovement) {
    	  EventPlayerMovement var4 = (EventPlayerMovement)event;
         if(BlockHelper.isInsideBlock(mc.thePlayer)) {
            var4.setX(var4.getX() / 2.0D);
            var4.setY(var4.getY() / 2.0D);
         }
      } else if(event instanceof EventPreSendMotionUpdates) {
         if(BlockHelper.isInsideBlock(mc.thePlayer)) {
            this.setColor(-2448096);
         } else {
            this.setColor(-9868951);
         }

         if(BlockHelper.isInsideBlock(mc.thePlayer)) {
            ++this.ticks;
         }
      } else if(event instanceof EventPacketSent) {
    	  EventPacketSent var5 = (EventPacketSent)event;
         if(var5.getPacket() instanceof C03PacketPlayer) {
            C03PacketPlayer player = (C03PacketPlayer)var5.getPacket();
            if(this.ticks >= 5) {
               player.y -= ((Double)this.offset.getValue()).doubleValue();
               this.ticks = 0;
            }
         }
      } else if(event instanceof EventInsideBlock) {
         ((EventInsideBlock)event).setCancelled(true);
      }

   }
}
