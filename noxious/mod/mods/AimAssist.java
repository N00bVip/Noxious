package noxious.mod.mods;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.EntityHelper;
import noxious.util.Location;
import noxious.util.Logger;
import noxious.value.Value;

public class AimAssist extends Mod {

   private final Value<Boolean> mobs = new Value("aimassist_mobs", Boolean.valueOf(true));
   private final Value<Boolean> players = new Value("aimassist_players", Boolean.valueOf(true));
   private final Value<Boolean> animals = new Value("aimassist_animals", Boolean.valueOf(true));
   private final Value<Double> reach = new Value("aimassist_reach", Double.valueOf(3.8D));
   private int tweenAmnt = 0;
   private EntityLivingBase target = null;
   private Random rand = new Random();


   public AimAssist() {
      super("AimAssist", -983056, ModManager.Category.COMBAT);
      this.setTag("Aim Assist");
      Noxious.getCommandManager().getContents().add(new Command("aimassist", "<animals/players/mobs>", new String[0]) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("players")) {
               AimAssist.this.players.setValue(Boolean.valueOf(!((Boolean)AimAssist.this.players.getValue()).booleanValue()));
               Logger.logChat("Aim Assist will " + (((Boolean)AimAssist.this.players.getValue()).booleanValue()?"now":"no longer") + " aim at players.");
            } else if(message.split(" ")[1].equalsIgnoreCase("mobs")) {
               AimAssist.this.mobs.setValue(Boolean.valueOf(!((Boolean)AimAssist.this.mobs.getValue()).booleanValue()));
               Logger.logChat("Aim Assist will " + (((Boolean)AimAssist.this.mobs.getValue()).booleanValue()?"now":"no longer") + " aim at players.");
            } else if(message.split(" ")[1].equalsIgnoreCase("animals")) {
               AimAssist.this.animals.setValue(Boolean.valueOf(!((Boolean)AimAssist.this.animals.getValue()).booleanValue()));
               Logger.logChat("Aim Assist will " + (((Boolean)AimAssist.this.animals.getValue()).booleanValue()?"now":"no longer") + " aim at animals.");
            } else {
               Logger.logChat("Option not valid! Available options: animals, players, mobs.");
            }

         }
      });
      Noxious.getCommandManager().getContents().add(new Command("aimassistreach", "<blocks>", new String[]{"assistreach", "aar"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               AimAssist.this.reach.setValue((Double)AimAssist.this.reach.getDefaultValue());
            } else {
               AimAssist.this.reach.setValue(Double.valueOf(Double.parseDouble(message.split(" ")[1])));
            }

            if(((Double)AimAssist.this.reach.getValue()).doubleValue() > 8.0D) {
               AimAssist.this.reach.setValue(Double.valueOf(8.0D));
            } else if(((Double)AimAssist.this.reach.getValue()).doubleValue() < 1.0D) {
               AimAssist.this.reach.setValue(Double.valueOf(1.0D));
            }

            Logger.logChat("Aim Assist Reach set to: " + AimAssist.this.reach.getValue());
         }
      });
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates && mc.thePlayer.getHeldItem() != null && (mc.thePlayer.getHeldItem().getItem() instanceof ItemSword || mc.thePlayer.getHeldItem().getItem() instanceof ItemAxe)) {
         this.target = null;
         if(mc.objectMouseOver != null && mc.objectMouseOver.entityHit instanceof EntityLivingBase) {
            this.target = (EntityLivingBase)mc.objectMouseOver.entityHit;
         }

         if(!this.isValidTarget(this.target)) {
            this.target = null;
         }

         if(this.target != null) {
            ++this.tweenAmnt;
            if(this.tweenAmnt > 3) {
               this.tweenAmnt = 0;
            }

            double prevX = this.target.posX;
            double prevY = this.target.posY;
            double prevZ = this.target.posZ;
            this.target.posY -= 0.4D;
            float[] values = new float[]{mc.thePlayer.rotationYaw + EntityHelper.getYawChangeToEntity(this.target) / (float)(5 - this.tweenAmnt), mc.thePlayer.rotationPitch + EntityHelper.getPitchChangeToEntity(this.target) / (float)(5 - this.tweenAmnt)};
            this.target.posX = prevX;
            this.target.posY = prevY;
            this.target.posZ = prevZ;
            mc.thePlayer.rotationYaw = values[0];
            mc.thePlayer.rotationPitch = values[1];
         }
      }

   }

   public boolean isInTargetMode(EntityLivingBase el) {
      boolean mobChecks = false;
      boolean playerChecks = el instanceof EntityPlayer && !Noxious.getFriendManager().isFriend(el.getName());
      if(el instanceof EntityMob) {
         mobChecks = true;
      } else if(el instanceof EntityWolf) {
         EntityWolf animalChecks = (EntityWolf)el;
         mobChecks = animalChecks.isAngry();
      } else if(el instanceof EntitySlime) {
         mobChecks = true;
      }

      boolean animalChecks1 = el instanceof EntityAnimal;
      boolean teamChecks = false;
      return playerChecks?((Boolean)this.players.getValue()).booleanValue():(mobChecks?((Boolean)this.mobs.getValue()).booleanValue():(animalChecks1?((Boolean)this.animals.getValue()).booleanValue():false));
   }

   public boolean isValidTarget(EntityLivingBase p) {
      if(p != null && p != mc.thePlayer && (double)mc.thePlayer.getDistanceToEntity(p) <= ((Double)this.reach.getValue()).doubleValue() && p.isEntityAlive() && !this.isTrueInvisible(p) && p instanceof EntityPlayer) {
         int var10000 = p.ticksExisted;
      }

      return this.isInTargetMode(p) && !p.isPlayerSleeping();
   }

   private double getDistToEntitySqPred(Entity e) {
      if(e instanceof EntityPlayer) {
         EntityPlayer p = (EntityPlayer)e;
         Location loc = EntityHelper.predictEntityLocation(p, (double)mc.getNetHandler().func_175102_a(mc.thePlayer.getUniqueID()).getResponseTime());
         if(loc != null) {
            return Math.min(mc.thePlayer.getDistanceSq(loc.x, loc.y, loc.z), mc.thePlayer.getDistanceSqToEntity(e));
         }
      }

      return mc.thePlayer.getDistanceSqToEntity(e);
   }

   public double getTargetWeight(EntityLivingBase el) {
      double weight = (double)(el.getMaxHealth() - (el.getHealth() + el.getAbsorptionAmount() + (float)(el.getTotalArmorValue() * 5)));
      weight -= mc.thePlayer.getDistanceSqToEntity(el) / 2.0D;
      if(el instanceof EntityPlayer) {
         weight += 50.0D;
      }

      if(el instanceof EntityCreeper) {
         weight += 35.0D;
      } else if(el instanceof EntitySkeleton) {
         weight += 25.0D;
      }

      return weight;
   }

   public boolean isTrueInvisible(EntityLivingBase el) {
      if(el instanceof EntityPlayer) {
         EntityPlayer p = (EntityPlayer)el;
         boolean hasArmour = false;
         ItemStack[] var7 = p.inventory.armorInventory;
         int var6 = p.inventory.armorInventory.length;

         for(int var5 = 0; var5 < var6; ++var5) {
            ItemStack stack = var7[var5];
            if(stack != null) {
               hasArmour = true;
            }
         }

         if(el.isInvisible() && el.getHeldItem() == null && !hasArmour) {
            return true;
         } else {
            return false;
         }
      } else {
         return el.isInvisible() && el.getHeldItem() == null;
      }
   }
}
