package noxious.mod.mods;

import net.minecraft.init.Blocks;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventBlockBreaking;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockData;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.value.Value;

public class Speedmine extends Mod {

   private final Value<Boolean> autotool = new Value("speedmine_autotool", Boolean.valueOf(true));
   private final Value<Boolean> instant = new Value("speedmine_instant", Boolean.valueOf(true));


   public Speedmine() {
      super("Speedmine", -12387247, ModManager.Category.PLAYER);
      Noxious.getCommandManager().getContents().add(new Command("speedmineautotool", "none", new String[]{"speedautotool", "smat"}) {
         public void run(String message) {
            Speedmine.this.autotool.setValue(Boolean.valueOf(!((Boolean)Speedmine.this.autotool.getValue()).booleanValue()));
            Logger.logChat("Speed Mine will " + (((Boolean)Speedmine.this.autotool.getValue()).booleanValue()?"now":"no longer") + " automatically switch tools.");
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("speedmineinstant", "none", new String[]{"instantmine", "smi"}) {
         public void run(String message) {
            Speedmine.this.instant.setValue(Boolean.valueOf(!((Boolean)Speedmine.this.instant.getValue()).booleanValue()));
            Logger.logChat("Speed Mine will " + (((Boolean)Speedmine.this.instant.getValue()).booleanValue()?"now":"no longer") + " mine blocks instantly.");
         }
      });
   }

   public void onEvent(Event event) {
      if(event instanceof EventBlockBreaking) {
         EventBlockBreaking bb = (EventBlockBreaking)event;
         if(bb.getState() == EventBlockBreaking.EnumBlock.DAMAGE) {
            if(!Noxious.getModManager().getModByName("civbreak").isEnabled()) {
               this.setColor(-5991988);
            }
         } else {
            this.setColor(-12387247);
         }

         if(bb.getState() == EventBlockBreaking.EnumBlock.CLICK) {
            if(((Boolean)this.instant.getValue()).booleanValue()) {
               mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.START_DESTROY_BLOCK, bb.getPos(), bb.getSide()));
               mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.STOP_DESTROY_BLOCK, bb.getPos(), bb.getSide()));
               mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, bb.getPos(), bb.getSide()));
            }

            if(((Boolean)this.autotool.getValue()).booleanValue() && !Noxious.getModManager().getModByName("nuker").isEnabled() && !Noxious.getModManager().getModByName("civbreak").isEnabled()) {
               BlockHelper.bestTool(bb.getPos().getX(), bb.getPos().getY(), bb.getPos().getZ());
            }
         }
      }

   }

   public BlockData getBlockData(BlockPos pos) {
      return mc.theWorld.getBlockState(pos.add(0, -1, 0)).getBlock() != Blocks.air?new BlockData(pos.add(0, -1, 0), EnumFacing.UP):(mc.theWorld.getBlockState(pos.add(-1, 0, 0)).getBlock() != Blocks.air?new BlockData(pos.add(-1, 0, 0), EnumFacing.EAST):(mc.theWorld.getBlockState(pos.add(1, 0, 0)).getBlock() != Blocks.air?new BlockData(pos.add(1, 0, 0), EnumFacing.WEST):(mc.theWorld.getBlockState(pos.add(0, 0, -1)).getBlock() != Blocks.air?new BlockData(pos.add(0, 0, -1), EnumFacing.SOUTH):(mc.theWorld.getBlockState(pos.add(0, 0, 1)).getBlock() != Blocks.air?new BlockData(pos.add(0, 0, 1), EnumFacing.NORTH):null))));
   }
}
