package noxious.mod.mods;

import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemBucketMilk;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import noxious.event.Event;
import noxious.event.events.EventPostSendMotionUpdates;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;

public class FastUse
  extends Mod
{
  public FastUse()
  {
    super("FastUse", -5985391, ModManager.Category.EXPLOITS);
    setTag("Fast Use");
  }

  private boolean isUsable(ItemStack stack)
  {
    if (stack == null) {
      return false;
    }
    if (mc.thePlayer.isUsingItem())
    {
      if ((stack.getItem() instanceof ItemBow)) {
        return true;
      }
      if ((stack.getItem() instanceof ItemFood)) {
        return true;
      }
      if ((stack.getItem() instanceof ItemPotion)) {
        return true;
      }
      if ((stack.getItem() instanceof ItemBucketMilk)) {
        return true;
      }
    }
    return false;
  }

  public void onEvent(Event event)
  {
    if ((event instanceof EventPostSendMotionUpdates))
    {
      if ((isUsable(mc.thePlayer.getCurrentEquippedItem())) && (mc.thePlayer.getItemInUseDuration() == 15))
      {
        for (int x = 0; x < 15; x++)
        {
          mc.getNetHandler().addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
          mc.getNetHandler().addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
          mc.getNetHandler().addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
          mc.getNetHandler().addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
          mc.getNetHandler().addToSendQueue(new C03PacketPlayer(mc.thePlayer.onGround));
        }
        mc.getNetHandler().addToSendQueue(new C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, new BlockPos(-1, -1, -1), EnumFacing.DOWN));
        mc.thePlayer.stopUsingItem();
        mc.playerController.updateController();
      }
    }
    else if ((event instanceof EventPreSendMotionUpdates)) {
      if ((isUsable(mc.thePlayer.getCurrentEquippedItem())) && (mc.thePlayer.isUsingItem())) {
        setColor(47872);
      } else {
        setColor(-5985391);
      }
    }
  }
}
