package noxious.mod.mods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.material.Material;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.StringUtils;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventDrawScreen;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;
import noxious.tabgui.TabGui;
import noxious.util.Logger;
import noxious.util.NahrFont;
import noxious.util.RenderHelper;
import noxious.value.Value;

public class HUD extends Mod {

   private int selected;
   private int selectedMod;
   private boolean open;
   private float width;
   private float height;
   public static Value<Boolean> arraylist = new Value("hud_arraylist", Boolean.valueOf(true));
   public static Value<Boolean> tabgui = new Value("hud_tabgui", Boolean.valueOf(true));
   public static Value<Boolean> tabguisaint = new Value("tabgui_noxious", Boolean.valueOf(true));
   public static Value<Boolean> tabguiwolfram = new Value("tabgui_tf2", Boolean.valueOf(false));
   public static Value<Boolean> armor = new Value("hud_armor-status", Boolean.valueOf(true));
   public static Value<Boolean> potions = new Value("hud_potioneffects", Boolean.valueOf(true));
   public static Value<Boolean> glucoes = new Value("hud_glucoes", Boolean.valueOf(false));
   public static Value<Boolean> noxious1 = new Value("hud_noxious", Boolean.valueOf(true));
   public static Value<Boolean> liquidsmoke = new Value("hud_liquidsmoke", Boolean.valueOf(true));
   public static Value<Boolean> arraylistrect = new Value("hud_array-rect", Boolean.valueOf(true));


   public HUD() {
      super("HUD");
      this.setEnabled(true);
      this.setCategory(Category.HUD);
      Noxious.getCommandManager().getContents().add(new Command("hud", "<armorstatus/potioneffects/arraylist/tabgui/arraylistrectangle>", new String[0]) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("arraylist")) {
               HUD.arraylist.setValue(Boolean.valueOf(!((Boolean)HUD.arraylist.getValue()).booleanValue()));
               Logger.logChat("HUD will " + (((Boolean)HUD.arraylist.getValue()).booleanValue()?"now":"no longer") + " display the arraylist.");
            } else if(message.split(" ")[1].equalsIgnoreCase("arraylistrectangle")) {
               HUD.arraylistrect.setValue(Boolean.valueOf(!((Boolean)HUD.arraylistrect.getValue()).booleanValue()));
               Logger.logChat("HUD will " + (((Boolean)HUD.arraylistrect.getValue()).booleanValue()?"now":"no longer") + " display the arraylist rectangle.");
            } else if(message.split(" ")[1].equalsIgnoreCase("tabgui")) {
               HUD.tabgui.setValue(Boolean.valueOf(!((Boolean)HUD.tabgui.getValue()).booleanValue()));
               Logger.logChat("HUD will " + (((Boolean)HUD.tabgui.getValue()).booleanValue()?"now":"no longer") + " display the tabgui.");
            } else if(message.split(" ")[1].equalsIgnoreCase("armorstatus")) {
               HUD.armor.setValue(Boolean.valueOf(!((Boolean)HUD.armor.getValue()).booleanValue()));
               Logger.logChat("HUD will " + (((Boolean)HUD.armor.getValue()).booleanValue()?"now":"no longer") + " display the armor status.");
            } else if(message.split(" ")[1].equalsIgnoreCase("potioneffects")) {
               HUD.potions.setValue(Boolean.valueOf(!((Boolean)HUD.potions.getValue()).booleanValue()));
               Logger.logChat("HUD will " + (((Boolean)HUD.potions.getValue()).booleanValue()?"now":"no longer") + " display the potion effects.");
            } else {
               Logger.logChat("Option not valid! Available options: armorstatus, potioneffects, arraylist, tabgui, arraylistrectangle .");
            }

         }
      });
   }

   public void onEvent(Event event) {
      if(event instanceof EventDrawScreen) {
         ScaledResolution scaledRes = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
         new CopyOnWriteArrayList();
         if(((Boolean)tabgui.getValue()).booleanValue()) {
            TabGui y = Noxious.getTabGUI();
            y.drawGui(2, ((Boolean)liquidsmoke.getValue()).booleanValue()?14:0, 55);
         }

         boolean flag = mc.fontRendererObj.getUnicodeFlag();
    	 mc.fontRendererObj.setUnicodeFlag(false);

         if(((Boolean)liquidsmoke.getValue()).booleanValue()) {
        	 GL11.glPopMatrix();
             GL11.glPushMatrix();
             GL11.glScaled(1.3D, 1.3D, 1.3D);
             mc.fontRendererObj.drawstringwithshadow(Noxious.getClientName().substring(0,1), 1.0F, 1.0F, 65280);
             GL11.glPopMatrix();
             GL11.glPushMatrix();
             GL11.glScaled(1.0D, 1.0D, 1.0D);
             mc.fontRendererObj.drawstringwithshadow(Noxious.getClientName().substring(1) + " " + Noxious.getVersion(), 9.5F, 3.5F, 65280);
         }

         float y1 = (float)(scaledRes.getScaledHeight() - 30);
         RenderHelper.getNahrFont().drawString("\u00a77X: \u00a7f" + Math.round(mc.thePlayer.posX) + "\u00a77 Y: \u00a7f" + Math.round(mc.thePlayer.posY) + "\u00a77 Z: \u00a7f" + Math.round(mc.thePlayer.posZ), 1.0F, GuiNewChat.getChatOpen()?y1:y1 + 14.0F, NahrFont.FontType.SHADOW_THICK, -2894893, -16777216);
         if(((Boolean)potions.getValue()).booleanValue()) {
            this.drawPotionEffects(scaledRes);
         }

         if(((Boolean)armor.getValue()).booleanValue()) {
            this.drawArmorStatus(scaledRes);
         }

         if(((Boolean)arraylist.getValue()).booleanValue()) {
            GL11.glEnable(3042);
            this.drawArraylist(scaledRes);
            GL11.glDisable(3042);
         }

         mc.fontRendererObj.setUnicodeFlag(flag);

      }

   }

   private void drawArraylist(ScaledResolution scaledRes) {
      int y = 0;
      List mods = Noxious.getModManager().getContents();
         Collections.sort(mods, new Comparator() {
            public int compare(Mod mod1, Mod mod2) {
               return RenderHelper.getNahrFont().getStringWidth(StringUtils.stripControlCodes(mod1.getTag())) > RenderHelper.getNahrFont().getStringWidth(StringUtils.stripControlCodes(mod2.getTag()))?-1:(RenderHelper.getNahrFont().getStringWidth(StringUtils.stripControlCodes(mod1.getTag())) < RenderHelper.getNahrFont().getStringWidth(StringUtils.stripControlCodes(mod2.getTag()))?1:0);
            }
            public int compare(Object var1, Object var2) {
               return this.compare((Mod)var1, (Mod)var2);
            }
         });

      Iterator var5 = mods.iterator();

      while(var5.hasNext()) {
         Mod module = (Mod)var5.next();
         if(module.isVisible() && module.isEnabled()) {
            ArrayList validmods = new ArrayList();
            validmods.add(module);
            Iterator var8 = validmods.iterator();

            while(var8.hasNext()) {
               Mod mod = (Mod)var8.next();
               int xf = (int)((float)scaledRes.getScaledWidth() - RenderHelper.getNahrFont().getStringWidth(StringUtils.stripControlCodes(mod.getTag())) - 2.0F);
               GL11.glPushMatrix();
               float width = mc.fontRendererObj.getStringWidth(mod.getTag());
               float height = scaledRes.getScaledHeight() - 10 - y;
               float widthc = (float)scaledRes.getScaledWidth() - mc.fontRendererObj.getStringWidth(mod.getTag()) - 2;
               mc.fontRendererObj.drawstringwithshadow(mod.getTag(), (int)widthc, (int)height, mod.getColor());
               if(this.height < height) {
                  this.height = height;
               }

               if(this.width < width) {
                  this.width = width;
               }

               y = (int)((double)y + (double)mc.fontRendererObj.FONT_HEIGHT + 0.7D);
               GL11.glPopMatrix();
            }
         }
      }

   }

   private void drawPotionEffects(ScaledResolution scaledRes) {
      int y = -2;

      for(Iterator var4 = mc.thePlayer.getActivePotionEffects().iterator(); var4.hasNext(); y += mc.fontRendererObj.FONT_HEIGHT) {
         PotionEffect effect = (PotionEffect)var4.next();
         Potion potion = Potion.potionTypes[effect.getPotionID()];
         String name = I18n.format(potion.getName(), new Object[0]);
         if(effect.getAmplifier() == 1) {
            name = name + " II";
         } else if(effect.getAmplifier() == 2) {
            name = name + " III";
         } else if(effect.getAmplifier() == 3) {
            name = name + " IV";
         } else if(effect.getAmplifier() == 4) {
            name = name + " V";
         } else if(effect.getAmplifier() == 5) {
            name = name + " VI";
         } else if(effect.getAmplifier() == 6) {
            name = name + " VII";
         } else if(effect.getAmplifier() == 7) {
            name = name + " VIII";
         } else if(effect.getAmplifier() == 8) {
            name = name + " IX";
         } else if(effect.getAmplifier() == 9) {
            name = name + " X";
         } else if(effect.getAmplifier() > 10) {
            name = name + " X+";
         } else {
            name = name + " I";
         }

         name = name + "\u00a7f: \u00a77" + Potion.getDurationString(effect);
         int color = Integer.MIN_VALUE;
         if(effect.getEffectName() == "potion.weither") {
            color = -16777216;
         } else if(effect.getEffectName() == "potion.weakness") {
            color = -9868951;
         } else if(effect.getEffectName() == "potion.waterBreathing") {
            color = -16728065;
         } else if(effect.getEffectName() == "potion.saturation") {
            color = -11179217;
         } else if(effect.getEffectName() == "potion.resistance") {
            color = -5658199;
         } else if(effect.getEffectName() == "potion.regeneration") {
            color = -1146130;
         } else if(effect.getEffectName() == "potion.poison") {
            color = -14513374;
         } else if(effect.getEffectName() == "potion.nightVision") {
            color = -6737204;
         } else if(effect.getEffectName() == "potion.moveSpeed") {
            color = -7876870;
         } else if(effect.getEffectName() == "potion.moveSlowdown") {
            color = -16741493;
         } else if(effect.getEffectName() == "potion.jump") {
            color = -5374161;
         } else if(effect.getEffectName() == "potion.invisibility") {
            color = -9404272;
         } else if(effect.getEffectName() == "potion.hunger") {
            color = -16744448;
         } else if(effect.getEffectName() == "potion.heal") {
            color = -65536;
         } else if(effect.getEffectName() == "potion.harm") {
            color = -3730043;
         } else if(effect.getEffectName() == "potion.fireResistance") {
            color = -29696;
         } else if(effect.getEffectName() == "potion.healthBoost") {
            color = -40121;
         } else if(effect.getEffectName() == "potion.digSpeed") {
            color = -989556;
         } else if(effect.getEffectName() == "potion.digSlowdown") {
            color = -5658199;
         } else if(effect.getEffectName() == "potion.damageBoost") {
            color = -7667712;
         } else if(effect.getEffectName() == "potion.confusion") {
            color = -5192482;
         } else if(effect.getEffectName() == "potion.blindness") {
            color = -8355712;
         } else if(effect.getEffectName() == "potion.absorption") {
            color = -23296;
         }


         RenderHelper.getNahrFont().drawString(name, (float)scaledRes.getScaledWidth() - RenderHelper.getNahrFont().getStringWidth(name) + 18.0F, y, NahrFont.FontType.SHADOW_THIN, color, 1090519039);
      }

   }

   private void drawArmorStatus(ScaledResolution scaledRes) {
      if(mc.playerController.isNotCreative()) {
         int x = 15;
         GL11.glPushMatrix();

         for(int index = 3; index >= 0; --index) {
            ItemStack stack = mc.thePlayer.inventory.armorInventory[index];
            if(stack != null) {
               mc.getRenderItem().func_180450_b(stack, scaledRes.getScaledWidth() / 2 + x - 1, scaledRes.getScaledHeight() - (mc.thePlayer.isInsideOfMaterial(Material.water)?65:55) - 2);
               mc.getRenderItem().func_175030_a(mc.fontRendererObj, stack, scaledRes.getScaledWidth() / 2 + x - 1, scaledRes.getScaledHeight() - (mc.thePlayer.isInsideOfMaterial(Material.water)?65:55) - 2);
               x += 18;
            }
         }

         GlStateManager.disableCull();
         GlStateManager.enableAlpha();
         GlStateManager.disableBlend();
         GlStateManager.disableLighting();
         GlStateManager.disableCull();
         GlStateManager.clear(256);
         GL11.glPopMatrix();
      }

   }
}
