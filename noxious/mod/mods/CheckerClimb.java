package noxious.mod.mods;

import net.minecraft.util.AxisAlignedBB;
import noxious.event.Event;
import noxious.event.events.EventBlockBoundingBox;
import noxious.management.managers.ModManager.Category;
import noxious.mod.Mod;

public class CheckerClimb extends Mod
{
    public CheckerClimb()
    {
        super("CheckerClimb", 0x00FF00, Category.EXPLOITS);
        this.setTag("Checker Climb");
        this.setEnabled(false);
    }

    public void onEvent(Event event)
    {
        if (event instanceof EventBlockBoundingBox)
        {
            EventBlockBoundingBox bb = (EventBlockBoundingBox)event;

            if ((double)bb.getY() > mc.thePlayer.posY)
            {
                bb.setBoundingBox((AxisAlignedBB)null);
            }
        }
    }
}
