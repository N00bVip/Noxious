package noxious.mod.mods;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import noxious.event.Event;
import noxious.event.events.EventBlockBoundingBox;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.TimeHelper;

public class Jesus extends Mod {

   private int waterUpdate = 1;
   private int getoutofwater;
   private int timer = 0;
   private final TimeHelper time = new TimeHelper();
   private boolean nextTick;


   public Jesus() {
      super("Jesus", -8355712, ModManager.Category.MOVEMENT);
   }

   public void onEvent(Event event) {
      if(event instanceof EventBlockBoundingBox) {
         if(mc.thePlayer == null) {
            return;
         }

         EventBlockBoundingBox shouldSendPacket = (EventBlockBoundingBox)event;
         if(shouldSendPacket.getBlock() instanceof BlockLiquid && !BlockHelper.isInLiquid() && !mc.thePlayer.isSneaking()) {
            try {
               if((double)BlockLiquid.getLiquidHeightPercent(shouldSendPacket.getBlock().getMetaFromState(mc.theWorld.getBlockState(new BlockPos(shouldSendPacket.getX(), shouldSendPacket.getY(), shouldSendPacket.getZ())))) <= 0.15D) {
                  shouldSendPacket.setBoundingBox(AxisAlignedBB.fromBounds((double)shouldSendPacket.getX(), (double)shouldSendPacket.getY(), (double)shouldSendPacket.getZ(), (double)(shouldSendPacket.getX() + 1), (double)(shouldSendPacket.getY() + 1), (double)(shouldSendPacket.getZ() + 1)));
               }
            } catch (Exception var5) {
               ;
            }
         }
      } else if(event instanceof EventPreSendMotionUpdates) {
         if(BlockHelper.isOnLiquid()) {
            this.setColor(-16728065);
         } else {
            this.setColor(-8355712);
         }

         if(BlockHelper.isInLiquid() && mc.thePlayer.isInsideOfMaterial(Material.air) && !mc.thePlayer.isSneaking()) {
            mc.thePlayer.motionY = 0.11D;
         }
      } else if(event instanceof EventPacketSent) {
         boolean shouldSendPacket1 = !mc.thePlayer.onGround && BlockHelper.getBlockAtPos(new BlockPos(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ)) instanceof BlockAir && BlockHelper.getBlockAtPos(new BlockPos(mc.thePlayer.posX, mc.thePlayer.posY - 1.0D, mc.thePlayer.posZ)) instanceof BlockAir && BlockHelper.getBlockAtPos(new BlockPos(mc.thePlayer.posX, mc.thePlayer.posY - 2.0D, mc.thePlayer.posZ)) instanceof BlockAir && BlockHelper.getBlockAtPos(new BlockPos(mc.thePlayer.posX, mc.thePlayer.posY - 3.0D, mc.thePlayer.posZ)) instanceof BlockLiquid;
         EventPacketSent sent = (EventPacketSent)event;
         if(sent.getPacket() instanceof C03PacketPlayer) {
            C03PacketPlayer player = (C03PacketPlayer)sent.getPacket();
            if(shouldSendPacket1) {
               player.field_149474_g = true;
            }

            if(BlockHelper.isOnLiquid()) {
               this.nextTick = !this.nextTick;
               if(this.nextTick) {
                  player.y -= 0.01D;
               }
            }
         }
      }

   }

   public boolean getColliding(int i) {
      if(!this.isEnabled()) {
         return false;
      } else {
         int mx = i;
         int mz = i;
         int max = i;
         int maz = i;
         if(mc.thePlayer.motionX > 0.01D) {
            mx = 0;
         } else if(mc.thePlayer.motionX < -0.01D) {
            max = 0;
         }

         if(mc.thePlayer.motionZ > 0.01D) {
            mz = 0;
         } else if(mc.thePlayer.motionZ < -0.01D) {
            maz = 0;
         }

         int xmin = MathHelper.floor_double(mc.thePlayer.getEntityBoundingBox().minX - (double)mx);
         int ymin = MathHelper.floor_double(mc.thePlayer.getEntityBoundingBox().minY - 1.0D);
         int zmin = MathHelper.floor_double(mc.thePlayer.getEntityBoundingBox().minZ - (double)mz);
         int xmax = MathHelper.floor_double(mc.thePlayer.getEntityBoundingBox().minX + (double)max);
         int ymax = MathHelper.floor_double(mc.thePlayer.getEntityBoundingBox().minY + 1.0D);
         int zmax = MathHelper.floor_double(mc.thePlayer.getEntityBoundingBox().minZ + (double)maz);

         for(int x = xmin; x <= xmax; ++x) {
            for(int y = ymin; y <= ymax; ++y) {
               for(int z = zmin; z <= zmax; ++z) {
                  Block block = BlockHelper.getBlockAtPos(new BlockPos(x, y, z));
                  if(block instanceof BlockLiquid && !mc.thePlayer.isInsideOfMaterial(Material.lava) && !mc.thePlayer.isInsideOfMaterial(Material.water)) {
                     return true;
                  }
               }
            }
         }

         return false;
      }
   }
}
