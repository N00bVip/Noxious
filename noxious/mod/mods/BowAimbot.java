package noxious.mod.mods;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBow;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.MathHelper;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.Logger;
import noxious.value.Value;

public class BowAimbot extends Mod {

   public final Value<Double> reach = new Value("bowaimbot_reach", Double.valueOf(32.0D));
   private final Value<Boolean> silent = new Value("bowaimbot_silent", Boolean.valueOf(false));
   private final Value<Boolean> players = new Value("bowaimbot_players", Boolean.valueOf(true));
   private final Value<Boolean> animals = new Value("bowaimbot_animals", Boolean.valueOf(false));
   private final Value<Boolean> mobs = new Value("bowaimbot_mobs", Boolean.valueOf(false));
   public final List<Entity> targets = new CopyOnWriteArrayList();


   public BowAimbot() {
      super("BowAimbot", -128, ModManager.Category.COMBAT);
      this.setTag("Bow Aimbot");
      Noxious.getCommandManager().getContents().add(new Command("bowaimbotreach", "<blocks>", new String[]{"bowreach", "bar"}) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("-d")) {
               BowAimbot.this.reach.setValue((Double)BowAimbot.this.reach.getDefaultValue());
            } else {
               BowAimbot.this.reach.setValue(Double.valueOf(Double.parseDouble(message.split(" ")[1])));
            }

            if(((Double)BowAimbot.this.reach.getValue()).doubleValue() > 64.0D) {
               BowAimbot.this.reach.setValue(Double.valueOf(64.0D));
            } else if(((Double)BowAimbot.this.reach.getValue()).doubleValue() < 8.0D) {
               BowAimbot.this.reach.setValue(Double.valueOf(8.0D));
            }

            Logger.logChat("Bow Aimbot Reach set to: " + BowAimbot.this.reach.getValue());
         }
      });
      Noxious.getCommandManager().getContents().add(new Command("bowaimbot", "<animals/players/mobs/silent>", new String[0]) {
         public void run(String message) {
            if(message.split(" ")[1].equalsIgnoreCase("players")) {
               BowAimbot.this.players.setValue(Boolean.valueOf(!((Boolean)BowAimbot.this.players.getValue()).booleanValue()));
               Logger.logChat("Bow Aimbot will " + (((Boolean)BowAimbot.this.players.getValue()).booleanValue()?"now":"no longer") + " aim at players.");
            } else if(message.split(" ")[1].equalsIgnoreCase("mobs")) {
               BowAimbot.this.mobs.setValue(Boolean.valueOf(!((Boolean)BowAimbot.this.mobs.getValue()).booleanValue()));
               Logger.logChat("Bow Aimbot will " + (((Boolean)BowAimbot.this.mobs.getValue()).booleanValue()?"now":"no longer") + " aim at players.");
            } else if(message.split(" ")[1].equalsIgnoreCase("animals")) {
               BowAimbot.this.animals.setValue(Boolean.valueOf(!((Boolean)BowAimbot.this.animals.getValue()).booleanValue()));
               Logger.logChat("Bow Aimbot will " + (((Boolean)BowAimbot.this.animals.getValue()).booleanValue()?"now":"no longer") + " aim at animals.");
            } else if(message.split(" ")[1].equalsIgnoreCase("silent")) {
               BowAimbot.this.silent.setValue(Boolean.valueOf(!((Boolean)BowAimbot.this.silent.getValue()).booleanValue()));
               Logger.logChat("Bow Aimbot will " + (((Boolean)BowAimbot.this.silent.getValue()).booleanValue()?"now":"no longer") + " aim silently.");
            } else {
               Logger.logChat("Option not valid! Available options: animals, players, mobs, silent.");
            }

         }
      });
   }

   private float[] getYawAndPitch2(Entity paramEntityPlayer) {
      double d1 = paramEntityPlayer.posX - mc.thePlayer.posX;
      double d2 = paramEntityPlayer.posZ - mc.thePlayer.posZ;
      double d3 = mc.thePlayer.posY + 0.12D - (paramEntityPlayer.posY + 1.82D);
      double d4 = (double)MathHelper.sqrt_double(d1 + d2);
      float f1 = (float)(Math.atan2(d2, d1) * 180.0D / 3.141592653589793D) - 90.0F;
      float f2 = (float)(Math.atan2(d3, d4) * 180.0D / 3.141592653589793D);
      return new float[]{f1, f2};
   }

   private float getDistanceBetweenAngles2(float paramFloat) {
      float f = Math.abs(paramFloat - mc.thePlayer.rotationYaw) % 360.0F;
      if(f > 180.0F) {
         f = 360.0F - f;
      }

      return f;
   }

   public boolean isValidTarget(Entity entity) {
      boolean valid = false;
      if(entity == mc.thePlayer.ridingEntity) {
         return false;
      } else if(!mc.thePlayer.canEntityBeSeen(entity)) {
         return false;
      } else {
         float[] arrayOfFloat = this.getYawAndPitch2(entity);
         double d2g = (double)this.getDistanceBetweenAngles2(arrayOfFloat[0]);
         double dooble = mc.isSingleplayer()?180.0D:40.0D;
         if(entity.isInvisible()) {
            valid = true;
         }

         if(entity instanceof EntityPlayer && ((Boolean)this.players.getValue()).booleanValue()) {
            valid = entity != null && d2g < dooble && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue() && entity != mc.thePlayer && entity.isEntityAlive() && !Noxious.getFriendManager().isFriend(entity.getName());
         } else if(entity instanceof IMob && ((Boolean)this.mobs.getValue()).booleanValue()) {
            valid = entity != null && d2g < dooble && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue() && entity.isEntityAlive();
         } else if(entity instanceof IAnimals && !(entity instanceof IMob) && ((Boolean)this.animals.getValue()).booleanValue()) {
            valid = entity != null && d2g < dooble && (double)mc.thePlayer.getDistanceToEntity(entity) <= ((Double)this.reach.getValue()).doubleValue() && entity.isEntityAlive();
         }

         return valid;
      }
   }

   public void onEvent(Event event) {
      if(event instanceof EventPreSendMotionUpdates) {
    	  EventPreSendMotionUpdates sent = (EventPreSendMotionUpdates)event;
         if(this.targets.isEmpty()) {
            this.populateTargets();
         }

         Iterator var4 = this.targets.iterator();

         while(var4.hasNext()) {
            Entity entity = (Entity)var4.next();
            if(this.isValidTarget(entity) && mc.thePlayer.isUsingItem() && mc.thePlayer.getCurrentEquippedItem().getItem() != null && mc.thePlayer.getCurrentEquippedItem().getItem() instanceof ItemBow) {
               int bowCurrentCharge = mc.thePlayer.getItemInUseDuration();
               float bowVelocity = (float)bowCurrentCharge / 20.0F;
               bowVelocity = (bowVelocity * bowVelocity + bowVelocity * 2.0F) / 3.0F;
               if((double)bowVelocity < 0.1D) {
                  return;
               }

               if(bowVelocity > 1.0F) {
                  bowVelocity = 1.0F;
               }

               double xDistance = entity.posX - mc.thePlayer.posX;
               double zDistance = entity.posZ - mc.thePlayer.posZ;
               double eyeDistance = entity.posY + (double)entity.getEyeHeight() - (mc.thePlayer.posY + (double)mc.thePlayer.getEyeHeight());
               double trajectoryXZ = Math.sqrt(xDistance * xDistance + zDistance * zDistance);
               Math.sqrt(trajectoryXZ * trajectoryXZ + eyeDistance * eyeDistance);
               float trajectoryTheta90 = (float)(Math.atan2(zDistance, xDistance) * 180.0D / 3.141592653589793D) - 90.0F;
               float bowTrajectory = -this.getTrajectoryAngleSolutionLow((float)trajectoryXZ, (float)eyeDistance, bowVelocity);
               sent.setYaw(trajectoryTheta90);
               sent.setPitch(bowTrajectory);
               if(!((Boolean)this.silent.getValue()).booleanValue()) {
                  mc.thePlayer.rotationYaw = trajectoryTheta90;
                  mc.thePlayer.rotationPitch = bowTrajectory;
               }
            } else {
               this.targets.remove(entity);
            }
         }
      } else if(event instanceof EventPacketSent) {
    	  EventPacketSent sent1 = (EventPacketSent)event;
         if(sent1.getPacket() instanceof C03PacketPlayer) {
            C03PacketPlayer entity1 = (C03PacketPlayer)sent1.getPacket();
         }
      }

   }

   public void onDisabled() {
      super.onDisabled();
      this.targets.clear();
   }

   private float getTrajectoryAngleSolutionLow(float angleX, float angleY, float bowVelocity) {
      float velocityIncrement = 0.006F;
      float squareRootBow = bowVelocity * bowVelocity * bowVelocity * bowVelocity - velocityIncrement * (velocityIncrement * angleX * angleX + 2.0F * angleY * bowVelocity * bowVelocity);
      return (float)Math.toDegrees(Math.atan(((double)(bowVelocity * bowVelocity) - Math.sqrt((double)squareRootBow)) / (double)(velocityIncrement * angleX)));
   }

   private void populateTargets() {
      Iterator var2 = mc.theWorld.loadedEntityList.iterator();

      while(var2.hasNext()) {
         Object o = var2.next();
         Entity entity = (Entity)o;
         if(this.isValidTarget(entity)) {
            this.targets.add(entity);
         }
      }

   }
}
