package noxious.mod.mods;

import net.minecraft.network.play.client.C03PacketPlayer;
import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Event;
import noxious.event.events.EventEntityStep;
import noxious.event.events.EventPacketSent;
import noxious.event.events.EventPreSendMotionUpdates;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.util.BlockHelper;
import noxious.util.Logger;
import noxious.value.ClampedValue;
import noxious.value.Value;

public class Step extends Mod {
	private final Value<Mode> currentMode = new Value<>("step_mode", Mode.OLD);
	private final ClampedValue<Float> height = new ClampedValue<>("step_height", 1.0646F, 0.5F, 10.0F);
	private boolean editingPackets;
    private int delay;


   public Step() {
	   super("Step", 0x00BFFF, ModManager.Category.MOVEMENT);
	   Noxious.getCommandManager().getContents().add(new Command("step", "<height/mode>", new String[0]) {
		   public void run(String message) {
			   if(message.split(" ")[1].equalsIgnoreCase("height")) {
				   String newHeightString = message.split(" ")[2];
				   try {
                       float newHeight = message.split(" ")[2].equalsIgnoreCase("-d") ? height.getDefaultValue() : Float.parseFloat(newHeightString);
                       height.setValue(newHeight);
                       if (height.getValue() > height.getMax())
                           height.setValue(height.getMax());
                       else if (height.getValue() < height.getMin())
                           height.setValue(height.getMin());

                       mc.thePlayer.stepHeight = height.getValue();
                       Logger.logChat(String.format("Step Height set to %s", height.getValue()));
                   } catch (NumberFormatException e) {
                	   Logger.logChat(String.format("\"%s\" is not a number.", newHeightString));
                   }
			   } else if(message.split(" ")[1].equalsIgnoreCase("mode")) {
				   if (message.split(" ")[2].equalsIgnoreCase("new")) {
					   currentMode.setValue(Mode.NEW);
					   Logger.logChat(String.format("Step Mode set to: %s", currentMode.getValue().getName()));
				   } else if (message.split(" ")[2].equalsIgnoreCase("old")) {
					   currentMode.setValue(Mode.OLD);
					   Logger.logChat(String.format("Step Mode set to: %s", currentMode.getValue().getName()));
				   } else if (message.split(" ")[2].equalsIgnoreCase("jump")) {
					   currentMode.setValue(Mode.JUMP);
					   Logger.logChat(String.format("Step Mode set to: %s", currentMode.getValue().getName()));
				   } else if (message.split(" ")[2].equalsIgnoreCase("-d")) {
					   currentMode.setValue(currentMode.getDefaultValue());
					   Logger.logChat(String.format("Step Mode set to: %s", currentMode.getValue().getName()));
				   }
			   }else {
				   Logger.logChat("Option not valid! Available options: height, mode.");
			   }
		   }
	   });
   }

   public void onEnabled() {
      super.onEnabled();
      if(mc.thePlayer != null) {
         mc.thePlayer.stepHeight = ((Float)this.height.getValue()).floatValue();
      }

   }

   public void onDisabled() {
      super.onDisabled();
      editingPackets = false;
      if(mc.thePlayer != null) {
         mc.thePlayer.stepHeight = 0.5F;
      }

   }

   public void onEvent(Event event) {
	   if (event instanceof EventEntityStep) {
		   EventEntityStep step = (EventEntityStep)event;
		   if (mc.thePlayer == null || !step.canStep())
	            return;
		   if (step.getEntity() != mc.thePlayer)
	            return;
		   switch (currentMode.getValue()) {
           case OLD:
               mc.thePlayer.stepHeight = height.getValue();

               editingPackets = !BlockHelper.isInLiquid(mc.thePlayer);
               step.setCancelled(!editingPackets);
               break;
           case NEW:
               mc.thePlayer.stepHeight = delay == 0 ? height.getValue() : 0.5F;

               double yDifference = mc.thePlayer.posY - mc.thePlayer.lastTickPosY;
               boolean yDiffCheck = yDifference == 0.0D;

               if (delay == 0 && yDiffCheck && step.canStep()) {

                   mc.getNetHandler().getNetworkManager().sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY + 0.41, mc.thePlayer.posZ, true));
                   mc.getNetHandler().getNetworkManager().sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.posY + 0.75, mc.thePlayer.posZ, true));

                   editingPackets = !BlockHelper.isInLiquid(mc.thePlayer);
                   step.setCancelled(!editingPackets);
                   delay = 5;
               } else {
                   mc.thePlayer.stepHeight = 0.5F;
               }
               break;
           default:
               mc.thePlayer.stepHeight = 0.5F;

               editingPackets = !BlockHelper.isInLiquid(mc.thePlayer);
               break;
		   }
	   }

	   if (event instanceof EventPacketSent) {
		   EventPacketSent packet = (EventPacketSent) event;
		   if (packet.getPacket() instanceof C03PacketPlayer) {
			   C03PacketPlayer player = (C03PacketPlayer)packet.getPacket();
			   if (!player.isMoving())
	               return;
	           if (editingPackets && currentMode.getValue() == Mode.OLD) {
	               if (mc.thePlayer.posY - mc.thePlayer.lastTickPosY >= 0.75D)
	                   player.setY(player.getY() + 0.0646D);
	               editingPackets = false;
	           } else if (currentMode.getValue() == Mode.JUMP) {
	               if (mc.thePlayer.isCollidedHorizontally && mc.thePlayer.onGround) {
	                   mc.thePlayer.motionY = 0.368125F;
	                   mc.thePlayer.isAirBorne = true;
	               }
	               editingPackets = false;
	           } else if (currentMode.getValue() == Mode.NEW) {
	        	   if (mc.thePlayer.onGround)
	                   editingPackets = false;
	               if (editingPackets)
	            	   ((EventPacketSent)event).setCancelled(true);
	           }
		   }
	   }

	   if (event instanceof EventPreSendMotionUpdates) {
		   if (delay > 0)
               delay--;
	   }
   }

   public boolean isEditingPackets() {
       return editingPackets;
   }

   private enum Mode {
       NEW, OLD, JUMP;

       public String getName() {
           String prettyName = "";
           String[] actualNameSplit = name().split("_");
           if (actualNameSplit.length > 0) {
               for (String arg : actualNameSplit) {
                   arg = arg.substring(0, 1).toUpperCase() + arg.substring(1, arg.length()).toLowerCase();
                   prettyName += arg + " ";
               }
           } else {
               prettyName = actualNameSplit[0].substring(0, 1).toUpperCase() + actualNameSplit[0].substring(1, actualNameSplit[0].length()).toLowerCase();
           }
           return prettyName.trim();
       }
   }
}
