package noxious.threads.event;

public interface ThreadEventListener {
	void onEvent(ThreadEvent event);
}
