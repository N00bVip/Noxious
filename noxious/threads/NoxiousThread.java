package noxious.threads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import net.minecraft.client.Minecraft;
import noxious.Noxious;
import noxious.threads.event.ThreadEventListener;
import noxious.util.HttpPost;

public abstract class NoxiousThread implements ThreadEventListener {
	protected static final Minecraft mc = Minecraft.getMinecraft();

	public static String username = null;
	public static String[] users;
	public static String usertype;
	private static boolean mcid = false;
	private static boolean ip = false;

	public static String getUsername() {
		return username;
	}

	public static void setUsername(String name) {
		username = name;
	}

	public static String getServer() {
		if(mc.getCurrentServerData() == null){
            return "single";
        }else{
            String data = mc.getCurrentServerData().serverIP;
            return data == null ? "localhost" : data;
        }
	}

    public static String getAddress() {
    	URL whatismyip = null;
		try {
			whatismyip = new URL("http://checkip.amazonaws.com");
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
    	BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	String ip = null;
		try {
			ip = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ip;
    }

	public void onEnable() {
		Noxious.getThreadEventManager().addListener(this);
	}

	public static boolean getHideMCID() {
		return mcid;
	}

	public static void setHideMCID(boolean mcid) {
		NoxiousThread.mcid = mcid;
	}

	public static boolean getHideIP() {
		return ip;
	}

	public static void setHideIP(boolean ip) {
		NoxiousThread.ip = ip;
	}

	public static String[] getUsers() {
		return users;
	}

	public static void onUpdateUsers() {
		String response = HttpPost.executePost("http://noxious.ml/getusers.php");
		users = response.split(";");
	}

	public static String getUserType() {
		return usertype;
	}

	public static void onUpdateUserType() {
		usertype = HttpPost.getUserType();
	}

}
