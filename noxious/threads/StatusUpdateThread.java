package noxious.threads;

import java.util.HashMap;
import java.util.Map;

import noxious.threads.event.ThreadEvent;
import noxious.util.HttpPost;

public class StatusUpdateThread extends NoxiousThread implements Runnable {

	public void run(){
		this.onEnable();
		while(true) {
			if (getUsername() != null) {
				Map<String, String>param = new HashMap<String, String>();
				param.put("username", getUsername());
				if(getHideIP()) {
					param.put("currentserver", "single");
					param.put("currentaddress", "192.168.1.1");
				} else {
					param.put("currentserver", getServer());
					param.put("currentaddress", getAddress());
				}

				if (getHideMCID()) {
					param.put("currentaccount", "Player");
				} else {
					param.put("currentaccount", mc.session.getUsername());
				}
				String response = HttpPost.executePost("http://noxious.ml/updatestatus.php", param);
			}
			try {
				Thread.sleep(1000 * 60 * 5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onEvent(ThreadEvent event) {
		if (getUsername() != null) {
			Map<String, String>param = new HashMap<String, String>();
			param.put("username", getUsername());
			if(getHideIP()) {
				param.put("currentserver", "single");
				param.put("currentaddress", "192.168.1.1");
			} else {
				param.put("currentserver", getServer());
				param.put("currentaddress", getAddress());
			}

			if (getHideMCID()) {
				param.put("currentaccount", "Player");
			} else {
				param.put("currentaccount", mc.session.getUsername());
			}
			String response = HttpPost.executePost("http://noxious.ml/updatestatus.php", param);
		}
	}
}
