package noxious.threads;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import noxious.Noxious;
import noxious.threads.event.ThreadEvent;
import noxious.threads.event.events.EventStatusUpdate;
import noxious.ui.screens.GuiNoxiousMainMenu;
import noxious.util.HttpPost;

public class NoxiousLogin extends NoxiousThread{

	public static  void onLogin(String user, String password) throws IOException {
		Map<String, String>param = new HashMap<String, String>();
		param.put("username", user);
		param.put("password", password);
		param.put("hwuuid", Noxious.getHardwareConfirmation().getUUID());
		String response = HttpPost.executePost("http://noxious.ml/checkauth.php", param);
		String[] split = response.split(",");

		if (split[1].equals("true")) {
			setUsername(user);
			Noxious.onStartup1();
			Noxious.getThreadEventManager().call(new EventStatusUpdate());
			Minecraft.getMinecraft().displayGuiScreen(new GuiNoxiousMainMenu());
			onUpdateUserType();
			onUpdateUsers();
		}
	}

	public void onEvent(ThreadEvent event) {
	}
}
