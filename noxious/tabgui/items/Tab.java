package noxious.tabgui.items;

import java.util.ArrayList;

import noxious.mod.Mod;
import noxious.tabgui.TabGui;
import noxious.util.NahrFont;
import noxious.util.RenderHelper;


public class Tab {

   private TabGui gui;
   public ArrayList<Mod> hacks;
   public String tabName;
   public int selectedItem = 0;
   public int menuHeight = 0;
   public int menuWidth = 0;
   private int colour;


   public Tab(TabGui GUI, String TabName) {
      this.tabName = TabName;
      this.gui = GUI;
      this.hacks = new ArrayList();
   }

   public void countMenuSize() {
      int maxWidth = 0;

      for(int i = 0; i < this.hacks.size(); ++i) {
         if(RenderHelper.getNahrFont().getStringWidth(((Mod)this.hacks.get(i)).getName() + 4) > (float)maxWidth) {
            maxWidth = (int)(RenderHelper.getNahrFont().getStringWidth(((Mod)this.hacks.get(i)).getName()) + 7.0F);
         }
      }

      this.menuWidth = maxWidth;
      this.menuHeight = this.hacks.size() * this.gui.tabHeight - 1;
   }

   public void drawTabMenu(int x, int y) {
      this.countMenuSize();
      x += 2;
      y += 2;
      RenderHelper.drawBorderedRect((float)(x - 2), (float)(y - 2), (float)(x + this.menuWidth - 5), (float)(y + this.menuHeight), 1.0F, -1157627904, -1879048192);

      for(int i = 0; i < this.hacks.size(); ++i) {
         this.colour = -1;
         RenderHelper.getNahrFont().drawString((i == TabGui.selectedItem?"\u00a76":(((Mod)this.hacks.get(i)).isEnabled()?"":this.gui.colorNormal)) + ((Mod)this.hacks.get(i)).getName(), (float)x, (float)(y + this.gui.tabHeight * i - 4), NahrFont.FontType.OUTLINE_THIN, -1, -16777216);
      }

   }
}
