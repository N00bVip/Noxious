package noxious.tabgui;

import java.util.ArrayList;
import java.util.Iterator;

import net.minecraft.client.Minecraft;
import noxious.Noxious;
import noxious.management.managers.ModManager;
import noxious.mod.Mod;
import noxious.tabgui.items.Tab;
import noxious.util.NahrFont;
import noxious.util.RenderHelper;

public class TabGui {

   public String colorNormal = "\u00a77";
   public int guiWidth;
   public int guiHeight = 0;
   public int tabHeight = 12;
   public static int selectedTab = 0;
   public static int selectedItem = 0;
   public static boolean mainMenu = true;
   public static ArrayList tabsList;
   private Minecraft mc;


   public TabGui(Minecraft minecraft) {
      this.mc = minecraft;
      tabsList = new ArrayList();
      Tab tabPlayer = new Tab(this, "Exploits");
      Iterator tabMovement = Noxious.getModManager().getContents().iterator();

      while(tabMovement.hasNext()) {
         Mod tabMove = (Mod)tabMovement.next();
         if(tabMove.getCategory() == ModManager.Category.EXPLOITS) {
            tabPlayer.hacks.add(tabMove);
         }
      }

      tabsList.add(tabPlayer);
      Tab tabMove1 = new Tab(this, "Movement");
      Iterator tabWorld = Noxious.getModManager().getContents().iterator();

      while(tabWorld.hasNext()) {
         Mod tabMovement1 = (Mod)tabWorld.next();
         if(tabMovement1.getCategory() == ModManager.Category.MOVEMENT) {
            tabMove1.hacks.add(tabMovement1);
         }
      }

      tabsList.add(tabMove1);
      Tab tabMovement2 = new Tab(this, "Player");
      Iterator tabRender = Noxious.getModManager().getContents().iterator();

      while(tabRender.hasNext()) {
         Mod tabWorld1 = (Mod)tabRender.next();
         if(tabWorld1.getCategory() == ModManager.Category.PLAYER) {
            tabMovement2.hacks.add(tabWorld1);
         }
      }

      tabsList.add(tabMovement2);
      Tab tabWorld2 = new Tab(this, "World");
      Iterator tabCombat = Noxious.getModManager().getContents().iterator();

      while(tabCombat.hasNext()) {
         Mod tabRender1 = (Mod)tabCombat.next();
         if(tabRender1.getCategory() == ModManager.Category.WORLD) {
            tabWorld2.hacks.add(tabRender1);
         }
      }

      tabsList.add(tabWorld2);
      Tab tabRender2 = new Tab(this, "Render");
      Iterator tabInvisible = Noxious.getModManager().getContents().iterator();

      while(tabInvisible.hasNext()) {
         Mod tabCombat1 = (Mod)tabInvisible.next();
         if(tabCombat1.getCategory() == ModManager.Category.RENDER) {
            tabRender2.hacks.add(tabCombat1);
         }
      }

      tabsList.add(tabRender2);
      Tab tabCombat2 = new Tab(this, "Combat");
      Iterator module = Noxious.getModManager().getContents().iterator();

      while(module.hasNext()) {
         Mod tabInvisible1 = (Mod)module.next();
         if(tabInvisible1.getCategory() == ModManager.Category.COMBAT) {
            tabCombat2.hacks.add(tabInvisible1);
         }
      }

      tabsList.add(tabCombat2);
      Tab tabInvisible2 = new Tab(this, "HUD");
      Iterator var10 = Noxious.getModManager().getContents().iterator();

      while(var10.hasNext()) {
         Mod module1 = (Mod)var10.next();
         if(module1.getCategory() == ModManager.Category.HUD) {
            tabInvisible2.hacks.add(module1);
         }
      }

      tabsList.add(tabInvisible2);
      this.guiHeight = this.tabHeight + tabsList.size() * this.tabHeight;
   }

   public void drawGui(int posY, int posX, int width) {
      int x = posY;
      this.guiWidth = width;
      RenderHelper.drawBorderedRect((float)(posY - 1), (float)(posX - 1), (float)(posY + this.guiWidth), (float)(posX + this.guiHeight - 12), 1.0F, -1157627904, -1879048192);
      int yOff = posX + 1;
      int[] var10000 = new int[]{'\u8833', -13369498, -6723841, -10040065, -43691, '\uff66'};

      for(int i = 0; i < tabsList.size(); ++i) {
         NahrFont var9 = RenderHelper.getNahrFont();
         float var10002 = (float)(x + 2);
         float var10003 = (float)(yOff - 4);
         int var10005 = i == selectedTab?-1:-5658199;
         var9.drawString(((Tab)tabsList.get(i)).tabName, var10002, var10003, NahrFont.FontType.OUTLINE_THIN, var10005, -16777216);
         if(i == selectedTab && !mainMenu) {
            ((Tab)tabsList.get(i)).drawTabMenu(x + this.guiWidth + 2, yOff - 2);
         }

         yOff += this.tabHeight;
      }

   }

   public static void parseKeyUp() {
      if(mainMenu) {
         --selectedTab;
         if(selectedTab < 0) {
            selectedTab = tabsList.size() - 1;
         }
      } else {
         --selectedItem;
         if(selectedItem < 0) {
            selectedItem = ((Tab)tabsList.get(selectedTab)).hacks.size() - 1;
         }
      }

   }

   public static void parseKeyDown() {
      if(mainMenu) {
         ++selectedTab;
         if(selectedTab > tabsList.size() - 1) {
            selectedTab = 0;
         }
      } else {
         ++selectedItem;
         if(selectedItem > ((Tab)tabsList.get(selectedTab)).hacks.size() - 1) {
            selectedItem = 0;
         }
      }

   }

   public static void parseKeyLeft() {
      if(!mainMenu) {
         mainMenu = true;
      }

   }

   public static void parseKeyRight() {
      if(mainMenu) {
         mainMenu = false;
         selectedItem = 0;
      }

   }

   public static void parseKeyToggle() {
      if(!mainMenu) {
         int sel = selectedItem;
         ((Mod)((Tab)tabsList.get(selectedTab)).hacks.get(sel)).toggle();
      }

   }
}
