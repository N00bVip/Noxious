package noxious;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

import org.lwjgl.opengl.Display;

import net.minecraft.client.Minecraft;
import noxious.command.Command;
import noxious.irc.IRC;
import noxious.irc.NoxiousIRCBot;
import noxious.management.managers.AltManager;
import noxious.management.managers.CommandManager;
import noxious.management.managers.EventManager;
import noxious.management.managers.FileManager;
import noxious.management.managers.FriendManager;
import noxious.management.managers.ModManager;
import noxious.management.managers.ThreadEventManager;
import noxious.management.managers.ValueManager;
import noxious.mod.Mod;
import noxious.protection.Protection;
import noxious.tabgui.TabGui;
import noxious.threads.StatusUpdateThread;
import noxious.ui.screens.GuiNoxiousLoginMenu;
import noxious.util.HardwareConfirmation;
import noxious.util.ListenerUtil;
import noxious.util.Logger;
import noxious.wallhack.WallhackManager;

public final class Noxious
{
	private static boolean newerVersionAvailable;
	private static String version = "1.1";
	private static String client_Name = "Noxious";
	private static EventManager eventManager = new EventManager();
	private static ThreadEventManager threadEventManager = new ThreadEventManager();
	private static ModManager modManager = new ModManager();
	private static final File directory = new File(Minecraft.getMinecraft().mcDataDir, getClientName());
	private static final FileManager fileManager = new FileManager();
    private static final ValueManager valueManager = new ValueManager();
	private static final FriendManager friendManager = new FriendManager();
	private static final CommandManager commandManager = new CommandManager();
	private static final AltManager altManager = new AltManager();
	private static final WallhackManager wallManager = new WallhackManager();
	private static final HardwareConfirmation hardwareConfirmation = new HardwareConfirmation();
	private static TabGui tabGUI;
	private static ListenerUtil listenerUtil;
	private static NoxiousIRCBot bot;

	public static NoxiousIRCBot getBot() {
		return bot;
	}

	public static void setBot(String username) {
		bot = new NoxiousIRCBot(username);
	}

	public static ListenerUtil getListenerUtil() {
		return listenerUtil;
	}

    public static AltManager getAltManager() {
        return altManager;
    }

    public static String getVersion() {
        return version;
    }

    public static String getClientName() {
        return client_Name;
    }

    public static CommandManager getCommandManager() {
        return commandManager;
    }

    public static File getDirectory() {
        return directory;
    }

    public static EventManager getEventManager() {
        return eventManager;
    }

    public static ThreadEventManager getThreadEventManager() {
        return threadEventManager;
    }

    public static FileManager getFileManager() {
        return fileManager;
    }

    public static FriendManager getFriendManager() {
        return friendManager;
    }

    public static ModManager getModManager() {
        return modManager;
    }

    public static ValueManager getValueManager() {
        return valueManager;
    }

    public static HardwareConfirmation getHardwareConfirmation() {
        return hardwareConfirmation;
    }

    public static final TabGui getTabGUI() {
        return tabGUI;
    }

    public static void onStartup() throws IOException {
        Display.setTitle("Noxious for Minecraft 1.8");
        Logger.logConsole("Version " + getVersion());
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        HardwareConfirmation.onGenerateUUID();
        Protection.setup();
        Logger.logConsole("Waiting Login...");
        Minecraft.getMinecraft().displayGuiScreen(new GuiNoxiousLoginMenu());
    }

    public static void  onStartup1() throws IOException {
    	Logger.logConsole("Started loading Noxious");
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        threadEventManager.setup();
        eventManager.setup();
        valueManager.setup();
        friendManager.setup();
        commandManager.setup();
        modManager.setup();
        altManager.setup();
        fileManager.setup();
        tabGUI = new TabGui(Minecraft.getMinecraft());
        Collections.sort(modManager.getContents(), new Comparator() {
            public int compare(final Mod mod1, final Mod mod2) {
                return mod1.getName().compareTo(mod2.getName());
            }

            @Override
            public int compare(final Object var1, final Object var2) {
                return this.compare((Mod)var1, (Mod)var2);
            }
        });
        Collections.sort(commandManager.getContents(), new Comparator() {
            public int compare(final Command mod1, final Command mod2) {
                return mod1.getCommand().compareTo(mod2.getCommand());
            }

            @Override
            public int compare(final Object var1, final Object var2) {
                return this.compare((Command)var1, (Command)var2);
            }
        });
        Logger.logConsole("Successfully loadedNoxious");
        HardwareConfirmation.onGenerateUUID();
        new Thread(new StatusUpdateThread()).start();
        IRC.onStartup();
    }
}
