package noxious.file.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import noxious.Noxious;
import noxious.file.CustomFile;
import noxious.mod.mods.Spammer;

public final class SpamMessages extends CustomFile {
   public SpamMessages() {
      super("spam");
   }

   public void loadFile() {
      try {
         Spammer var5 = (Spammer)Noxious.getModManager().getModByName("spammer");
         if(var5 == null) {
            return;
         }

         BufferedReader reader = new BufferedReader(new FileReader(this.getFile()));

         String line;
         while((line = reader.readLine()) != null) {
            if(!line.equals("")) {
               line.replaceAll("\r", "");
               line.replaceAll("\n", "");
               var5.getMessages().add(line);
            }
         }

         reader.close();
      } catch (FileNotFoundException var4) {
         var4.printStackTrace();
      } catch (IOException var51) {
         var51.printStackTrace();
      }

   }

   public void saveFile() {
      try {
         BufferedWriter var2 = new BufferedWriter(new FileWriter(this.getFile()));
         var2.close();
      } catch (IOException var21) {
         var21.printStackTrace();
      }

   }
}
