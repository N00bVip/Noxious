package noxious.file.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import noxious.Noxious;
import noxious.event.events.EventChatSent;
import noxious.file.CustomFile;

public final class AutoExec extends CustomFile {
   public AutoExec() {
      super("autoexec");
   }

   public void loadFile() {
      try {
         BufferedReader var5 = new BufferedReader(new FileReader(this.getFile()));

         String line;
         while((line = var5.readLine()) != null) {
            EventChatSent sent = new EventChatSent("." + line);
            Noxious.getEventManager().call(sent);
            sent.checkForCommands();
         }

         var5.close();
      } catch (FileNotFoundException var4) {
         var4.printStackTrace();
      } catch (IOException var51) {
         var51.printStackTrace();
      }

   }

   public void saveFile() {
      try {
         BufferedWriter var2 = new BufferedWriter(new FileWriter(this.getFile()));
         var2.close();
      } catch (IOException var21) {
         var21.printStackTrace();
      }

   }
}
