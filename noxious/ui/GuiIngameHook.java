package noxious.ui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import noxious.Noxious;
import noxious.event.events.EventDrawScreen;

public class GuiIngameHook extends GuiIngame {
   public GuiIngameHook(Minecraft mcIn) {
      super(mcIn);
   }

   public static void renderUI() {
	  Noxious.getEventManager().call(new EventDrawScreen());
   }
}
