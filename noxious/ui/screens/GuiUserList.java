package noxious.ui.screens;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import noxious.Noxious;
import noxious.threads.AltLoginThread;
import noxious.threads.NoxiousThread;
import noxious.util.Alt;
import noxious.util.HttpPost;
import noxious.util.RenderHelper;

public class GuiUserList extends GuiScreen {
   private GuiButton login;
   private GuiButton mcid;
   private GuiButton ip;
   private AltLoginThread loginThread;
   private int offset;
   public String selectedUser = null;
   public String[] selectedUserData = null;
   String[] users = null;
   String userType = null;
   boolean connect = false;

   public GuiUserList() {
   }

   public void actionPerformed(GuiButton button) {
      switch(button.id) {
      case 0:
    	 String[] port = selectedUserData[2].split(":");
    	 if (NoxiousThread.getAddress().equals(selectedUserData[3])) {
    		 mc.setServerData(new ServerData("Direct Login", selectedUserData[2]));
	    	 this.mc.displayGuiScreen(new GuiConnecting(this, mc, mc.getCurrentServerData()));
    	 } else {
	    	 mc.setServerData(new ServerData("Direct Login", selectedUserData[2].startsWith("localhost") ? selectedUserData[3] : selectedUserData[2]));
	    	 this.mc.displayGuiScreen(new GuiConnecting(this, mc, mc.getCurrentServerData()));
    	 }
         break;
      case 1:
    	 getUsers();
    	 userType = HttpPost.getUserType();
         break;
      case 2:
    	  this.mc.displayGuiScreen((GuiScreen)null);
    	  break;
      case 3:
    	  NoxiousThread.setHideIP(!NoxiousThread.getHideIP());
    	  checkBolean();
    	  break;
      case 4:
    	  NoxiousThread.setHideMCID(!NoxiousThread.getHideMCID());
    	  checkBolean();
    	  break;
      }
   }

   public void drawScreen(int par1, int par2, float par3) {
      int y = 0;
      if(Mouse.hasWheel()) {
         y = Mouse.getDWheel();
         if(y < 0) {
            this.offset += 36;
            if(this.offset < 0) {
               this.offset = 0;
            }
         } else if(y > 0) {
            this.offset -= 36;
            if(this.offset < 0) {
               this.offset = 0;
            }
         }
      }

      this.drawDefaultBackground();
      this.drawString(this.fontRendererObj, NoxiousThread.getUsername(), 10, 10, -7829368);
      this.drawCenteredString(this.fontRendererObj, "Online Users - " + users.length + " Users", this.width / 2, 10, -1);
      RenderHelper.drawBorderedRect(50.0F, 33.0F, (float)(this.width - 50), (float)(this.height - 50), 1.0F, -16777216, Integer.MIN_VALUE);
      GL11.glPushMatrix();
      this.prepareScissorBox(0.0F, 33.0F, (float)this.width, (float)(this.height - 50));
      GL11.glEnable(3089);
      Iterator var6 = Noxious.getAltManager().getContents().iterator();
      y = 38;

      for ( int i = 0; i < users.length; i++ ) {
       	  String[] split = users[i].split(",");

       	  if(split[0].equals(selectedUser)) {
                 if(this.isMouseOverAlt(par1, par2, y - this.offset) && Mouse.isButtonDown(0)) {
                    RenderHelper.drawBorderedRect(52.0F, (float)(y - this.offset - 4), (float)(this.width - 52), (float)(y - this.offset + 30), 1.0F, -16777216, -2142943931);
                 } else if(this.isMouseOverAlt(par1, par2, y - this.offset)) {
                    RenderHelper.drawBorderedRect(52.0F, (float)(y - this.offset - 4), (float)(this.width - 52), (float)(y - this.offset + 30), 1.0F, -16777216, -2142088622);
                 } else {
                    RenderHelper.drawBorderedRect(52.0F, (float)(y - this.offset - 4), (float)(this.width - 52), (float)(y - this.offset + 30), 1.0F, -16777216, -2144259791);
                 }
              } else if(this.isMouseOverAlt(par1, par2, y - this.offset) && Mouse.isButtonDown(0)) {
                 RenderHelper.drawBorderedRect(52.0F, (float)(y - this.offset - 4), (float)(this.width - 52), (float)(y - this.offset + 30), 1.0F, -16777216, -2146101995);
              } else if(this.isMouseOverAlt(par1, par2, y - this.offset)) {
                 RenderHelper.drawBorderedRect(52.0F, (float)(y - this.offset - 4), (float)(this.width - 52), (float)(y - this.offset + 30), 1.0F, -16777216, -2145180893);
              }

       	  if (split[1].equals("1")) {
       		  this.drawCenteredString(this.fontRendererObj, "[§6Vip§f] " + split[0].toString(), this.width / 2, y - this.offset, -1);
       	  }  else if (split[1].equals("2")) {
       		  this.drawCenteredString(this.fontRendererObj, "[§cAdmin§f] " + split[0].toString(), this.width / 2, y - this.offset, -1);
       	  }  else if (split[1].equals("3")) {
       		  this.drawCenteredString(this.fontRendererObj, "[§bDev§f] " + split[0].toString(), this.width / 2, y - this.offset, -1);
       	  }  else if (split[1].equals("4")) {
       		  this.drawCenteredString(this.fontRendererObj, "[§aNoxious§f] " + split[0].toString(), this.width / 2, y - this.offset, -1);
       	  } else {
       		  this.drawCenteredString(this.fontRendererObj, split[0].toString(), this.width / 2, y - this.offset, -1);
       	  }
       	  String[] port = split[2].split(":");
       	  if (userType.equals("0") && split[2].startsWith("localhost") || split[2].equals("single")) {
       		  this.drawCenteredString(this.fontRendererObj, "Not Connected", this.width / 2, y - this.offset + 10, 5592405);
       	  } else if (!userType.equals("0") && split[2].startsWith("localhost")) {
       		  this.drawCenteredString(this.fontRendererObj, split[3], this.width / 2, y - this.offset + 10, 5592405);
       	  } else {
       		  this.drawCenteredString(this.fontRendererObj, split[2], this.width / 2, y - this.offset + 10, 5592405);
       	  }

       	  this.drawCenteredString(this.fontRendererObj, split[4], this.width / 2, y - this.offset + 21, 5592405);
       	  y += 36;
      }

      while(true) {
         Alt alt;
         do {
            if(!var6.hasNext()) {
               GL11.glDisable(3089);
               GL11.glPopMatrix();
               super.drawScreen(par1, par2, par3);
               if(this.selectedUser != null && connect) {
                   this.login.enabled = true;
                } else {
                   this.login.enabled = false;
                }

               if(Keyboard.isKeyDown(200)) {
                  this.offset -= 36;
                  if(this.offset < 0) {
                     this.offset = 0;
                  }
               } else if(Keyboard.isKeyDown(208)) {
                  this.offset += 36;
                  if(this.offset < 0) {
                     this.offset = 0;
                  }
               }

               return;
            }

            alt = (Alt)var6.next();
         } while(!this.isAltInArea(y));
      }
   }

   public void initGui() {
	  getUsers();
	  userType = HttpPost.getUserType();
	  if (!userType.equals("0")) {
		  this.buttonList.add(this.mcid = new GuiButton(4, this.width / 2 - 154, this.height - 48, 150, 20, "Hide MCID: " + (NoxiousThread.getHideMCID() ? "ON" : "OFF")));
		  this.buttonList.add(this.ip = new GuiButton(3, this.width / 2 + 4, this.height - 48, 150, 20, "Hide Connected: " + (NoxiousThread.getHideIP() ? "ON" : "OFF")));
	  }
	  this.buttonList.add(this.login = new GuiButton(0, this.width / 2 - 154, this.height - 24, 100, 20, "Login"));
      this.buttonList.add(new GuiButton(1, this.width / 2 - 50, this.height - 24, 100, 20, "Reload"));
      this.buttonList.add(new GuiButton(2, this.width / 2 + 4 + 50, this.height - 24, 100, 20, "Cancel"));
      this.login.enabled = false;
   }

   public void checkBolean() {

	   Map<String, String>param = new HashMap<String, String>();
	   param.put("username", NoxiousThread.getUsername());

	   this.mcid.displayString = "Hide MCID: ";

	   if (NoxiousThread.getHideMCID()) {
           this.mcid.displayString = this.mcid.displayString + "ON";
           param.put("currentaccount", "Player");
       } else {
           this.mcid.displayString = this.mcid.displayString + "OFF";
           param.put("currentaccount", mc.session.getUsername());
       }

	   this.ip.displayString = "Hide Connected: ";

	   if (NoxiousThread.getHideIP()) {
           this.ip.displayString = this.ip.displayString + "ON";
           param.put("currentserver", "single");
           param.put("currentaddress", "192.168.1.1");
       } else {
           this.ip.displayString = this.ip.displayString + "OFF";
           param.put("currentserver", NoxiousThread.getServer());
           param.put("currentaddress", NoxiousThread.getAddress());
       }

	   String response = HttpPost.executePost("http://noxious.ml/updatestatus.php", param);
   }

   private boolean isAltInArea(int y) {
      return y - this.offset <= this.height - 70;
   }

   private boolean isMouseOverAlt(int x, int y, int y1) {
      return x >= 52 && y >= y1 - 4 && x <= this.width - 52 && y <= y1 + 30 && x >= 0 && y >= 33 && x <= this.width && y <= this.height - 50;
   }

   protected void mouseClicked(int par1, int par2, int par3) {
      if(this.offset < 0) {
         this.offset = 0;
      }

      int y = 38 - this.offset;

      for ( int i = 0; i < users.length; i++ , y += 36) {
         String[] user = users[i].split(",");
         String e = user[0];
         if(this.isMouseOverAlt(par1, par2, y)) {
            if(e == this.selectedUser) {
               this.actionPerformed((GuiButton)this.buttonList.get(0));
               return;
            }

            if (userType.equals("0") && user[2].equals("localhost") || user[2].equals("single")) {
            	connect = false;
            } else {
            	connect = true;
            }
            this.selectedUserData = users[i].split(",");
            this.selectedUser = e;
         }
      }

      try {
         super.mouseClicked(par1, par2, par3);
      } catch (IOException var71) {
         var71.printStackTrace();
      }

   }

   public void prepareScissorBox(float x, float y, float x2, float y2) {
      ScaledResolution scale = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
      int factor = scale.getScaleFactor();
      GL11.glScissor((int)(x * (float)factor), (int)(((float)scale.getScaledHeight() - y2) * (float)factor), (int)((x2 - x) * (float)factor), (int)((y2 - y) * (float)factor));
   }

   public void getUsers() {
	   String response = HttpPost.executePost("http://noxious.ml/getusers.php");
	   users = response.split(";");
   }
}
