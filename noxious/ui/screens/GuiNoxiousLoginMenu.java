package noxious.ui.screens;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.util.ResourceLocation;
import noxious.Noxious;
import noxious.threads.NoxiousLogin;
import noxious.ui.GuiPasswordField;
import noxious.util.RenderHelper;

public final class GuiNoxiousLoginMenu extends GuiScreen {
   private GuiPasswordField password;
   private final GuiScreen previousScreen = null;
   private GuiTextField username;
   private static ResourceLocation background = new ResourceLocation("Noxious/background.png");

   public GuiNoxiousLoginMenu() {
	      Calendar var1 = Calendar.getInstance();
	      var1.setTime(new Date());
	      if(var1.get(2) + 1 == 12 && var1.get(5) == 24) {
	    	  background = new ResourceLocation("Noxious/xmasbackground.png");
	      }
   }

   protected void actionPerformed(GuiButton button) throws IOException {
      switch(button.id) {
      case 0:
         NoxiousLogin.onLogin(username.getText(), password.getText());
         break;
      }
   }

   public void drawScreen(int x, int y, float z) {
	  this.renderBackground(this.width, this.height);
      this.username.drawTextBox();
      this.password.drawTextBox();
      RenderHelper.drawBorderedRect(70.0F, 15.0F, (float)(this.width - 70), (float)(this.height - 50), 1.0F, -16777216, Integer.MIN_VALUE);
      this.drawCenteredString(this.mc.fontRendererObj, "Noxious Login", this.width / 2, 20, -1);
      if(this.username.getText().isEmpty()) {
         this.drawString(this.mc.fontRendererObj, "Username", this.width / 2 - 96,66, -7829368);
      }

      if(this.password.getText().isEmpty()) {
         this.drawString(this.mc.fontRendererObj, "Password", this.width / 2 - 96, 106, -7829368);
      }

      String var10 = "Minecraft 1.8";
      if(this.mc.isDemo()) {
         var10 = var10 + " Demo";
      }
      this.drawString(this.fontRendererObj, Noxious.getClientName() + " for " + var10, 2, this.height - 20, -1);
      this.drawString(this.fontRendererObj, "Version §a" + Noxious.getVersion(), 2, this.height - 10, -1);
      String var11 = "Coded: @N00bVip";
      this.drawString(this.fontRendererObj, "Coded: @N00bVip", this.width - this.fontRendererObj.getStringWidth("Coded: @N00bVip") - 4, this.height - 10, -1);

      super.drawScreen(x, y, z);
   }

   private void renderBackground(int par1, int par2) {
	      GlStateManager.disableDepth();
	      GlStateManager.depthMask(false);
	      GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
	      GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
	      GlStateManager.disableAlpha();
	      this.mc.getTextureManager().bindTexture(background);
	      Tessellator var2 = Tessellator.getInstance();
	      WorldRenderer var3 = var2.getWorldRenderer();
	      var3.startDrawingQuads();
	      var3.addVertexWithUV(0.0D, (double)par2, -90.0D, 0.0D, 1.0D);
	      var3.addVertexWithUV((double)par1, (double)par2, -90.0D, 1.0D, 1.0D);
	      var3.addVertexWithUV((double)par1, 0.0D, -90.0D, 1.0D, 0.0D);
	      var3.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
	      var2.draw();
	      GlStateManager.depthMask(true);
	      GlStateManager.enableDepth();
	      GlStateManager.enableAlpha();
	      GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
	   }

   public void initGui() {
      int var3 = this.height / 4 + 24;
      this.buttonList2.add(new GuiButton(0, this.width / 2 - 100, var3 + 72 + 12, "Login"));
      this.username = new GuiTextField(var3, this.mc.fontRendererObj, this.width / 2 - 100, 60, 200, 20);
      this.password = new GuiPasswordField(this.mc.fontRendererObj, this.width / 2 - 100, 100, 200, 20);
      this.username.setFocused(true);
      Keyboard.enableRepeatEvents(true);
   }

   protected void keyTyped(char character, int key) throws IOException {
	   if(key == 1) {
		   return;
	   }
      try {
         super.keyTyped(character, key);
      } catch (IOException var4) {
         var4.printStackTrace();
      }

      if(character == 9) {
         if(!this.username.isFocused() && !this.password.isFocused()) {
            this.username.setFocused(true);
         } else {
            this.username.setFocused(this.password.isFocused());
            this.password.setFocused(!this.username.isFocused());
         }
      }

      if(character == 13) {
         this.actionPerformed((GuiButton)this.buttonList2.get(0));
      }

      this.username.textboxKeyTyped(character, key);
      this.password.textboxKeyTyped(character, key);
   }

   protected void mouseClicked(int x, int y, int button) {
      try {
         super.mouseClicked(x, y, button);
      } catch (IOException var5) {
         var5.printStackTrace();
      }

      this.username.mouseClicked(x, y, button);
      this.password.mouseClicked(x, y, button);
   }

   public void onGuiClosed() {
      Keyboard.enableRepeatEvents(false);
   }

   public void updateScreen() {
      this.username.updateCursorCounter();
      this.password.updateCursorCounter();
   }
}