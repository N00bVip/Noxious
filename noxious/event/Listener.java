package noxious.event;

public interface Listener {
   void onEvent(Event var1);
}
