package noxious.event.events;

import noxious.event.Event;

public final class EventBlockClicked extends Event
{
    private final int x;
    private final int y;
    private final int z;
    private final int side;

    public EventBlockClicked(int x, int y, int z, int side)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.side = side;
    }

    public int getSide()
    {
        return this.side;
    }

    public int getX()
    {
        return this.x;
    }

    public int getY()
    {
        return this.y;
    }

    public int getZ()
    {
        return this.z;
    }
}
