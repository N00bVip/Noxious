package noxious.event.events;

import noxious.event.Event;

public class EventBlockOpacity extends Event
{
    private int blockOpacity;

    public EventBlockOpacity(int blockOpacity)
    {
        this.blockOpacity = blockOpacity;
    }

    public int getBlockOpacity()
    {
        return this.blockOpacity;
    }

    public void setBlockOpacity(int blockOpacity)
    {
        this.blockOpacity = blockOpacity;
    }
}
