package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;
import net.minecraft.network.Packet;

public final class EventPacketReceive extends Event implements Cancellable
{
    private boolean cancel;
    private Packet packet;

    public EventPacketReceive(Packet packet)
    {
        this.packet = packet;
    }

    public Packet getPacket()
    {
        return this.packet;
    }

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean cancel)
    {
        this.cancel = cancel;
    }

    public void setPacket(Packet packet)
    {
        this.packet = packet;
    }
}
