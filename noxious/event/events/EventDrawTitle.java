package noxious.event.events;

import noxious.event.Event;

public class EventDrawTitle extends Event {

	public static String title;
	public static String sub;

	public EventDrawTitle(String title, String sub) {
		this.title = title;
		this.sub = sub;
	}

	public static String getTitle() {
		return title;
	}

	public static String getSub() {
		return sub;
	}

	public static void setTitle(String title) {
		EventDrawTitle.title = title;
	}

	public static void setSub(String sub) {
		EventDrawTitle.sub = sub;
	}

}
