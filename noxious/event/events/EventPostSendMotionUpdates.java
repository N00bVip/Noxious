package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;

public final class EventPostSendMotionUpdates extends Event implements Cancellable
{
    private boolean cancel;

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean cancel)
    {
        this.cancel = cancel;
    }
}
