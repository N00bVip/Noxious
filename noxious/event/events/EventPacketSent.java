package noxious.event.events;

import net.minecraft.network.Packet;
import noxious.event.Cancellable;
import noxious.event.Event;

public final class EventPacketSent extends Event implements Cancellable
{
    private boolean cancel;
    private Packet packet;

    public EventPacketSent(Packet packet)
    {
        this.packet = packet;
    }

    public Packet getPacket()
    {
        return this.packet;
    }

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean cancel)
    {
        this.cancel = cancel;
    }

    public void setPacket(Packet packet)
    {
        this.packet = packet;
    }
}
