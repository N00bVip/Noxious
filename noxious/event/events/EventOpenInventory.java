package noxious.event.events;

import net.minecraft.client.gui.GuiHopper;
import net.minecraft.client.gui.inventory.GuiBeacon;
import net.minecraft.client.gui.inventory.GuiBrewingStand;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.client.gui.inventory.GuiDispenser;
import net.minecraft.client.gui.inventory.GuiFurnace;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.world.IInteractionObject;
import noxious.event.Cancellable;
import noxious.event.Event;

public class EventOpenInventory extends Event implements Cancellable {

	public static InventoryPlayer inventory = new InventoryPlayer(mc.thePlayer);
	private IInventory Inventory;
	private boolean cancel;

	public EventOpenInventory(IInventory Inventory) {
		this.Inventory = Inventory;
	}

	public IInventory getInventory() {
		return Inventory;
	}

	public void setInventory(IInventory Inventory) {
		this.Inventory = Inventory;
	}

	public void openInventory() {
		String name = this.Inventory instanceof IInteractionObject?((IInteractionObject)this.Inventory).getGuiID():"minecraft:container";
		if("minecraft:chest".equals(name)) {
			this.mc.displayGuiScreen(new GuiChest(this.inventory, this.Inventory));
		} else if("minecraft:hopper".equals(name)) {
			this.mc.displayGuiScreen(new GuiHopper(this.inventory, this.Inventory));
		} else if("minecraft:furnace".equals(name)) {
			this.mc.displayGuiScreen(new GuiFurnace(this.inventory, this.Inventory));
		} else if("minecraft:brewing_stand".equals(name)) {
			mc.displayGuiScreen(new GuiBrewingStand(this.inventory, this.Inventory));
		} else if("minecraft:beacon".equals(name)) {
			this.mc.displayGuiScreen(new GuiBeacon(this.inventory, this.Inventory));
		} else if(!"minecraft:dispenser".equals(name) && !"minecraft:dropper".equals(name)) {
			this.mc.displayGuiScreen(new GuiChest(this.inventory, this.Inventory));
		} else {
			this.mc.displayGuiScreen(new GuiDispenser(this.inventory, this.Inventory));
		}
	}

	public static void openInventory(IInventory Inventory) {
		String name = Inventory instanceof IInteractionObject?((IInteractionObject)Inventory).getGuiID():"minecraft:container";
		if("minecraft:chest".equals(name)) {
			mc.displayGuiScreen(new GuiChest(inventory, Inventory));
		} else if("minecraft:hopper".equals(name)) {
			mc.displayGuiScreen(new GuiHopper(inventory, Inventory));
		} else if("minecraft:furnace".equals(name)) {
			mc.displayGuiScreen(new GuiFurnace(inventory, Inventory));
		} else if("minecraft:brewing_stand".equals(name)) {
			mc.displayGuiScreen(new GuiBrewingStand(inventory, Inventory));
		} else if("minecraft:beacon".equals(name)) {
			mc.displayGuiScreen(new GuiBeacon(inventory, Inventory));
		} else if(!"minecraft:dispenser".equals(name) && !"minecraft:dropper".equals(name)) {
			mc.displayGuiScreen(new GuiChest(inventory, Inventory));
		} else {
			mc.displayGuiScreen(new GuiDispenser(inventory, Inventory));
		}
	}

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean cancel)
    {
        this.cancel = cancel;
    }

}
