package noxious.event.events;

import noxious.event.Event;

public class EventWalking extends Event {

   boolean walk = false;


   public void setSafeWalk(boolean walk) {
      this.walk = walk;
   }

   public boolean getSafeWalk() {
      return this.walk;
   }
}
