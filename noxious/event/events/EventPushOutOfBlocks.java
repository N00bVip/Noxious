package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;

public final class EventPushOutOfBlocks extends Event implements Cancellable
{
    public boolean isCancelled()
    {
        return true;
    }

    public void setCancelled(boolean cancel) {}
}
