package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;

public class EventRenderBurning extends Event implements Cancellable
{
    private boolean cancel;

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean cancel)
    {
        this.cancel = cancel;
    }
}
