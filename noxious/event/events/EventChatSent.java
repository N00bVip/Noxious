package noxious.event.events;

import java.util.Iterator;

import noxious.Noxious;
import noxious.command.Command;
import noxious.event.Cancellable;
import noxious.event.Event;
import noxious.threads.NoxiousThread;
import noxious.util.Logger;

public final class EventChatSent extends Event implements Cancellable
{
    private boolean cancel;
    private String message;

    public EventChatSent(String message)
    {
        this.message = message;
    }

    public void checkForCommands()
    {
    	if (getMessage().startsWith("$")) {
	    	if (Noxious.getBot() != null) {
	    		setCancelled(true);

				String msg = getMessage().replace("$", "");

				Noxious.getBot().send(msg);
				if (NoxiousThread.getUserType().equals("1")) {
					Logger.logIRC("§f[§6Vip§f] " + "§f<" + NoxiousThread.getUsername() + "> " + msg);
				} else if (NoxiousThread.getUserType().equals("2")) {
					Logger.logIRC("§f[§cAdmin§f] " + "§f<" + NoxiousThread.getUsername() + "> " + msg);
				} else if (NoxiousThread.getUserType().equals("3")) {
					Logger.logIRC("§f[§bDev§f] " + "§f<" + NoxiousThread.getUsername() + "> " + msg);
				} else if (NoxiousThread.getUserType().equals("4")) {
					Logger.logIRC("§f[§aNoxious§f] " + "§f<" + NoxiousThread.getUsername() + "> " + msg);
				} else {
					Logger.logIRC("§f<" + NoxiousThread.getUsername() + "> " + msg);
				}
			} else {
				Logger.logChat("IRC is disabled");
			}
    	}
    	else if (this.message.startsWith("."))
        {
            Iterator var2 = Noxious.getCommandManager().getContents().iterator();

            while (var2.hasNext())
            {
                Command command = (Command)var2.next();

                if (this.message.split(" ")[0].equalsIgnoreCase("." + command.getCommand()))
                {
                    try
                    {
                        command.run(this.message);
                    }
                    catch (Exception var9)
                    {
                        Logger.logChat("Invalid arguments! " + command.getCommand() + " " + command.getArguments());
                    }

                    this.cancel = true;
                }
                else
                {
                    String[] var6;
                    int var5 = (var6 = command.getAliases()).length;

                    for (int var4 = 0; var4 < var5; ++var4)
                    {
                        String alias = var6[var4];

                        if (this.message.split(" ")[0].equalsIgnoreCase("." + alias))
                        {
                            try
                            {
                                command.run(this.message);
                            }
                            catch (Exception var8)
                            {
                                Logger.logChat("Invalid arguments! " + alias + " " + command.getArguments());
                            }

                            this.cancel = true;
                        }
                    }
                }
            }

            if (!this.cancel)
            {
                Logger.logChat("Command \"" + this.message + "\" was not found!");
                this.cancel = true;
            }
        }
    }

    public String getMessage()
    {
        return this.message;
    }

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean cancel)
    {
        this.cancel = cancel;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
