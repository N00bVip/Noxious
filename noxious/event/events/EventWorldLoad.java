package noxious.event.events;

import noxious.event.Event;
import net.minecraft.client.multiplayer.WorldClient;

public final class EventWorldLoad extends Event
{
    private final WorldClient world;

    public EventWorldLoad(WorldClient world)
    {
        this.world = world;
    }

    public WorldClient getWorld()
    {
        return this.world;
    }
}
