package noxious.event.events;

import noxious.event.Event;

public class EventMouseClick extends Event
{
    private int button;

    public EventMouseClick(int button)
    {
        this.button = button;
    }

    public int getButton()
    {
        return this.button;
    }
}
