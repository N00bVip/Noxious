package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;

public class EventWorldBob extends Event implements Cancellable
{
    private boolean cancel;
    private float partialTicks;

    public EventWorldBob(float partialTicks)
    {
        this.partialTicks = partialTicks;
    }

    public float getPartialTicks()
    {
        return this.partialTicks;
    }

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean shouldCancel)
    {
        this.cancel = shouldCancel;
    }

    public void setPartialTicks(float partialTicks)
    {
        this.partialTicks = partialTicks;
    }
}
