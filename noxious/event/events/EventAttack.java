package noxious.event.events;

import net.minecraft.entity.Entity;
import noxious.event.Event;

public class EventAttack extends Event {

   static Entity target;


   public EventAttack(Entity target) {
      this.target = target;
   }

   public static Entity getTarget() {
      return EventAttack.target;
   }

   public void setTarget(Entity target) {
	   EventAttack.target = target;
   }
}
