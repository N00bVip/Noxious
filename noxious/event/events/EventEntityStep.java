package noxious.event.events;

import net.minecraft.entity.Entity;
import noxious.event.Cancellable;
import noxious.event.Event;

public class EventEntityStep extends Event implements Cancellable {
    private final Entity entity;
    private final boolean canStep;
    private boolean cancelled;
    private double x, y, z;

    public EventEntityStep(Entity entity, boolean canStep, double x, double y, double z) {
        this.entity = entity;
        this.canStep = canStep;

        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public Entity getEntity() {
        return entity;
    }

    public boolean canStep() {
        return canStep;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
