package noxious.event.events;

import java.util.Iterator;

import noxious.Noxious;
import noxious.event.Event;
import noxious.mod.Mod;
import noxious.mod.mods.HUD;
import noxious.tabgui.TabGui;

public final class EventPressedKey extends Event {

   private final int key;

   public EventPressedKey(int key) {
      this.key = key;
   }

   public void checkKey()
   {
       if (this.key != 0)
       {
           if(this.key == (((Boolean)HUD.tabgui.getValue()).booleanValue()?200:72)) {
               TabGui.parseKeyUp();
            }

            if(this.key == (((Boolean)HUD.tabgui.getValue()).booleanValue()?208:80)) {
               TabGui.parseKeyDown();
            }

            if(this.key == (((Boolean)HUD.tabgui.getValue()).booleanValue()?203:75)) {
               TabGui.parseKeyLeft();
            }

            if(this.key == (((Boolean)HUD.tabgui.getValue()).booleanValue()?205:77)) {
               TabGui.parseKeyRight();
            }

            if(this.key == 28) {
               TabGui.parseKeyToggle();
            }
           Iterator var2 = Noxious.getModManager().getContents().iterator();

           while (var2.hasNext())
           {
               Mod mod = (Mod)var2.next();

               if (mod.getKeybind() != 0 && this.key == mod.getKeybind())
               {
                   mod.toggle();
               }
           }
       }
   }

   public int getKey() {
      return this.key;
   }
}
