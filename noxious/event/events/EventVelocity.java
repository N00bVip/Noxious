package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;
import net.minecraft.entity.Entity;

public final class EventVelocity extends Event implements Cancellable
{
    private boolean cancel;
    private final Entity entity;
    private final EventVelocity.Type type;

    public EventVelocity(EventVelocity.Type type, Entity entity)
    {
        this.type = type;
        this.entity = entity;
    }

    public Entity getEntity()
    {
        return this.entity;
    }

    public EventVelocity.Type getType()
    {
        return this.type;
    }

    public boolean isCancelled()
    {
        return this.cancel;
    }

    public void setCancelled(boolean cancel)
    {
        this.cancel = cancel;
    }

    public boolean isHurtcam()
    {
        return false;
    }

    public static enum Type
    {
        KNOCKBACK("KNOCKBACK", 0),
        WATER("WATER", 1);
        // $FF: synthetic field
        private static final EventVelocity.Type[] ENUM$VALUES = new EventVelocity.Type[]{KNOCKBACK, WATER};

        private Type(String var1, int var2) {}
    }
}
