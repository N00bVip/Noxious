package noxious.event.events;

import noxious.event.Event;

public final class EventFOVChange extends Event
{
    private float fov;

    public EventFOVChange(float fov)
    {
        this.fov = fov;
    }

    public float getFOV()
    {
        return this.fov;
    }

    public void setFOV(float fov)
    {
        this.fov = fov;
    }
}
