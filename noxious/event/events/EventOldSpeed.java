package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;

public class EventOldSpeed extends Event implements Cancellable {

   boolean cancel;


   public boolean isCancelled() {
      return this.cancel;
   }

   public void setCancelled(boolean shouldCancel) {
      this.cancel = shouldCancel;
   }
}
