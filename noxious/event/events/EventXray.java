package noxious.event.events;

import noxious.event.Event;

public class EventXray extends Event {

   boolean allow = false;


   public boolean shouldRay() {
      return this.allow;
   }

   public void setRay(boolean allow) {
      this.allow = allow;
   }
}
