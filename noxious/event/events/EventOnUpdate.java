package noxious.event.events;

import noxious.event.Cancellable;
import noxious.event.Event;

public class EventOnUpdate extends Event implements Cancellable {

   private boolean cancel;


   public boolean isCancelled() {
      return this.cancel;
   }

   public void setCancelled(boolean shouldCancel) {
      this.cancel = shouldCancel;
   }
}
