package noxious.irc;

import noxious.Noxious;
import noxious.threads.NoxiousThread;

public class IRC {


	public static void onStartup() {
		Noxious.setBot(getUsername());
		new Thread(Noxious.getBot()).start();
	}


	public static String getUsername() {
		return NoxiousThread.getUsername();
	}
}
