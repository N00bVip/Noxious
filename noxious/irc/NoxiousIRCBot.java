package noxious.irc;

import noxious.pircbot.NickAlreadyInUseException;
import noxious.pircbot.PircBot;
import noxious.util.Logger;

public class NoxiousIRCBot extends PircBot implements Runnable {
	private String channel = "#MinecraftHackNoxiousIRCClient";
	private String server = "irc.freenode.net";
	private boolean _autoNickChange;
	public static String username = "McHackNoxious";
	public static IRCBotClient bot;

	public NoxiousIRCBot(String username) {
		NoxiousIRCBot.username = username;
	}

	public void run() {
		try {
			bot = new IRCBotClient(username);

			bot.setVerbose(true);
			bot.connect(this.server);
			bot.joinChannel(this.channel);
		} catch (NickAlreadyInUseException e) {
		} catch (Exception e) {
			Logger.logChat("Error in IRC chat");
			e.printStackTrace();
		}
	}

	public void send(String message) {
		bot.sendMessage(channel, message);
	}

	public String getUsername() {
		return username;
	}

	public void quit() {
		bot.disconnect();
	}

	public void setUsername(String s) {
		username = s;
		setName(s);
	}
}
