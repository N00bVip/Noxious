package noxious.irc;


import noxious.pircbot.PircBot;
import noxious.util.HttpPost;
import noxious.util.Logger;

public class IRCBotClient extends PircBot {

	private String username;

	public IRCBotClient(String name) {
		this.username = name;
		setName(name);
	}

	public void onMessage(String channel, String sender, String login, String hostname, String message) {

		//TODO: 後で治す(?)
		if (HttpPost.getUserType(sender.replace("_", "")).equals("1")) {
			Logger.logIRC("§f[§6Vip§f] " + "§f<" + sender.replace("_", "") + "> " + message);
		} else if (HttpPost.getUserType(sender.replace("_", "")).equals("2")) {
			Logger.logIRC("§f[§cAdmin§f] " + "§f<" + sender.replace("_", "") + "> " + message);
		} else if (HttpPost.getUserType(sender.replace("_", "")).equals("3")) {
			Logger.logIRC("§f[§bDev§f] " + "§f<" + sender.replace("_", "") + "> " + message);
		} else if (HttpPost.getUserType(sender.replace("_", "")).equals("4")) {
			Logger.logIRC("§f[§aNoxious§f] " + "§f<" + sender.replace("_", "") + "> " + message);
		} else {
			Logger.logIRC("§f<" + sender.replace("_", "") + "> " + message);
		}
	}

	protected void onKick(String channel, String kickerNick, String kickerLogin, String kickerHostname, String recipientNick, String reason) {
		if (recipientNick.equalsIgnoreCase(getNick())) {
			Logger.logIRC("&cYou have been kicked by &f" + kickerNick + " &cfor &f" + reason);
			Logger.logIRC("&cRestart minecraft to get back into the IRC");
		} else {
			Logger.logIRC(recipientNick + "&c has been kicked by &f" + kickerNick + " &cfor &f" + reason);
		}
	}

	public void setUsername(String name) {
		this.username = name;
		setName(name);
	}
}
