package noxious.wallhack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import net.minecraft.block.Block;
import noxious.management.ListManager;

public class WallhackManager extends ListManager<Block>
{
    public Block getBlockUsingName(final String name) {
        if (this.getContents() == null) {
            return null;
        }
        for (final Block block : this.getContents()) {
            if (Block.blockRegistry.getNameForObject(block).toString().equalsIgnoreCase(name)) {
                return block;
            }
        }
        return null;
    }

    public Block getBlockUsingBlock(final Block block) {
        if (this.getContents() == null) {
            return null;
        }
        for (final Block bl : this.getContents()) {
            if (bl == block) {
                return block;
            }
        }
        return null;
    }

    public boolean doesListContain(final Block block) {
        return this.getBlockUsingBlock(block) != null;
    }

    @Override
    public void setup() {
        Collections.sort(this.contents = new ArrayList(), new Comparator() {
            public int compare(final Block block1, final Block block2) {
                return block1.getLocalizedName().compareTo(block2.getLocalizedName());
            }

            @Override
            public int compare(final Object var1, final Object var2) {
                return this.compare((Block)var1, (Block)var2);
            }
        });
    }
}
